import React, { Component } from 'react';
import {Tab, Popup, Menu, Label} from 'semantic-ui-react'
import ReactMapGL, {Marker} from 'react-map-gl';
import {sampleJSON, averageLatLong} from './sampleData'
import {  markerColorMap, badgeColorMap, statusTextMap, STYLE} from './Constants'
import './App.css';

const {avg_lat, avg_long} = averageLatLong(sampleJSON)

const getBadgeCount = (dataArray = [], statusVal) => dataArray.filter(dataPoint => dataPoint.status === statusVal).length

const TabText = (val, statusTextType, count = 0) => (
  <Menu.Item key={statusTextType || 'first'} color="white">
    {count ? <Label style={{backgroundColor: badgeColorMap[statusTextType]}} >{count}</Label> : null}
    <p className="tab_text">{val}</p>
  </Menu.Item>
)
// reason && activity ? `${reason}, ${activity}` : (reason || activity)
const header = (name) => <h3 className="popover_header">{name}</h3>

const content = ({device_id}) => <p className="popover_content">{device_id}</p>

class App extends Component {
  state = {
    tabPosition: 1,
    viewport: {
      latitude: null,
      longitude: null
    },
    zoom: 3
  }

  updateZoomValue = (zoom) => this.setState({zoom})
  
  setViewPort = (viewport, zoom) => this.setState({viewport, zoom})
  
  updateTabPosition = (tabPosition) => this.setState({tabPosition})

  getMapView = (deviceStatus) => (
    <div className={'mapContainer'}>
      <ReactMapGL
        width={900}
        height={600}
        latitude={this.state.viewport.latitude || avg_lat}
        longitude={this.state.viewport.longitude || avg_long}
        zoom={this.state.zoom}
        mapboxApiAccessToken={'pk.eyJ1Ijoic2FocmNrciIsImEiOiJjamdlaHpqcHYwN2hyMnhwM3h6Ym9yaGloIn0._dkewPeuh8dh6y5N7gNEuQ'}
        onViewportChange={(viewport) => this.setState({viewport})}
        mapStyle={'mapbox://styles/mapbox/dark-v9'}
        doubleClickZoom={true}
      >
        { 
          (deviceStatus ? sampleJSON.filter(dataPoint => dataPoint.status === deviceStatus) : sampleJSON)
            .map(dataPoint => (
              <Marker 
                key={dataPoint.device_id}
                latitude={dataPoint.lat} 
                longitude={dataPoint.long}
              >
                <Popup 
                  header={header(dataPoint.place)} 
                  content={content(dataPoint)} 
                  style={STYLE.POPUP_STYLE}
                  on={"hover"}
                  position='top center'
                  trigger = {(
                    <div
                      className={`map_marker ${markerColorMap[dataPoint.status]}`}
                      onClick={ _ => this.setViewPort({latitude: dataPoint.lat, longitude: dataPoint.long}, 12)}
                    />
                  )}
                  basic
                />
              </Marker>
            ))}
      </ReactMapGL>
      {/* <button onClick={_ => this.updateZoomValue(this.state.zoom + 1)}> + </button>
      <button onClick={_ => this.updateZoomValue(this.state.zoom - 1)}> - </button> */}
    </div>
  )

  render() {
    return (
      <div className="god_map_view_container">
        <Tab 
          className="ui centered god_map_view" 
          onChange={this.onTabChange}
          menu={{ secondary: true, pointing: true, inverted: true }}
          panes = {[
            { 
              key: 1,
              menuItem: TabText("All"),
              render: () => this.getMapView()
            },
            { 
              key: 2,
              menuItem: TabText("On Trip", statusTextMap.ON_TRIP ,getBadgeCount(sampleJSON, statusTextMap.ON_TRIP)),
              render: () => this.getMapView(statusTextMap.ON_TRIP)
            },
            { 
              key: 3,
              menuItem: TabText("At Visit", statusTextMap.ON_VISIT ,getBadgeCount(sampleJSON, statusTextMap.ON_VISIT)),
              render: () => this.getMapView(statusTextMap.ON_VISIT)
            },
            { 
              key: 4,
              menuItem: TabText("Permission Denied", statusTextMap.TRACKING_DISABLED ,getBadgeCount(sampleJSON, statusTextMap.TRACKING_DISABLED)),
              render: () => this.getMapView(statusTextMap.TRACKING_DISABLED)
            },
            { 
              key: 5,
              menuItem: TabText("Disconnected", statusTextMap.DISCONNECTED ,getBadgeCount(sampleJSON, statusTextMap.DISCONNECTED)),
              render: () => this.getMapView(statusTextMap.DISCONNECTED)
            },
          ]}
        />
      </div>
    );
  }
}

export default App;
