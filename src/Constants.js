const markerColorMap = {
  on_trip : 'processing',
  on_visit : 'warning',
  tracking_disabled : 'error',
  disconnected : 'default'
}

const badgeColorMap = {
  on_trip : '#00CE5B',
  on_visit : '#F97C24',
  tracking_disabled : '#EF074F',
  disconnected : '#9B9B9B'
}

const statusTextMap = {
  ON_TRIP: 'on_trip',
  ON_VISIT: 'on_visit',
  TRACKING_DISABLED: 'tracking_disabled',
  DISCONNECTED: 'disconnected'
}

const STYLE = {
  POPUP_STYLE: {
    backgroundColor: '#9013FE',
    borderColor: '#9013FE',
    borderRadius: '4px'
  }
}

export {
  markerColorMap,
  badgeColorMap,
  statusTextMap,
  STYLE
}