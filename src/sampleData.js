const sampleJSON = [
  {
    "device_id": "0007a979-6594-452e-826a-326cfd6860c4",
    "lat": 28.6391824,
    "long": 77.0743123,
    "place": "Sans Souci",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0017de9a-22a7-44f7-8709-56cf98325fed",
    "lat": 28.63282,
    "long": 77.04451,
    "place": "Yarrow Bay Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "002a4ef7-faae-4f30-8c7d-d8e6646aca63",
    "lat": 28.72117,
    "long": 77.0922,
    "place": "Chutney's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "0037d872-3237-4db5-9960-457ed64e5470",
    "lat": 19.01187,
    "long": 72.8422,
    "place": "Buca di Beppo",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "003f351b-a95e-487d-ae30-6a3907df921d",
    "lat": 28.6385643,
    "long": 77.1836212,
    "place": "Yarrow Bay Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "006a1db6-5ccf-4537-b73a-96f41e754a42",
    "lat": 28.53193,
    "long": 77.20791,
    "place": "Sans Souci",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "00b08b56-f50e-47c7-bc85-50fc234bd0b4",
    "lat": 28.46561,
    "long": 76.99644,
    "place": "Palomino",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "00b9e210-02a2-4b55-8a7b-d90435084892",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "00c01f65-9d8e-4e72-b5f2-4a88cb0736ea",
    "lat": 28.3659882,
    "long": 76.9435359,
    "place": "Pike Brewing Company",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "00e1f3b3-938f-4806-b50c-ea2bdb6959c3",
    "lat": 28.6909763,
    "long": 77.2950709,
    "place": "Space Needle",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "00eea482-c6f0-4dc7-9e36-17e5875646f5",
    "lat": 28.59125,
    "long": 77.3772,
    "place": "Buca di Beppo",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "00f19329-8b57-4d62-816f-320c259c9472",
    "lat": 19.02009,
    "long": 73.01782,
    "place": "Mac and Jack’s Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "00f65ff6-4c1f-450d-b226-8c3cc8829409",
    "lat": 28.70528,
    "long": 77.19769,
    "place": "Buca di Beppo",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "013497f0-d9c2-46c7-8a0c-61b574600080",
    "lat": 19.1784515,
    "long": 72.8530593,
    "place": "Pike Brewing Company",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "01371cf2-7728-4978-a483-390b1e841d90",
    "lat": 28.59060733,
    "long": 77.30519486,
    "place": "Café Juanita",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "013af3ae-6773-4c45-92b8-6a315fc2f4c2",
    "lat": 19.0338989,
    "long": 73.0718803,
    "place": "Rover's",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "0153c42b-28d3-4569-90fa-9e3f8a9b057f",
    "lat": 28.4661264,
    "long": 77.0816006,
    "place": "Dahlia Lounge",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "015ba4d2-0f13-4719-a757-2de4ef873736",
    "lat": 28.57961043,
    "long": 77.3643469,
    "place": "Isabella Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "015c8374-e8f3-4823-8017-3f40d21f104d",
    "lat": 12.91171,
    "long": 77.68186,
    "place": "Dahlia Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0180fad4-3a3b-4ae6-a6dd-3ddd8ec52d3f",
    "lat": 12.96869,
    "long": 77.75205,
    "place": "I Love Sushi",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "01c4eed3-66af-4a74-aea9-e95f97eca1fb",
    "lat": 28.5082318,
    "long": 77.2307777,
    "place": "Redhook Ale Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "01ce3a6f-414a-4381-94eb-073c03b2f27e",
    "lat": 17.4458,
    "long": 78.38699,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "01ef8b4f-39ee-4113-8fb5-2449cff2280f",
    "lat": 23.02039,
    "long": 72.54365,
    "place": "Wild Ginger",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "01f5e063-e6f2-4628-8df5-38c323855453",
    "lat": 28.4817717,
    "long": 77.0948783,
    "place": "Wasabi Bistro",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "01f83ef7-f28d-4594-a58a-76e1f3eecd7b",
    "lat": 28.58197,
    "long": 77.3731,
    "place": "Salish Lodge",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "020a7e3d-617e-4d05-a60c-f98c6516d2f4",
    "lat": 19.07847,
    "long": 72.88061,
    "place": "Pike Brewing Company",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "022dc2b9-c747-4d6c-9ba9-8d87d38d7cae",
    "lat": 28.5081506,
    "long": 77.2308966,
    "place": "Space Needle",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0253b8f4-49eb-4a7d-9c69-2aa6c805971e",
    "lat": 28.6267384,
    "long": 77.2999215,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0259195a-c507-45cb-ae0b-87bf8900f898",
    "lat": 18.95609,
    "long": 72.82448,
    "place": "Wasabi Bistro",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "025d9c7e-5d76-4572-a5e9-e5a89b997098",
    "lat": 12.99351,
    "long": 80.20107,
    "place": "Maggiano's Little Italy",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "0287764e-e941-4b92-be9c-e2bd9f5f83b6",
    "lat": 28.5546005,
    "long": 77.2895799,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "0299abd6-6651-49c2-bf97-58ee43e9787a",
    "lat": 26.78103,
    "long": 80.95173,
    "place": "Maggiano's Little Italy",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "02bee1d6-001d-402a-8e03-2a26ef95d43e",
    "lat": 28.6533,
    "long": 77.30209,
    "place": "Trader Vic's",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "02cfa62e-7302-48e9-a40d-ad34233fd409",
    "lat": 19.14234,
    "long": 72.84225,
    "place": "Bis On Main",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "02d7c4cb-8217-40f8-87ab-4914c5b54267",
    "lat": 28.62632,
    "long": 77.43819,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "02ded263-9a40-4514-9e65-0f8673dd6db3",
    "lat": 18.5327608,
    "long": 73.9468862,
    "place": "Manzana Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "02ec20f7-08b1-45b1-a39f-0a9a0245c600",
    "lat": 28.6753428,
    "long": 77.220344,
    "place": "Wasabi Bistro",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "02f0fec7-8158-4588-8fa7-02fda5a1532e",
    "lat": 28.4665,
    "long": 77.0062,
    "place": "The Calcutta Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "02fcda7c-a731-4556-aef2-7a72a37e84e7",
    "lat": 28.429,
    "long": 77.32445,
    "place": "Wasabi Bistro",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "0300181a-474e-4a94-ab02-564dd736fe26",
    "lat": 28.71819105,
    "long": 77.1775377,
    "place": "Bis On Main",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "0359bd20-f564-4ab9-9bc3-09817d102b63",
    "lat": 28.51798,
    "long": 77.20325,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "03951de6-bee3-40cb-b2eb-050abb34c436",
    "lat": 18.95525,
    "long": 72.83603,
    "place": "Cutters Bayhouse",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "03a288ac-6538-42a2-9cd7-715f4b5054d0",
    "lat": 28.6407,
    "long": 77.36024,
    "place": "Wasabi Bistro",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "03a7cd55-a5d3-4581-94a8-217cb0d9a13a",
    "lat": 28.67156,
    "long": 77.08481,
    "place": "Columbia Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "03c04698-6f84-456c-ab9c-fde4a4caeda7",
    "lat": 26.8794116,
    "long": 81.0065383,
    "place": "Royal India",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "03d99b49-2b00-4452-ad20-64b0bdf08f42",
    "lat": 22.5805,
    "long": 88.42554,
    "place": "Sans Souci",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "03e5995f-134a-418a-bb4c-9128a1b5ff6e",
    "lat": 28.57,
    "long": 77.3615,
    "place": "Wasabi Bistro",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "0416dbb0-34f5-458b-9a00-6e7cf9e569c8",
    "lat": 12.97341,
    "long": 77.60246,
    "place": "Chateau Ste Michelle Winery",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "04366d70-6a70-4709-a8a4-4ae8248ff301",
    "lat": 26.78073,
    "long": 80.96084,
    "place": "Sans Souci",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "045b6ff8-4e32-4d8c-950b-0725027d0736",
    "lat": 18.45977,
    "long": 73.7852,
    "place": "Redhook Ale Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "047de0ce-3613-4ba5-8d96-f582ca4d6c98",
    "lat": 26.8933,
    "long": 80.99626,
    "place": "The Calcutta Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0497deea-af03-472a-9fe2-cf328be051b6",
    "lat": 28.5240194,
    "long": 77.1930409,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "049c75e8-533d-4ab3-9c59-93edf1509cbf",
    "lat": 28.7065292,
    "long": 77.0985168,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "04be57aa-3bd9-4acd-9db7-ee31d5b873ac",
    "lat": 28.53885,
    "long": 77.21414,
    "place": "Palomino",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "04c4a995-52a1-476e-9ef6-32f83cd9f512",
    "lat": 26.90549,
    "long": 80.95727,
    "place": "Wild Ginger",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "04d02c69-5a06-4682-8283-ada65fd106e0",
    "lat": 22.5366025,
    "long": 88.3496555,
    "place": "Isabella Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "04e41b99-e2f1-4a4a-b61f-9e296efc58c2",
    "lat": 28.63797162,
    "long": 77.3791997,
    "place": "The Calcutta Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "04ebc56b-8c9a-4cf2-9e0c-8ffdcf6c7c9a",
    "lat": 28.64566,
    "long": 77.41948,
    "place": "Trader Vic's",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "04f51b44-d79e-4272-83c3-379cbb471e91",
    "lat": 28.61019,
    "long": 77.08849,
    "place": "Redhook Ale Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "05096e61-1e0c-404a-9dac-da211c107351",
    "lat": 17.38498,
    "long": 78.43312,
    "place": "The Calcutta Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "0517ce72-5700-42a6-a8ab-2bf526a193a2",
    "lat": 12.9372033,
    "long": 77.568845,
    "place": "Yarrow Bay Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0532af5e-e2c0-4b16-ba46-82e413884b84",
    "lat": 17.47672,
    "long": 78.54194,
    "place": "Desert Fire",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "053f9794-d60e-4a9a-bf5b-ecf3baea58d1",
    "lat": 28.4883,
    "long": 77.00014,
    "place": "Café Juanita",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "054fa861-42a0-499a-afcd-f998334c359b",
    "lat": 28.4266895,
    "long": 77.0950288,
    "place": "Market Cellar Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "05709e02-3d8b-41a6-aa06-1a57038b6745",
    "lat": 23.00663,
    "long": 72.59105,
    "place": "Royal India",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0578abe2-4d72-443c-84b6-3bd043c135bb",
    "lat": 12.96508,
    "long": 77.71237,
    "place": "Maggiano's Little Italy",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "05864955-ed30-4b27-8378-19f2bcd9f200",
    "lat": 19.0138961,
    "long": 72.8190351,
    "place": "Italianissimo Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "05d3de2e-0d7d-441c-86af-bfbaff0994d3",
    "lat": 28.4529,
    "long": 77.06151,
    "place": "Pike Brewing Company",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "05f04024-7acd-4332-a72a-802f552ddac2",
    "lat": 22.58715,
    "long": 88.33934,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "062822e5-b780-4cd1-8581-470764ee0d85",
    "lat": 17.5062,
    "long": 78.33048,
    "place": "Palomino",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "0634d1ce-d81e-4bd7-92d7-b7855dff747b",
    "lat": 13.03856,
    "long": 77.66203,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "06652954-71e4-4ea9-91b8-fe72c53c7d60",
    "lat": 28.64489,
    "long": 77.33851,
    "place": "Wasabi Bistro",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0674f950-7bb2-4a92-9b67-4fca8710ea85",
    "lat": 12.87432,
    "long": 77.61964,
    "place": "Market Cellar Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "06802923-6153-4234-a916-4ca36821364e",
    "lat": 28.60259,
    "long": 77.37924,
    "place": "I Love Sushi",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0695b88f-498e-4864-921a-7632e81166c0",
    "lat": 19.23652,
    "long": 72.97301,
    "place": "Trader Vic's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "06b45a7c-cd15-453b-8731-97711bd481ec",
    "lat": 28.5921092,
    "long": 77.0350934,
    "place": "Sea Garden",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "06c0015d-79e4-478f-8b8f-fc75d67a3aa0",
    "lat": 23.03025,
    "long": 72.58802,
    "place": "Pike Brewing Company",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "06c57220-06ea-4ff4-b242-1e529f356618",
    "lat": 28.63553,
    "long": 77.1832,
    "place": "Assagio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "06ed92e6-3697-420f-850d-0facb8864dd5",
    "lat": 18.59291,
    "long": 73.79791,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "070446f4-f450-4aaa-b1cd-e857ab057aa4",
    "lat": 28.4814554,
    "long": 77.30375823,
    "place": "Coho Cafe",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "077715a9-3173-4b3e-a3d2-52b7fb0fc9d7",
    "lat": 19.02001,
    "long": 73.01761,
    "place": "Uptown China Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "079058e7-c74b-43eb-b19c-368d88885910",
    "lat": 28.56816,
    "long": 77.34596,
    "place": "Café Juanita",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "07d8a73c-1891-4d0d-bf08-eedc429e9b07",
    "lat": 28.53193,
    "long": 77.20791,
    "place": "Royal India",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "07eb10ab-41d8-423d-9986-bca3e9a1d14e",
    "lat": 28.59164,
    "long": 77.30932,
    "place": "Sans Souci",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "08475357-c8a5-40d7-afb0-ed1f100a87f8",
    "lat": 28.56103,
    "long": 77.22723,
    "place": "Uptown China Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "084a96b7-c8b3-46b5-8e3f-024eb3b752b6",
    "lat": 28.3704,
    "long": 77.31431,
    "place": "Chutney's",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "085d4e3d-96d3-4e83-a72f-09cfb84caf70",
    "lat": 19.1204017,
    "long": 72.8694867,
    "place": "Soos Creek Wine Cellars",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0863709a-b79f-4a4f-b9b0-81ca2e7007ce",
    "lat": 28.68711,
    "long": 77.36667,
    "place": "Chutney's",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "08a914e1-b378-426f-b344-6c647d508bc3",
    "lat": 28.62719,
    "long": 77.30862,
    "place": "Buca di Beppo",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "08ed8742-47c7-4bab-9baf-765ad59f0f3e",
    "lat": 28.6580048,
    "long": 77.1190309,
    "place": "Il Fornaio",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "09071bb7-4e0d-420e-8610-589ec89e0e78",
    "lat": 23.055,
    "long": 72.63162,
    "place": "Royal India",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "0929b7eb-165c-4f7e-964e-8d7d74514e38",
    "lat": 28.4707,
    "long": 77.29647,
    "place": "Palomino",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "092e733c-c8d5-4cf4-9bd5-bbf5379b79e5",
    "lat": 28.45375,
    "long": 77.06296,
    "place": "Desert Fire",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "0937eb52-8c34-4777-9e8e-d5955b2512ab",
    "lat": 28.55737,
    "long": 77.28282,
    "place": "Italianissimo Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "095237b5-5536-4d3c-bb12-73d8a093da4f",
    "lat": 28.57893,
    "long": 77.38174,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "0964205b-01a6-44f4-87b2-3bbfaa8cc975",
    "lat": 28.5081506,
    "long": 77.2308966,
    "place": "Typhoon",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "0983a344-60c7-45eb-a781-5d20ab75782d",
    "lat": 28.5135719,
    "long": 77.1699641,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "098857c5-2fc8-4279-9e7f-3b354030ab3f",
    "lat": 28.634539,
    "long": 77.3530191,
    "place": "Sans Souci",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "09ce3c33-42c1-4b89-b90b-78bea6f5999f",
    "lat": 28.39691,
    "long": 76.96961,
    "place": "Rover's",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "09eb19cd-067f-4438-8841-dbf43ea5e4ec",
    "lat": 28.7076608,
    "long": 77.1102646,
    "place": "Salute of Bellevue",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  }
]

const completeJson = [
  {
    "device_id": "0007a979-6594-452e-826a-326cfd6860c4",
    "lat": 28.6391824,
    "long": 77.0743123,
    "place": "Sans Souci",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0017de9a-22a7-44f7-8709-56cf98325fed",
    "lat": 28.63282,
    "long": 77.04451,
    "place": "Yarrow Bay Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "002a4ef7-faae-4f30-8c7d-d8e6646aca63",
    "lat": 28.72117,
    "long": 77.0922,
    "place": "Chutney's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "0037d872-3237-4db5-9960-457ed64e5470",
    "lat": 19.01187,
    "long": 72.8422,
    "place": "Buca di Beppo",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "003f351b-a95e-487d-ae30-6a3907df921d",
    "lat": 28.6385643,
    "long": 77.1836212,
    "place": "Yarrow Bay Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "006a1db6-5ccf-4537-b73a-96f41e754a42",
    "lat": 28.53193,
    "long": 77.20791,
    "place": "Sans Souci",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "00b08b56-f50e-47c7-bc85-50fc234bd0b4",
    "lat": 28.46561,
    "long": 76.99644,
    "place": "Palomino",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "00b9e210-02a2-4b55-8a7b-d90435084892",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "00c01f65-9d8e-4e72-b5f2-4a88cb0736ea",
    "lat": 28.3659882,
    "long": 76.9435359,
    "place": "Pike Brewing Company",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "00e1f3b3-938f-4806-b50c-ea2bdb6959c3",
    "lat": 28.6909763,
    "long": 77.2950709,
    "place": "Space Needle",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "00eea482-c6f0-4dc7-9e36-17e5875646f5",
    "lat": 28.59125,
    "long": 77.3772,
    "place": "Buca di Beppo",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "00f19329-8b57-4d62-816f-320c259c9472",
    "lat": 19.02009,
    "long": 73.01782,
    "place": "Mac and Jack’s Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "00f65ff6-4c1f-450d-b226-8c3cc8829409",
    "lat": 28.70528,
    "long": 77.19769,
    "place": "Buca di Beppo",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "013497f0-d9c2-46c7-8a0c-61b574600080",
    "lat": 19.1784515,
    "long": 72.8530593,
    "place": "Pike Brewing Company",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "01371cf2-7728-4978-a483-390b1e841d90",
    "lat": 28.59060733,
    "long": 77.30519486,
    "place": "Café Juanita",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "013af3ae-6773-4c45-92b8-6a315fc2f4c2",
    "lat": 19.0338989,
    "long": 73.0718803,
    "place": "Rover's",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "0153c42b-28d3-4569-90fa-9e3f8a9b057f",
    "lat": 28.4661264,
    "long": 77.0816006,
    "place": "Dahlia Lounge",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "015ba4d2-0f13-4719-a757-2de4ef873736",
    "lat": 28.57961043,
    "long": 77.3643469,
    "place": "Isabella Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "015c8374-e8f3-4823-8017-3f40d21f104d",
    "lat": 12.91171,
    "long": 77.68186,
    "place": "Dahlia Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0180fad4-3a3b-4ae6-a6dd-3ddd8ec52d3f",
    "lat": 12.96869,
    "long": 77.75205,
    "place": "I Love Sushi",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "01c4eed3-66af-4a74-aea9-e95f97eca1fb",
    "lat": 28.5082318,
    "long": 77.2307777,
    "place": "Redhook Ale Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "01ce3a6f-414a-4381-94eb-073c03b2f27e",
    "lat": 17.4458,
    "long": 78.38699,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "01ef8b4f-39ee-4113-8fb5-2449cff2280f",
    "lat": 23.02039,
    "long": 72.54365,
    "place": "Wild Ginger",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "01f5e063-e6f2-4628-8df5-38c323855453",
    "lat": 28.4817717,
    "long": 77.0948783,
    "place": "Wasabi Bistro",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "01f83ef7-f28d-4594-a58a-76e1f3eecd7b",
    "lat": 28.58197,
    "long": 77.3731,
    "place": "Salish Lodge",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "020a7e3d-617e-4d05-a60c-f98c6516d2f4",
    "lat": 19.07847,
    "long": 72.88061,
    "place": "Pike Brewing Company",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "022dc2b9-c747-4d6c-9ba9-8d87d38d7cae",
    "lat": 28.5081506,
    "long": 77.2308966,
    "place": "Space Needle",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0253b8f4-49eb-4a7d-9c69-2aa6c805971e",
    "lat": 28.6267384,
    "long": 77.2999215,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0259195a-c507-45cb-ae0b-87bf8900f898",
    "lat": 18.95609,
    "long": 72.82448,
    "place": "Wasabi Bistro",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "025d9c7e-5d76-4572-a5e9-e5a89b997098",
    "lat": 12.99351,
    "long": 80.20107,
    "place": "Maggiano's Little Italy",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "0287764e-e941-4b92-be9c-e2bd9f5f83b6",
    "lat": 28.5546005,
    "long": 77.2895799,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "0299abd6-6651-49c2-bf97-58ee43e9787a",
    "lat": 26.78103,
    "long": 80.95173,
    "place": "Maggiano's Little Italy",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "02bee1d6-001d-402a-8e03-2a26ef95d43e",
    "lat": 28.6533,
    "long": 77.30209,
    "place": "Trader Vic's",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "02cfa62e-7302-48e9-a40d-ad34233fd409",
    "lat": 19.14234,
    "long": 72.84225,
    "place": "Bis On Main",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "02d7c4cb-8217-40f8-87ab-4914c5b54267",
    "lat": 28.62632,
    "long": 77.43819,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "02ded263-9a40-4514-9e65-0f8673dd6db3",
    "lat": 18.5327608,
    "long": 73.9468862,
    "place": "Manzana Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "02ec20f7-08b1-45b1-a39f-0a9a0245c600",
    "lat": 28.6753428,
    "long": 77.220344,
    "place": "Wasabi Bistro",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "02f0fec7-8158-4588-8fa7-02fda5a1532e",
    "lat": 28.4665,
    "long": 77.0062,
    "place": "The Calcutta Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "02fcda7c-a731-4556-aef2-7a72a37e84e7",
    "lat": 28.429,
    "long": 77.32445,
    "place": "Wasabi Bistro",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "0300181a-474e-4a94-ab02-564dd736fe26",
    "lat": 28.71819105,
    "long": 77.1775377,
    "place": "Bis On Main",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "0359bd20-f564-4ab9-9bc3-09817d102b63",
    "lat": 28.51798,
    "long": 77.20325,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "03951de6-bee3-40cb-b2eb-050abb34c436",
    "lat": 18.95525,
    "long": 72.83603,
    "place": "Cutters Bayhouse",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "03a288ac-6538-42a2-9cd7-715f4b5054d0",
    "lat": 28.6407,
    "long": 77.36024,
    "place": "Wasabi Bistro",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "03a7cd55-a5d3-4581-94a8-217cb0d9a13a",
    "lat": 28.67156,
    "long": 77.08481,
    "place": "Columbia Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "03c04698-6f84-456c-ab9c-fde4a4caeda7",
    "lat": 26.8794116,
    "long": 81.0065383,
    "place": "Royal India",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "03d99b49-2b00-4452-ad20-64b0bdf08f42",
    "lat": 22.5805,
    "long": 88.42554,
    "place": "Sans Souci",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "03e5995f-134a-418a-bb4c-9128a1b5ff6e",
    "lat": 28.57,
    "long": 77.3615,
    "place": "Wasabi Bistro",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "0416dbb0-34f5-458b-9a00-6e7cf9e569c8",
    "lat": 12.97341,
    "long": 77.60246,
    "place": "Chateau Ste Michelle Winery",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "04366d70-6a70-4709-a8a4-4ae8248ff301",
    "lat": 26.78073,
    "long": 80.96084,
    "place": "Sans Souci",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "045b6ff8-4e32-4d8c-950b-0725027d0736",
    "lat": 18.45977,
    "long": 73.7852,
    "place": "Redhook Ale Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "047de0ce-3613-4ba5-8d96-f582ca4d6c98",
    "lat": 26.8933,
    "long": 80.99626,
    "place": "The Calcutta Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0497deea-af03-472a-9fe2-cf328be051b6",
    "lat": 28.5240194,
    "long": 77.1930409,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "049c75e8-533d-4ab3-9c59-93edf1509cbf",
    "lat": 28.7065292,
    "long": 77.0985168,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "04be57aa-3bd9-4acd-9db7-ee31d5b873ac",
    "lat": 28.53885,
    "long": 77.21414,
    "place": "Palomino",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "04c4a995-52a1-476e-9ef6-32f83cd9f512",
    "lat": 26.90549,
    "long": 80.95727,
    "place": "Wild Ginger",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "04d02c69-5a06-4682-8283-ada65fd106e0",
    "lat": 22.5366025,
    "long": 88.3496555,
    "place": "Isabella Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "04e41b99-e2f1-4a4a-b61f-9e296efc58c2",
    "lat": 28.63797162,
    "long": 77.3791997,
    "place": "The Calcutta Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "04ebc56b-8c9a-4cf2-9e0c-8ffdcf6c7c9a",
    "lat": 28.64566,
    "long": 77.41948,
    "place": "Trader Vic's",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "04f51b44-d79e-4272-83c3-379cbb471e91",
    "lat": 28.61019,
    "long": 77.08849,
    "place": "Redhook Ale Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "05096e61-1e0c-404a-9dac-da211c107351",
    "lat": 17.38498,
    "long": 78.43312,
    "place": "The Calcutta Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "0517ce72-5700-42a6-a8ab-2bf526a193a2",
    "lat": 12.9372033,
    "long": 77.568845,
    "place": "Yarrow Bay Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0532af5e-e2c0-4b16-ba46-82e413884b84",
    "lat": 17.47672,
    "long": 78.54194,
    "place": "Desert Fire",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "053f9794-d60e-4a9a-bf5b-ecf3baea58d1",
    "lat": 28.4883,
    "long": 77.00014,
    "place": "Café Juanita",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "054fa861-42a0-499a-afcd-f998334c359b",
    "lat": 28.4266895,
    "long": 77.0950288,
    "place": "Market Cellar Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "05709e02-3d8b-41a6-aa06-1a57038b6745",
    "lat": 23.00663,
    "long": 72.59105,
    "place": "Royal India",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0578abe2-4d72-443c-84b6-3bd043c135bb",
    "lat": 12.96508,
    "long": 77.71237,
    "place": "Maggiano's Little Italy",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "05864955-ed30-4b27-8378-19f2bcd9f200",
    "lat": 19.0138961,
    "long": 72.8190351,
    "place": "Italianissimo Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "05d3de2e-0d7d-441c-86af-bfbaff0994d3",
    "lat": 28.4529,
    "long": 77.06151,
    "place": "Pike Brewing Company",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "05f04024-7acd-4332-a72a-802f552ddac2",
    "lat": 22.58715,
    "long": 88.33934,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "062822e5-b780-4cd1-8581-470764ee0d85",
    "lat": 17.5062,
    "long": 78.33048,
    "place": "Palomino",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "0634d1ce-d81e-4bd7-92d7-b7855dff747b",
    "lat": 13.03856,
    "long": 77.66203,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "06652954-71e4-4ea9-91b8-fe72c53c7d60",
    "lat": 28.64489,
    "long": 77.33851,
    "place": "Wasabi Bistro",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0674f950-7bb2-4a92-9b67-4fca8710ea85",
    "lat": 12.87432,
    "long": 77.61964,
    "place": "Market Cellar Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "06802923-6153-4234-a916-4ca36821364e",
    "lat": 28.60259,
    "long": 77.37924,
    "place": "I Love Sushi",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0695b88f-498e-4864-921a-7632e81166c0",
    "lat": 19.23652,
    "long": 72.97301,
    "place": "Trader Vic's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "06b45a7c-cd15-453b-8731-97711bd481ec",
    "lat": 28.5921092,
    "long": 77.0350934,
    "place": "Sea Garden",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "06c0015d-79e4-478f-8b8f-fc75d67a3aa0",
    "lat": 23.03025,
    "long": 72.58802,
    "place": "Pike Brewing Company",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "06c57220-06ea-4ff4-b242-1e529f356618",
    "lat": 28.63553,
    "long": 77.1832,
    "place": "Assagio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "06ed92e6-3697-420f-850d-0facb8864dd5",
    "lat": 18.59291,
    "long": 73.79791,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "070446f4-f450-4aaa-b1cd-e857ab057aa4",
    "lat": 28.4814554,
    "long": 77.30375823,
    "place": "Coho Cafe",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "077715a9-3173-4b3e-a3d2-52b7fb0fc9d7",
    "lat": 19.02001,
    "long": 73.01761,
    "place": "Uptown China Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "079058e7-c74b-43eb-b19c-368d88885910",
    "lat": 28.56816,
    "long": 77.34596,
    "place": "Café Juanita",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "07d8a73c-1891-4d0d-bf08-eedc429e9b07",
    "lat": 28.53193,
    "long": 77.20791,
    "place": "Royal India",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "07eb10ab-41d8-423d-9986-bca3e9a1d14e",
    "lat": 28.59164,
    "long": 77.30932,
    "place": "Sans Souci",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "08475357-c8a5-40d7-afb0-ed1f100a87f8",
    "lat": 28.56103,
    "long": 77.22723,
    "place": "Uptown China Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "084a96b7-c8b3-46b5-8e3f-024eb3b752b6",
    "lat": 28.3704,
    "long": 77.31431,
    "place": "Chutney's",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "085d4e3d-96d3-4e83-a72f-09cfb84caf70",
    "lat": 19.1204017,
    "long": 72.8694867,
    "place": "Soos Creek Wine Cellars",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0863709a-b79f-4a4f-b9b0-81ca2e7007ce",
    "lat": 28.68711,
    "long": 77.36667,
    "place": "Chutney's",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "08a914e1-b378-426f-b344-6c647d508bc3",
    "lat": 28.62719,
    "long": 77.30862,
    "place": "Buca di Beppo",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "08ed8742-47c7-4bab-9baf-765ad59f0f3e",
    "lat": 28.6580048,
    "long": 77.1190309,
    "place": "Il Fornaio",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "09071bb7-4e0d-420e-8610-589ec89e0e78",
    "lat": 23.055,
    "long": 72.63162,
    "place": "Royal India",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "0929b7eb-165c-4f7e-964e-8d7d74514e38",
    "lat": 28.4707,
    "long": 77.29647,
    "place": "Palomino",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "092e733c-c8d5-4cf4-9bd5-bbf5379b79e5",
    "lat": 28.45375,
    "long": 77.06296,
    "place": "Desert Fire",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "0937eb52-8c34-4777-9e8e-d5955b2512ab",
    "lat": 28.55737,
    "long": 77.28282,
    "place": "Italianissimo Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "095237b5-5536-4d3c-bb12-73d8a093da4f",
    "lat": 28.57893,
    "long": 77.38174,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "0964205b-01a6-44f4-87b2-3bbfaa8cc975",
    "lat": 28.5081506,
    "long": 77.2308966,
    "place": "Typhoon",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "0983a344-60c7-45eb-a781-5d20ab75782d",
    "lat": 28.5135719,
    "long": 77.1699641,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "098857c5-2fc8-4279-9e7f-3b354030ab3f",
    "lat": 28.634539,
    "long": 77.3530191,
    "place": "Sans Souci",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "09ce3c33-42c1-4b89-b90b-78bea6f5999f",
    "lat": 28.39691,
    "long": 76.96961,
    "place": "Rover's",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "09eb19cd-067f-4438-8841-dbf43ea5e4ec",
    "lat": 28.7076608,
    "long": 77.1102646,
    "place": "Salute of Bellevue",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0a003b2e-ded5-40b4-8056-855bf5dc17ff",
    "lat": 18.95525,
    "long": 72.8361,
    "place": "Dahlia Lounge",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "0a127057-c22f-4844-a874-52238e25700e",
    "lat": 28.65574,
    "long": 77.28632,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0a354753-2439-4d2f-8d52-5e38abf8ed85",
    "lat": 17.44591,
    "long": 78.38699,
    "place": "Market Cellar Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0a36bab6-c96c-490f-886f-dd3eca6c2d58",
    "lat": 19.19084,
    "long": 72.82029,
    "place": "Columbia Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "0a4f7dbb-191f-4878-addb-aac853831673",
    "lat": 28.5017313,
    "long": 77.377103,
    "place": "Royal India",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "0a673a82-85b1-42d7-96d5-bf7da6abc1e2",
    "lat": 28.47574,
    "long": 77.51793,
    "place": "Buca di Beppo",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0a737fcf-d766-4ace-a97c-1a87d9354b50",
    "lat": 22.60214,
    "long": 88.37627,
    "place": "Herb Farm",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "0a7f418b-3180-47b6-a2ea-a189940dab5d",
    "lat": 28.4356892,
    "long": 77.0566262,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "0a870fc6-1310-495d-8b6e-7be6a2f70921",
    "lat": 28.46021,
    "long": 77.08846,
    "place": "Mac and Jack’s Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "0aa60ae7-5407-4c4e-a4c7-f2ca8d655159",
    "lat": 19.10901,
    "long": 72.85514,
    "place": "Tap House Grill",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0ab302eb-fbcf-4e51-b348-5b713aedb323",
    "lat": 28.6580048,
    "long": 77.1190309,
    "place": "Uptown China Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0ac5c02b-a4bd-46ec-bfdc-4c070fba1dad",
    "lat": 28.63541,
    "long": 77.1832,
    "place": "Salute of Bellevue",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0acf0f3b-944e-45f9-bf89-a187b9e0a33d",
    "lat": 28.67827,
    "long": 77.28874,
    "place": "Desert Fire",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "0adbae38-59e0-4a69-af6f-ba8c76721db7",
    "lat": 12.95794,
    "long": 77.69448,
    "place": "The Calcutta Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "0ae1fb9f-7253-43c6-9ec8-aaa0f6d342bd",
    "lat": 28.39691,
    "long": 76.96961,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "0b02ee12-762c-4a5f-a2a9-a0f3bcc7db14",
    "lat": 22.60368,
    "long": 88.41132,
    "place": "Cutters Bayhouse",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0b11494b-d4e6-4e2d-9feb-0ff9d3f4bd20",
    "lat": 12.9538,
    "long": 77.52692,
    "place": "Dahlia Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0b22bac8-ecff-4441-a5ad-bef094d6c2bd",
    "lat": 28.65463,
    "long": 77.35094,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0b3e6a45-456f-47b6-be8c-62b561eae558",
    "lat": 12.92477,
    "long": 77.6674,
    "place": "Bis On Main",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "0b7c9b44-ef79-4bf3-b747-69d655d94b9e",
    "lat": 19.2185882,
    "long": 72.8705949,
    "place": "Market Cellar Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "0b90b56f-0e44-4939-b57d-4ea95803baa1",
    "lat": 28.56345,
    "long": 77.38411,
    "place": "Herb Farm",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0be10848-38b5-4a70-a916-daca4730b3c0",
    "lat": 28.50823,
    "long": 77.23078,
    "place": "Chutney's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "0c222c12-9365-4d27-acc4-3e48fdc2b753",
    "lat": 13.0209,
    "long": 77.69313,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0c266c12-7cb8-4a63-b536-c5c06e6938f9",
    "lat": 28.6741257,
    "long": 77.12119,
    "place": "Trader Vic's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0c2b3f4a-4a74-4e81-9d79-793bd6776863",
    "lat": 28.6831,
    "long": 77.36374,
    "place": "Palomino",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "0c80123a-b118-47fb-a516-e1f906426331",
    "lat": 12.8993548,
    "long": 77.69580388,
    "place": "Salute of Bellevue",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0c813138-ad7d-4fa9-b99a-c088041dfb36",
    "lat": 22.61389,
    "long": 88.43273,
    "place": "Italianissimo Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "0ca49063-31d1-4b54-a207-001ed46f2a4f",
    "lat": 28.6332138,
    "long": 77.3122497,
    "place": "Pike Brewing Company",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "0cc6a674-6f5a-43b7-93c2-6dfc0d56d278",
    "lat": 28.658,
    "long": 77.11903,
    "place": "Pike Brewing Company",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "0cd12747-228a-46e2-826f-fa31a3eee122",
    "lat": 28.51791,
    "long": 77.20336,
    "place": "The Tasting Room",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "0cdcee60-ee00-4e2d-b88d-861f22ab8b95",
    "lat": 28.60644,
    "long": 77.06363,
    "place": "Chutney's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0d260e74-9e36-4b05-9909-66f2a71a1133",
    "lat": 22.49442,
    "long": 88.33794,
    "place": "Il Fornaio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0d58f84b-31b3-44f0-a5d4-8e93cb7167ba",
    "lat": 23.01856,
    "long": 72.56148,
    "place": "Assagio",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0d639092-989f-44f4-a485-55208777f5b5",
    "lat": 28.63662,
    "long": 77.23945,
    "place": "Café Juanita",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0db8dda1-b70b-4702-8e98-019ddc6ec249",
    "lat": 28.4447122,
    "long": 77.066974,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0dba418f-403a-48f7-aeec-de9a3bd90008",
    "lat": 28.6811952,
    "long": 77.1638391,
    "place": "Chutney's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0dbb7102-0c9e-455b-b62a-1fa0dd6af39c",
    "lat": 22.62526,
    "long": 88.42062,
    "place": "Palomino",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "0dbceebe-2c39-4ce3-ba57-05481e812782",
    "lat": 28.59697,
    "long": 77.05192,
    "place": "Salute of Bellevue",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "0dc92247-6498-4a5e-9516-6cb6a84a12db",
    "lat": 22.60214,
    "long": 88.37627,
    "place": "Assagio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0e174b1d-e245-43ef-bfa4-7d1b8a94fb2c",
    "lat": 28.60795,
    "long": 77.39976,
    "place": "Salute of Bellevue",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "0e3c97f9-aea7-4789-a7db-2bf50096f505",
    "lat": 26.85545,
    "long": 75.78532,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0e4eb78e-2bae-46c9-97ff-85dd71c31eea",
    "lat": 28.65947,
    "long": 77.14019,
    "place": "Sea Garden",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0e66d46b-8e9c-483b-9196-e15d191b7a67",
    "lat": 28.4886872,
    "long": 77.0956853,
    "place": "Herb Farm",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0ebfe41a-f805-47a5-a24f-5d0208ec9c18",
    "lat": 28.55693,
    "long": 77.06728,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "0eef7326-70ef-4315-a2da-82cf609992ab",
    "lat": 22.45725,
    "long": 88.30882,
    "place": "Rover's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0ef89531-e4ac-4c1a-a8a4-aac17d5323da",
    "lat": 18.4644297,
    "long": 73.9047707,
    "place": "Wild Ginger",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "0f0afed3-3f01-4555-a432-e7d2a94d6469",
    "lat": 19.20539,
    "long": 72.96046,
    "place": "Assagio",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "0f27fd39-022c-4c86-9fc2-cbbd33a93fe9",
    "lat": 26.85598,
    "long": 80.88686,
    "place": "I Love Sushi",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0f38b8d1-dfcb-4054-92f4-781ab3863a92",
    "lat": 28.65796,
    "long": 77.11906,
    "place": "Market Cellar Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0f608a03-99d3-4f6f-80f5-8bbfec0d652b",
    "lat": 26.88112104,
    "long": 81.00646337,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0f8d9dbb-44f5-480a-9882-c74a701e007e",
    "lat": 28.6366387,
    "long": 77.2791422,
    "place": "Soos Creek Wine Cellars",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "0fd71f41-2cee-4225-af1f-ca410d10c6c1",
    "lat": 28.72117,
    "long": 77.09224,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "0fddefa7-b7fd-4e44-bfc1-38ce93dacb25",
    "lat": 28.72665,
    "long": 77.16975,
    "place": "Desert Fire",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "1048173d-3cfb-46cd-84ad-20e4cdda663e",
    "lat": 17.36303,
    "long": 78.54034,
    "place": "Pike Brewing Company",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "10c4e793-8f15-48f3-b98d-ce91694b3b3b",
    "lat": 28.48348,
    "long": 77.01868,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "10fc597f-3f40-48cd-a206-50994708c384",
    "lat": 19.1253,
    "long": 72.91842,
    "place": "Yarrow Bay Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "1150061b-cb7f-4b01-8c92-6a81a3a00e03",
    "lat": 17.50095,
    "long": 78.41087,
    "place": "Yarrow Bay Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1156f4dc-5cd0-480c-81b6-bae205a92c31",
    "lat": 28.64858,
    "long": 77.1092,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "11615eec-40af-4ba4-b215-614e47ce4f0b",
    "lat": 28.56382,
    "long": 77.05936,
    "place": "Wild Ginger",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "1177e900-b54d-475b-a7ce-e3af190816f6",
    "lat": 28.6166,
    "long": 77.2831,
    "place": "Desert Fire",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "11d4eb7e-400e-4680-a2fc-d83055b7e0ec",
    "lat": 28.5546005,
    "long": 77.2895799,
    "place": "Palomino",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "120fead1-676f-4f99-a4ea-8e59835780f4",
    "lat": 28.6409383,
    "long": 77.2085383,
    "place": "Italianissimo Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1212d6fb-e40d-443f-937c-a47e4c1668da",
    "lat": 26.91629,
    "long": 75.70243,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "1248f2b3-a3a7-48fe-b09f-27b5321f2477",
    "lat": 28.45373,
    "long": 77.06295,
    "place": "Redhook Ale Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "124b8804-2369-4297-aad9-2cf34669234d",
    "lat": 28.46256,
    "long": 77.00025,
    "place": "Isabella Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "12691f2a-baac-4085-8546-1d60fafc17b9",
    "lat": 28.6328893,
    "long": 77.2720913,
    "place": "Columbia Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "126dd3f5-7606-4e1a-8943-9838f65654ea",
    "lat": 19.19084,
    "long": 72.82029,
    "place": "Space Needle",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "12f901a7-6bdd-4f33-be54-2541eb34e5c1",
    "lat": 19.07391,
    "long": 72.82409,
    "place": "Royal India",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "13139841-7845-4074-bd0f-c968c59e5ead",
    "lat": 28.5445186,
    "long": 77.27582,
    "place": "The Tasting Room",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "131a0952-943e-48c2-be90-a4ef031e005b",
    "lat": 28.46551,
    "long": 77.00556,
    "place": "Pike Brewing Company",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "13239680-8eb3-4bc6-90f6-5428748640b3",
    "lat": 26.8794116,
    "long": 81.0065383,
    "place": "Maggiano's Little Italy",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "132fd1c6-e59e-413f-b668-3e4e6bff84d0",
    "lat": 28.5610379,
    "long": 77.2272677,
    "place": "Sea Garden",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1358aa68-5ebc-4b68-9f3b-25f0a1cff40a",
    "lat": 22.95985,
    "long": 72.61,
    "place": "Café Juanita",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "136a33ed-689e-470f-bbc6-e95eab3b86ea",
    "lat": 28.6385643,
    "long": 77.1836212,
    "place": "Coho Cafe",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1376b74d-4169-46ab-a38f-4bc943050baf",
    "lat": 19.23658,
    "long": 72.97271,
    "place": "Maggiano's Little Italy",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "13908338-622b-4dea-9e35-7ca555efa4ee",
    "lat": 12.8879012,
    "long": 77.62918407,
    "place": "Palomino",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "1394c5e3-da5c-44d6-bcea-26a2081a17ad",
    "lat": 28.56801,
    "long": 77.38309,
    "place": "Royal India",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "13989695-7b3f-4950-8a1a-496f54726f7b",
    "lat": 28.49591,
    "long": 77.08446,
    "place": "Wasabi Bistro",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "140e4f73-5790-4e44-b1a0-e7f27b6331dc",
    "lat": 22.5791915,
    "long": 88.4804969,
    "place": "Italianissimo Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "145ef9be-9a06-4812-ac89-71fbabc6f929",
    "lat": 18.9642,
    "long": 72.83347,
    "place": "Assagio",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "146bbb55-39a1-46ed-87c7-7e0b0a788b81",
    "lat": 18.49148,
    "long": 73.79221,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "14880507-96dd-4e94-acba-93e6ec8ae8d9",
    "lat": 28.64469,
    "long": 77.33864,
    "place": "Maggiano's Little Italy",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "14a3507b-6d98-41fb-b2f1-dac8d05ac462",
    "lat": 12.9845616,
    "long": 77.5584315,
    "place": "Yarrow Bay Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "14a4fa48-23b3-41cd-bc0e-2c0c243501ed",
    "lat": 28.573362,
    "long": 77.3929532,
    "place": "Yarrow Bay Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "14d4323e-d875-43ff-bb8f-9e4f04786200",
    "lat": 19.222523,
    "long": 72.8660529,
    "place": "Soos Creek Wine Cellars",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "14fd7f41-ad8b-40ff-9a43-53dffdef6807",
    "lat": 22.62511,
    "long": 88.41755,
    "place": "Dahlia Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "15033b98-132b-4c31-8385-a6d513e4a088",
    "lat": 17.50094,
    "long": 78.41087,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "1505e3b5-c19c-4041-bc7f-148a3ea39c9f",
    "lat": 19.11585,
    "long": 73.00211,
    "place": "The Tasting Room",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "1511517f-d806-42b1-a618-ae239f9c75c3",
    "lat": 28.7444186,
    "long": 77.0610211,
    "place": "Italianissimo Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "152f25f0-1bac-4990-8433-04b9411c77eb",
    "lat": 28.70528,
    "long": 77.19768,
    "place": "Kaspar's",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "1551456c-6a65-4dbf-8d5e-ba05d1adbe52",
    "lat": 28.5864383,
    "long": 77.3662833,
    "place": "Cutters Bayhouse",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "15a0dc7d-6193-43e7-b387-1030c3d96641",
    "lat": 28.7478765,
    "long": 77.1950637,
    "place": "Café Juanita",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "15ac6a9b-ac94-4955-9ca9-4e40519e5f9f",
    "lat": 23.055,
    "long": 72.63162,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "15ba3022-365d-4ccf-8127-7c877d026432",
    "lat": 26.85545,
    "long": 75.78532,
    "place": "Rover's",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "15f0b78b-e268-4236-9b86-551ac6739fab",
    "lat": 17.47695,
    "long": 78.54206,
    "place": "Mac and Jack’s Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "161d031f-ab65-4578-aba8-3add8fc01e69",
    "lat": 19.09803,
    "long": 72.84768,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "162db0d8-c5c2-4487-bcbc-43c3ffc6f902",
    "lat": 12.97817,
    "long": 77.68008,
    "place": "Typhoon",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "162de4f1-2b39-4f6c-ba9e-4bde64a4b0cc",
    "lat": 28.62437,
    "long": 77.09755,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1645d695-5571-4b83-8af1-58cb594148c6",
    "lat": 28.6872161,
    "long": 77.132511,
    "place": "Sea Garden",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "1659d2d7-d732-48c2-bc9b-5c1791a04a9c",
    "lat": 12.9801724,
    "long": 77.6777569,
    "place": "Salute of Bellevue",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "165fd35a-e4fe-4e7d-a132-a76dd50afa58",
    "lat": 12.83353,
    "long": 77.70163,
    "place": "Cutters Bayhouse",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "16caee8e-18c9-4815-b3aa-9fd4d9467ad3",
    "lat": 28.72665,
    "long": 77.16975,
    "place": "Desert Fire",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "16d54a8b-e3a2-42aa-a080-ed30a1dd3fdc",
    "lat": 28.68337,
    "long": 77.31311,
    "place": "Typhoon",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "171ef0da-a287-4b89-ab90-6f19042c3d6f",
    "lat": 28.615158,
    "long": 77.3460408,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1727a8df-a34f-43bd-b43e-5a4689f3bacb",
    "lat": 28.56433,
    "long": 77.24379,
    "place": "Rover's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1767c293-73c1-453d-93be-359c98e5f88d",
    "lat": 23.055,
    "long": 72.63162,
    "place": "Isabella Ristorante",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "17757ee0-0d52-4bd2-a782-6622e8580d3b",
    "lat": 28.52436,
    "long": 77.39213,
    "place": "Cutters Bayhouse",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "179f2cfa-8bd9-4393-b74c-fe614da0fd11",
    "lat": 13.03856,
    "long": 77.66203,
    "place": "Tap House Grill",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "17f97cb2-8bc7-4c30-97bb-d45d3b783145",
    "lat": 28.496,
    "long": 77.29483,
    "place": "Maggiano's Little Italy",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "17fa90e0-1509-499c-bf29-0901c014f8c4",
    "lat": 12.96508,
    "long": 77.71231,
    "place": "Bis On Main",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "188a4124-c347-4e1f-84d6-64054aa819ba",
    "lat": 28.6023246,
    "long": 77.0745418,
    "place": "Trader Vic's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "189ce3b6-343c-4ce1-af8b-66e1db029064",
    "lat": 28.44274,
    "long": 77.06475,
    "place": "Italianissimo Ristorante",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "18b1f117-ba24-4a40-8bca-05c52ec19f22",
    "lat": 28.53822,
    "long": 77.23057,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "18c2aad6-14aa-477d-aeae-9df64f3dab0a",
    "lat": 28.6786573,
    "long": 77.1903919,
    "place": "Typhoon",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "18c975dc-ffb5-4365-a920-13ca13eae7f1",
    "lat": 19.1204017,
    "long": 72.8694867,
    "place": "Desert Fire",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "18e48a73-fb3c-4823-84a5-fafd8f9d57dc",
    "lat": 28.59483,
    "long": 77.33853,
    "place": "Mac and Jack’s Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "19316c40-00f8-4af2-9bac-cb2ec97ef461",
    "lat": 28.57892,
    "long": 77.38176,
    "place": "Bis On Main",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "193c2f0c-cb5f-4701-8082-833dfaf23219",
    "lat": 18.53905,
    "long": 73.88657,
    "place": "Bis On Main",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "195ef422-b9b8-4b4a-b8e3-520218eabd31",
    "lat": 12.96479,
    "long": 77.65767,
    "place": "Space Needle",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "1960605b-881e-45e3-bd87-ebd5092ae743",
    "lat": 28.65041,
    "long": 77.27522,
    "place": "Chutney's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "19626d68-38f7-4471-8422-3da7b209e887",
    "lat": 28.63228,
    "long": 77.29643,
    "place": "Space Needle",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "197cbfc3-d962-494d-b5fa-09b75d29342f",
    "lat": 28.488992,
    "long": 77.0124588,
    "place": "Sans Souci",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "197d1530-08a4-46ea-a207-966e99bef3ef",
    "lat": 28.59892,
    "long": 77.29487,
    "place": "Il Fornaio",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "199db041-d987-482a-9f62-e32176d51676",
    "lat": 28.51553,
    "long": 77.20558,
    "place": "Manzana Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "19b7300c-14e3-4fa2-91d0-e70765448840",
    "lat": 28.46598,
    "long": 76.99941,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1a3c2e24-9039-4bb5-a760-722b2a34e9d8",
    "lat": 28.6407,
    "long": 77.36024,
    "place": "Assagio",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "1a6ae13f-3826-4ce0-a808-bf22277067b6",
    "lat": 13.00627,
    "long": 77.7202,
    "place": "The Tasting Room",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "1a91e192-11ee-455c-acbb-db2213d010c5",
    "lat": 13.03513,
    "long": 77.63381,
    "place": "Sea Garden",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "1abc2aa5-c31a-4986-96ed-75c02f08691e",
    "lat": 28.49912,
    "long": 77.29242,
    "place": "Café Juanita",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "1adb3d84-53cb-4a07-bbd0-5ae86eb5bc9f",
    "lat": 19.07847,
    "long": 72.88061,
    "place": "The Calcutta Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1aedc9dc-8008-431a-ad70-a3c3e828e29a",
    "lat": 28.5913358,
    "long": 77.3097536,
    "place": "Coho Cafe",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "1af3aedb-f305-4593-956d-983803b7777a",
    "lat": 19.1063,
    "long": 72.90011,
    "place": "Tap House Grill",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "1afc6dfa-609d-4a5b-a2ef-a056c1f1fe8b",
    "lat": 28.54901,
    "long": 77.23977,
    "place": "Yarrow Bay Grill",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "1b18d7ce-3885-45d3-8b70-202d2ca3c2de",
    "lat": 28.68309,
    "long": 77.36382,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1b1b5827-b2f9-4138-a1b1-37bccf7aede8",
    "lat": 19.05035,
    "long": 73.0746,
    "place": "Trader Vic's",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "1b26ba3d-2935-4a74-ac08-acb3ae4191a1",
    "lat": 26.84489,
    "long": 81.00478,
    "place": "Royal India",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "1b3eecad-8a54-418e-9d6c-faf4e858e15b",
    "lat": 28.4289604,
    "long": 77.3245185,
    "place": "The Calcutta Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1b451f0d-6fe9-4231-a7e2-ffde22f38942",
    "lat": 28.70804,
    "long": 77.17134,
    "place": "Trader Vic's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1b4c4181-7840-4a87-a413-5b4e5fe27f7e",
    "lat": 28.66956,
    "long": 77.33101,
    "place": "Trader Vic's",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1b539ef5-307c-4900-b827-9d356bdc0f39",
    "lat": 19.12092,
    "long": 72.84947,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1b5c4344-5168-4bef-92bd-8de962b50e86",
    "lat": 22.61394,
    "long": 88.43209,
    "place": "Café Juanita",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "1b649e28-338d-4feb-9d8a-53b48cbab243",
    "lat": 18.95525,
    "long": 72.8361,
    "place": "Dahlia Lounge",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1b6d733a-2b30-4b44-a416-528c703c7b86",
    "lat": 12.88103,
    "long": 77.6184733,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1bab1d81-e73f-4607-8e24-5785b78fe287",
    "lat": 19.10625,
    "long": 72.90011,
    "place": "Salish Lodge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1bb76078-97c6-4415-bf5f-a0d5b935b6f6",
    "lat": 28.45587,
    "long": 77.03994,
    "place": "Mac and Jack’s Brewery",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "1bc25dcb-95c5-4f9d-ab97-81c1609e2300",
    "lat": 19.1204017,
    "long": 72.8694867,
    "place": "The Tasting Room",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1bdd824b-8e16-4ae7-a96e-5c3a121b25f2",
    "lat": 28.36195,
    "long": 77.28375,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1c04a31f-20f2-4db7-8905-d598a8fbcbde",
    "lat": 28.6741257,
    "long": 77.12119,
    "place": "Wasabi Bistro",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "1c091740-c54c-4dde-8415-d6d442c4bd06",
    "lat": 28.46493,
    "long": 77.10331,
    "place": "Wasabi Bistro",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "1c0e7d05-8c85-4851-9783-bd1e45388582",
    "lat": 12.9780466,
    "long": 77.6799831,
    "place": "Trader Vic's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1c101161-2ed0-4809-ba72-35c052717cd8",
    "lat": 28.43645,
    "long": 77.05992,
    "place": "The Calcutta Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "1c20b5d2-0775-47ec-a5da-0556949243f9",
    "lat": 23.10745085,
    "long": 72.59124093,
    "place": "Wild Ginger",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1c2b5c61-3451-4477-b329-49b51db048bf",
    "lat": 28.55046,
    "long": 77.2904,
    "place": "Cutters Bayhouse",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "1c94a219-2adb-44a7-9f07-67812ac044d4",
    "lat": 28.64543,
    "long": 77.33909,
    "place": "Trader Vic's",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "1ca3c141-b819-4805-8f0a-a1573886320b",
    "lat": 19.14992,
    "long": 72.83159,
    "place": "Coho Cafe",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1cb73528-7391-4a6c-936b-b2ecddf55193",
    "lat": 12.96441,
    "long": 77.64945,
    "place": "Mac and Jack’s Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "1cea49de-2c0d-49ea-a191-310c4be38d7a",
    "lat": 28.73609,
    "long": 77.16919,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1d297993-9fea-4636-97c4-0114ad989541",
    "lat": 28.46953,
    "long": 77.29712,
    "place": "Dahlia Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "1db485ff-d8b7-44e2-adc5-e5e7fc0b6cd5",
    "lat": 17.4455,
    "long": 78.38695,
    "place": "Columbia Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1db4a8ef-7083-4210-a546-57b004c80dff",
    "lat": 28.491613,
    "long": 77.177957,
    "place": "Chutney's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1dcb0f5f-2526-4a6a-9345-2550ef44aeca",
    "lat": 18.9258,
    "long": 72.82673,
    "place": "Herb Farm",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "1dcf4b49-3be6-44a0-8f0f-d09357ee33d4",
    "lat": 28.6391824,
    "long": 77.0743123,
    "place": "Bis On Main",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "1dd39190-bbb1-4223-a0c2-6788f9e1dbee",
    "lat": 28.63748,
    "long": 77.36115,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "1df00a8b-9ffd-4013-b06c-ba856245a3af",
    "lat": 28.4994595,
    "long": 77.2343836,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1e26107b-9ae1-4c9b-a84b-eaf105c81761",
    "lat": 28.71844,
    "long": 77.27667,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1e34f617-5f42-4fc3-9bf7-0880c46837f2",
    "lat": 28.63221,
    "long": 77.30641,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1e3fa4be-20ad-4e1c-bbf4-07f793038998",
    "lat": 28.615158,
    "long": 77.3460408,
    "place": "Wasabi Bistro",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "1e4ddcd3-0bc9-45bd-a4e9-8d63128572b7",
    "lat": 19.18647,
    "long": 72.83428,
    "place": "Sea Garden",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1e7503d8-64e4-4379-82b0-e14b7e7ccfed",
    "lat": 28.56052,
    "long": 77.06249,
    "place": "Palomino",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1ea3492a-4d58-4495-9c24-4853b4791df8",
    "lat": 28.71235,
    "long": 77.14652,
    "place": "Uptown China Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "1ea89012-e85e-4f8f-957d-c43c564a4333",
    "lat": 12.9818786,
    "long": 77.6155783,
    "place": "Trader Vic's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1eb73ff0-a7da-4fec-b4f0-d2466f98c2cc",
    "lat": 22.58482,
    "long": 88.36457,
    "place": "Palomino",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1f00b689-f630-4c49-bfcc-0e7f6d69a69b",
    "lat": 28.5744387,
    "long": 77.2283659,
    "place": "Sea Garden",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "1f188155-8619-41fb-92c3-9893cd6c1335",
    "lat": 28.49912,
    "long": 77.29242,
    "place": "Kaspar's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1f7300a3-b7fe-4487-acb8-1ce01553ee97",
    "lat": 22.58682,
    "long": 88.3379,
    "place": "Typhoon",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1f76b50c-1a16-44c8-a647-67e2cef52b27",
    "lat": 12.91377,
    "long": 77.62043,
    "place": "Palomino",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1f815b54-7936-4b5a-9de3-05c7f0fc0270",
    "lat": 12.96775,
    "long": 77.67073,
    "place": "Coho Cafe",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "1f979d2a-ecc4-485b-942f-3beff0ffd3b5",
    "lat": 28.5196517,
    "long": 77.1633732,
    "place": "Rover's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "1fafaccc-2feb-451d-a2c7-54ace6f447b3",
    "lat": 28.59060733,
    "long": 77.30519486,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "1fb0b189-6206-42ad-8ba4-732db36601dd",
    "lat": 28.46611,
    "long": 77.08154,
    "place": "Soos Creek Wine Cellars",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "1fc6fb4b-c731-4be5-9eb5-6a57b3869df9",
    "lat": 12.95794,
    "long": 77.69448,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1fd7ef91-93f7-4f8b-9dde-4fb0fabf81be",
    "lat": 28.5071507,
    "long": 77.2357335,
    "place": "Desert Fire",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "1fe28eff-9c06-443d-9802-bbc55ea090b1",
    "lat": 13.0053621,
    "long": 77.6369078,
    "place": "Wild Ginger",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "1feea9a0-0760-4357-b0f2-c36b71d4530b",
    "lat": 28.62602,
    "long": 77.43373,
    "place": "Isabella Ristorante",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "201a843b-c9c3-4d18-886c-965101873b54",
    "lat": 28.5702442,
    "long": 77.3865089,
    "place": "The Tasting Room",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "203e5c1b-c37e-4309-aa47-fdc4d78d196c",
    "lat": 19.07597,
    "long": 72.82834,
    "place": "Soos Creek Wine Cellars",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "204c156f-6de2-4def-bf97-72373e6b15f7",
    "lat": 28.5071507,
    "long": 77.2357335,
    "place": "Sea Garden",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "2064ecb4-7220-45f2-ab56-0f666ca3c1f2",
    "lat": 28.70528,
    "long": 77.19765,
    "place": "Isabella Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "20e895de-1410-4416-8c15-d8606579b3c8",
    "lat": 18.62593,
    "long": 73.79936,
    "place": "Coho Cafe",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "212d13fc-597d-4f3d-9a11-117efee2b44a",
    "lat": 28.7428,
    "long": 77.11752,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "21434df2-2e90-47b4-87ee-0dad24ff3250",
    "lat": 28.6389693,
    "long": 77.0742992,
    "place": "Isabella Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "214709fa-f19f-4021-9949-2d04d6662368",
    "lat": 28.65326,
    "long": 77.30203,
    "place": "Uptown China Restaurant",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "217dd681-cfe3-407b-8c81-2f0c5021012f",
    "lat": 13.10026,
    "long": 80.28004,
    "place": "Mac and Jack’s Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "21a717a7-4d0a-45f1-acf6-6b5f1a1ebe5d",
    "lat": 28.65322,
    "long": 77.10017,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "21ab0560-1a2e-4653-9e03-bc35a9b1c8f5",
    "lat": 12.9211,
    "long": 80.15279,
    "place": "Herb Farm",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "21bb195d-3529-48be-8ee4-756dc1e74154",
    "lat": 28.4664,
    "long": 77.03019,
    "place": "Barking Frog",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "221328ae-7fe3-415d-8521-9fee2574ed3c",
    "lat": 28.60225,
    "long": 77.3415,
    "place": "Sans Souci",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "223a8b7b-8860-46c6-baea-47ede43fcd25",
    "lat": 28.6862494,
    "long": 77.2838548,
    "place": "Café Juanita",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "225a01a2-71a4-4460-b93c-fc63027f8a9a",
    "lat": 19.19084,
    "long": 72.82029,
    "place": "Market Cellar Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "2278a379-a7a5-4584-812f-a57da248bab6",
    "lat": 28.47231,
    "long": 77.05755,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "229ef1c3-6afe-47e3-9219-141090f67790",
    "lat": 23.055,
    "long": 72.63162,
    "place": "Isabella Ristorante",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "22aa7e68-eee0-4003-a4c5-4b3d96e7c30d",
    "lat": 28.633,
    "long": 77.30493,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "2359b54f-ae29-47af-bdb9-651afab11587",
    "lat": 28.49611,
    "long": 77.18939,
    "place": "Rover's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "235f03c9-d9c7-4500-b13c-e4cc4477eff4",
    "lat": 19.08858,
    "long": 72.84188,
    "place": "Bis On Main",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "23747419-9e71-4de4-a4fd-b0733680eb57",
    "lat": 12.99235,
    "long": 80.19781,
    "place": "Market Cellar Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "23a2b5cb-c23c-4b22-b629-7d4879eedd69",
    "lat": 28.6343227,
    "long": 77.0838727,
    "place": "Yarrow Bay Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "23fdc7ea-6c59-40a5-91dd-f9de09ef10f2",
    "lat": 12.9764861,
    "long": 77.7277921,
    "place": "Uptown China Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "244db142-deba-40ae-970a-cd91039cce83",
    "lat": 28.56008,
    "long": 77.0624,
    "place": "Café Juanita",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "249d56b9-f3cd-4365-ac12-a7474cb093da",
    "lat": 28.46903,
    "long": 77.00662,
    "place": "Redhook Ale Brewery",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "24db11b1-0cb2-4890-a99b-9f27e40a129d",
    "lat": 28.5789,
    "long": 77.38166,
    "place": "Redhook Ale Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "250254d0-4a71-4983-b7ac-4bb9d5f49f4a",
    "lat": 19.14232,
    "long": 72.84225,
    "place": "Space Needle",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "25046248-8478-40b4-a16f-c0348bbe2a22",
    "lat": 28.3998202,
    "long": 77.3556479,
    "place": "Trader Vic's",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "253f0c6c-11c5-417b-8de9-6e42de8101bc",
    "lat": 12.86444,
    "long": 77.65948,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "2553940e-7c24-402c-b158-b7d1d64dcfd2",
    "lat": 22.95982,
    "long": 72.61026,
    "place": "Buca di Beppo",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "255e3579-de54-473e-9e93-57a4bbe3e13f",
    "lat": 17.41209,
    "long": 78.55735,
    "place": "Chutney's",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "257a2e50-c779-48b4-a555-0ace0ba22dfb",
    "lat": 28.56023,
    "long": 77.21974,
    "place": "Assagio",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "25862971-e67e-4b84-a6a7-a6f92c492d3c",
    "lat": 22.4942097,
    "long": 88.3890481,
    "place": "Yarrow Bay Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "25bb47e3-0f0b-482e-9c55-b6c8039a7078",
    "lat": 28.6872161,
    "long": 77.132511,
    "place": "Sea Garden",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "25e6d391-33e6-4781-8f23-5c75ebf32d72",
    "lat": 12.96862,
    "long": 77.75177,
    "place": "Assagio",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "25f94739-eeea-42cb-86d0-277de0d522ff",
    "lat": 26.8794116,
    "long": 81.0065383,
    "place": "Chutney's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "26044174-a082-4eb9-9898-1eb1958729d6",
    "lat": 28.67149,
    "long": 77.08479,
    "place": "Rover's",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "26213104-2fc7-4878-aae0-402ff80e8307",
    "lat": 28.46247,
    "long": 77.04123,
    "place": "Assagio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "262b7a40-d9d3-4e48-9803-8cc873c39eb9",
    "lat": 28.55084,
    "long": 77.28964,
    "place": "Isabella Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "267e22b4-14cc-4d96-8d52-bf251a56877b",
    "lat": 28.71451,
    "long": 77.2132,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "269eeaff-2e28-4429-a660-796d3ba05f26",
    "lat": 28.4775554,
    "long": 77.0085042,
    "place": "Café Juanita",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "26d05d24-a240-4124-bbc4-81595ccfecd1",
    "lat": 19.08967,
    "long": 72.84204,
    "place": "Yarrow Bay Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "26e56e62-36f8-4cbc-b3cf-4868fdb3f6c8",
    "lat": 28.38944,
    "long": 77.0594,
    "place": "Columbia Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "2739624a-f719-44be-9a16-8d7e6ebd0c10",
    "lat": 28.371141,
    "long": 77.314353,
    "place": "Chutney's",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "27590c6e-a373-4816-8c39-938b5bf8b130",
    "lat": 28.491613,
    "long": 77.177957,
    "place": "Italianissimo Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "27aa4642-267e-4a1e-b753-097deb320b9e",
    "lat": 28.67361,
    "long": 77.22501,
    "place": "Typhoon",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "27b65ba8-c6f8-4b02-9601-1918d60c66f0",
    "lat": 19.03885,
    "long": 72.86404,
    "place": "Pike Brewing Company",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "281247ed-2243-4600-b06b-6699e36a849b",
    "lat": 28.63076,
    "long": 77.19854,
    "place": "Palomino",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "2837110a-7365-488a-9e6e-1af0ae455ecb",
    "lat": 18.98044,
    "long": 72.84131,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "28432f2d-e192-4bc1-88eb-5e0a9b5bc208",
    "lat": 28.57958,
    "long": 77.36599,
    "place": "The Calcutta Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "2851d868-1c1d-46a9-9bd4-9bb2adf4e688",
    "lat": 22.45722,
    "long": 88.30868,
    "place": "Italianissimo Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "2855f31e-5248-49c5-b34e-0bb7a09d5a37",
    "lat": 19.09803,
    "long": 72.84767,
    "place": "Redhook Ale Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "285aad45-c293-447a-89d2-5276d0579ee1",
    "lat": 28.68723325,
    "long": 77.13256941,
    "place": "Royal India",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "2861527b-aa2e-4ce4-8b42-675a5728be20",
    "lat": 28.70798,
    "long": 77.17134,
    "place": "Sans Souci",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "28679dbd-d3be-4d89-bf05-756063c144db",
    "lat": 17.39917,
    "long": 78.41779,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "28cdecb4-254e-43c8-b508-f7951b2f2f4b",
    "lat": 12.96818,
    "long": 77.50371,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "28cf0d95-64f2-4476-90a0-328dc3d8df2e",
    "lat": 26.90124,
    "long": 80.94609,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "29168251-f77a-42d3-bc61-64aea0756b8d",
    "lat": 22.574,
    "long": 88.46057,
    "place": "Yarrow Bay Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "295139b6-9c28-470b-9f86-0e8df8115abf",
    "lat": 23.055,
    "long": 72.63162,
    "place": "Chutney's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "29741662-1112-42d9-9b12-2ac1ffc30747",
    "lat": 18.5678333,
    "long": 73.9186317,
    "place": "Manzana Grill",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "298fad7b-17a9-4fca-a812-e9be04156f88",
    "lat": 26.9326741,
    "long": 75.7951361,
    "place": "Sea Garden",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "29936f98-5673-418a-ab28-2b477ff608b4",
    "lat": 26.89763,
    "long": 80.97842,
    "place": "Maggiano's Little Italy",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "29e2e5b7-ce6a-4c53-bc41-298a76271132",
    "lat": 28.6041,
    "long": 77.04788,
    "place": "Typhoon",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "2a4fedf1-3ad2-478e-ab28-803d0c1f8793",
    "lat": 28.57909,
    "long": 77.38177,
    "place": "Market Cellar Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "2a529d98-3fae-4ca5-9162-9988eca21265",
    "lat": 28.4312483,
    "long": 77.0868759,
    "place": "Barking Frog",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "2a541b82-3838-40f4-878a-915595b0481f",
    "lat": 28.48294315,
    "long": 77.01932575,
    "place": "Il Fornaio",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "2a7b1aa9-bbe9-4cd9-b467-615b20b46ae5",
    "lat": 28.63199,
    "long": 77.2774,
    "place": "Herb Farm",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "2b04ee67-e6d9-4316-8324-4d552e52197a",
    "lat": 28.47523,
    "long": 77.0598,
    "place": "Cutters Bayhouse",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "2b063423-1a1d-4d82-8512-9a7096092867",
    "lat": 28.64489,
    "long": 77.33851,
    "place": "Italianissimo Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "2b2fabda-d236-4f24-8bbd-b71ee03c8a23",
    "lat": 12.95628,
    "long": 77.69444,
    "place": "Soos Creek Wine Cellars",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "2b4f19a2-dfbb-4a09-9c7f-0271231c2e7e",
    "lat": 28.53337,
    "long": 77.2288,
    "place": "Yarrow Bay Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "2b4f2d44-3e61-4140-86a0-35baa381cf24",
    "lat": 28.5774424,
    "long": 77.0856252,
    "place": "Herb Farm",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "2b5fdd90-c019-417f-b710-0e07263356ce",
    "lat": 28.64277,
    "long": 77.35314,
    "place": "Desert Fire",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "2b754cbf-5d8c-461c-9eb7-acf318d75f63",
    "lat": 28.4275373,
    "long": 77.0386648,
    "place": "Kaspar's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "2baea2f8-0e59-4232-96ef-fa7e7f2410c2",
    "lat": 22.5473616,
    "long": 88.366274,
    "place": "Buca di Beppo",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "2bb03150-81d3-4a2e-8f3b-e9b0357923cc",
    "lat": 28.5440869,
    "long": 77.1236435,
    "place": "Il Fornaio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "2bc6fb08-90a7-4b1c-bbaa-9169819a3294",
    "lat": 19.2054544,
    "long": 72.9835246,
    "place": "Herb Farm",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "2bd88321-9cc9-437b-ab85-9949e1e84a26",
    "lat": 28.57406,
    "long": 77.33079,
    "place": "Desert Fire",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "2bee2832-aafb-4c9c-b364-b74ee5af6395",
    "lat": 28.62789,
    "long": 77.3025,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "2c94eb53-3ddf-4758-8fbc-6c232fc43610",
    "lat": 28.60808,
    "long": 77.33301,
    "place": "Salute of Bellevue",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "2cf47cdb-d9dc-4c7d-9f25-5a47d57b74b4",
    "lat": 28.39486,
    "long": 76.96735,
    "place": "Bis On Main",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "2cfc477f-a1f9-45b6-ac9c-60c8fdfcd1b7",
    "lat": 17.3707305,
    "long": 78.4858323,
    "place": "Café Juanita",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "2d3ae431-28e3-4eff-a209-40274e4359d1",
    "lat": 17.46659,
    "long": 78.34508,
    "place": "Yarrow Bay Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "2d6e7348-4fab-47f3-9e0f-c8cfcbc463e4",
    "lat": 28.65781,
    "long": 77.36207,
    "place": "I Love Sushi",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "2d889f95-d5bd-4bfe-9a24-7f793e502eac",
    "lat": 12.9503643,
    "long": 77.6160986,
    "place": "Il Fornaio",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "2d9b5b54-03a0-4c34-b135-0718bf8b9420",
    "lat": 28.45378,
    "long": 77.04104,
    "place": "Chutney's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "2da01755-e34a-4e7b-84eb-743d7d9c323e",
    "lat": 19.10642,
    "long": 72.89949,
    "place": "Isabella Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "2da793a6-c50a-431d-bd96-b6bcf3d7893b",
    "lat": 12.9818786,
    "long": 77.6155783,
    "place": "Buca di Beppo",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "2db5f03d-f9a6-4b59-88a9-7146e62c8dc2",
    "lat": 28.49539,
    "long": 77.17885,
    "place": "Sans Souci",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "2db7f804-5d31-4e92-8ff7-e4f0c6b22651",
    "lat": 28.6267384,
    "long": 77.2999215,
    "place": "Pike Brewing Company",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "2dfb5b3b-e271-44a7-8a48-0ea5289141ae",
    "lat": 28.6409383,
    "long": 77.2085383,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "2e653ffb-67cf-4bb2-9f3a-ad3f088dc6f6",
    "lat": 28.62325,
    "long": 77.29484,
    "place": "Pike Brewing Company",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "2e77b8fa-f1c5-48c6-8108-87256dde5584",
    "lat": 28.57518,
    "long": 77.1387,
    "place": "Soos Creek Wine Cellars",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "2ea3df85-483c-4bbf-857b-85771101ab50",
    "lat": 23.1081723,
    "long": 72.5914872,
    "place": "Uptown China Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "2ea8d4e7-4146-4b2e-9a04-e13a8faad09a",
    "lat": 19.14232,
    "long": 72.84226,
    "place": "Cutters Bayhouse",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "2ec7d440-c7c5-4eb2-bdc1-75fe6f628263",
    "lat": 28.50978,
    "long": 77.21148,
    "place": "Buca di Beppo",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "2eecc9d9-cd69-454f-a3b2-e3305e6512fb",
    "lat": 19.10556,
    "long": 72.90007,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "2f02eccf-a0a9-4b9a-b296-7cfe37e05549",
    "lat": 12.9214817,
    "long": 77.5197406,
    "place": "Rover's",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "2f4b03d7-5e6d-41c1-b2e3-567b94263fc7",
    "lat": 22.61808,
    "long": 88.40809,
    "place": "Kaspar's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "2f5c4bab-000b-4554-abbd-9bf573832e75",
    "lat": 19.07383,
    "long": 72.8234,
    "place": "Space Needle",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "2f627d89-1d2a-40a7-9206-10a3ad92fca1",
    "lat": 28.50173,
    "long": 77.37711,
    "place": "Redhook Ale Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "2f6efe26-b80e-4e5d-871e-39e41c6c07c3",
    "lat": 28.4390317,
    "long": 77.072324,
    "place": "Market Cellar Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "2fccaf2b-9b52-4e67-acf0-51ad0e46f36f",
    "lat": 22.56732,
    "long": 88.47365,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "2fe4af6f-576c-4e65-868b-bb1ed7e6da00",
    "lat": 28.63737,
    "long": 77.36103,
    "place": "Sans Souci",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "2feb15b2-8ee6-49c5-898f-d9af6d23c132",
    "lat": 28.7245,
    "long": 77.08583,
    "place": "Royal India",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "3045c59b-8e4c-4476-b7ab-9800cb718135",
    "lat": 28.63487,
    "long": 77.3576,
    "place": "Mac and Jack’s Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "304ccef5-33d5-45bd-b816-ab9f7b80bf8b",
    "lat": 28.65336,
    "long": 77.30207,
    "place": "Redhook Ale Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "306b0ac8-4f93-45bf-9609-715640ce42dd",
    "lat": 28.691952,
    "long": 77.2009275,
    "place": "Bis On Main",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "306c978a-43ab-4f3a-9b95-918bfc242d26",
    "lat": 28.67889,
    "long": 77.31291,
    "place": "The Calcutta Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "3082354f-c672-4f31-ab60-f5379241f125",
    "lat": 28.62632,
    "long": 77.43819,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "30a63c8b-5e30-4d36-b385-38e4edeeaced",
    "lat": 18.95503,
    "long": 72.83569,
    "place": "Salish Lodge",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "30ba2ca4-1caf-4b01-9015-b6247d47cae1",
    "lat": 28.50173,
    "long": 77.37711,
    "place": "Desert Fire",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "30c3a9e7-90f6-4de6-9df8-3bc159008a4c",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "Chutney's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "30c532e6-56e7-4012-88cb-3f37694592d2",
    "lat": 28.6330519,
    "long": 77.3122682,
    "place": "Market Cellar Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "30c58c2a-8f5a-4e02-9bbd-33c8f74f4ad6",
    "lat": 19.1964566,
    "long": 72.8757323,
    "place": "Dahlia Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "30fe40fd-623b-48b9-8689-e68bc6cd4dfc",
    "lat": 28.6558113,
    "long": 77.2863249,
    "place": "Wasabi Bistro",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "3103d021-8e45-488c-a1d5-1d0f802a8963",
    "lat": 28.5618225,
    "long": 77.2429593,
    "place": "Market Cellar Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "3116d2c3-7d67-4537-9f74-03ef3ff9094d",
    "lat": 28.59926,
    "long": 77.31817,
    "place": "Il Fornaio",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "3147f418-6e0d-4b30-99f7-3a1f68afb6af",
    "lat": 18.4644297,
    "long": 73.9047707,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "31505cb2-3bcd-4800-9ab3-dc521d44156a",
    "lat": 28.64909,
    "long": 77.37343,
    "place": "Café Juanita",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "318d704a-f4d6-4e3d-9a6c-0c8107aca6c9",
    "lat": 28.56878,
    "long": 77.16091,
    "place": "Rover's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "319853bf-79f5-4db6-9bdf-d4aa86668a81",
    "lat": 12.96616,
    "long": 77.64972,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "31bc83af-33de-415b-a31b-bf67c34ada1e",
    "lat": 19.222523,
    "long": 72.8660529,
    "place": "Bis On Main",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "321a899e-e66c-4617-b922-f0cc931f6526",
    "lat": 12.96479,
    "long": 77.65767,
    "place": "Buca di Beppo",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "321fa5a1-d29b-443c-b358-8168bfa2bc46",
    "lat": 22.4894531,
    "long": 88.3883636,
    "place": "Redhook Ale Brewery",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "323e6b0a-9780-453b-8dd6-6b4b028e54ac",
    "lat": 28.5789393,
    "long": 77.3816766,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "32923217-c43b-4f00-ad69-ac48eb5ee224",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "32d5705e-6b35-4bf1-afea-c3ab19ef1402",
    "lat": 28.49679,
    "long": 77.1802,
    "place": "Salute of Bellevue",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "32e6d8ba-63c3-44a4-8d40-e03f4c96b8e6",
    "lat": 26.9016,
    "long": 80.94722,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "330ddd9d-122e-4dc7-b537-e7c8dc133e3c",
    "lat": 22.61088,
    "long": 88.43383,
    "place": "Coho Cafe",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "331bb46d-0593-46f7-b851-d04424352868",
    "lat": 26.84361,
    "long": 81.00002,
    "place": "Typhoon",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "333a0e97-cf6a-4b44-bf11-6cb281c16af7",
    "lat": 28.5087,
    "long": 77.0192,
    "place": "Isabella Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "3348f949-9cc0-4962-b9ec-15ad6e51dc8c",
    "lat": 28.46594,
    "long": 77.00655,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "33516c36-2f4a-4c0f-8d86-1f70ae9f8a9d",
    "lat": 12.95794,
    "long": 77.69448,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "335d4dd6-fafd-4b59-88e3-42c3a6a73e3a",
    "lat": 28.50853,
    "long": 77.01992,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "3379c48e-d566-475e-a1f7-63e989d92e1a",
    "lat": 28.6023246,
    "long": 77.0745418,
    "place": "Buca di Beppo",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "33b11d5c-4303-4675-8b9d-77b4251c15dd",
    "lat": 28.4289604,
    "long": 77.3245185,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "33d0f678-d8ef-4b65-859d-72307f307002",
    "lat": 18.9641301,
    "long": 72.833509,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "3404bf2d-65ec-426c-838b-9e21f33287fe",
    "lat": 28.63545286,
    "long": 77.3646536,
    "place": "Salish Lodge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "340f5bae-7863-4edc-a45c-d95c53e77516",
    "lat": 12.96608,
    "long": 77.64968,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "34182873-1280-44bb-a875-9294ffeeb7f6",
    "lat": 12.92623,
    "long": 77.55769,
    "place": "Columbia Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "342c2076-c3ea-4e8c-8523-d9bfe6d1fc18",
    "lat": 18.49162,
    "long": 73.79225,
    "place": "Space Needle",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "34469f9f-00c9-46e2-b730-300f2fcba85d",
    "lat": 13.00438,
    "long": 77.7546,
    "place": "Café Juanita",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "3446b6f8-80bf-409c-a548-18e748a3e632",
    "lat": 26.77743,
    "long": 80.91196,
    "place": "Wasabi Bistro",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "34755b42-038f-40bb-93dd-92f5715a825b",
    "lat": 19.0136516,
    "long": 72.8190463,
    "place": "Maggiano's Little Italy",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "349cf720-ca7f-49ad-a99b-ca56984057b4",
    "lat": 28.59060733,
    "long": 77.30519486,
    "place": "Wild Ginger",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "351a1ae2-b42c-403c-97f8-75ff32645d62",
    "lat": 23.055,
    "long": 72.63162,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "3536f551-46ed-48ad-8ca0-9c7c187eaf61",
    "lat": 28.6862494,
    "long": 77.2838548,
    "place": "Kaspar's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "35530c46-2fe2-4fb4-8c14-115523c52001",
    "lat": 19.10578,
    "long": 72.9001,
    "place": "Herb Farm",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "3592395f-8fa7-43a0-9192-829719ce0055",
    "lat": 17.39917,
    "long": 78.41779,
    "place": "Typhoon",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "35bfff65-9dd3-4e11-99d4-befc304768ef",
    "lat": 13.0209426,
    "long": 77.6931412,
    "place": "Maggiano's Little Italy",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "35c58ab3-aedb-4dc3-ab48-cee81fa00459",
    "lat": 28.71113,
    "long": 77.14566,
    "place": "Chutney's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "35c60af2-274d-44ff-89bb-304a50ac4633",
    "lat": 28.68362,
    "long": 77.13209,
    "place": "Columbia Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "35e0281c-8b1d-4abd-b1a4-c76696061e8a",
    "lat": 12.91106,
    "long": 77.56642,
    "place": "Redhook Ale Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "3601e4b1-8e3a-440d-93ed-00caa67a9548",
    "lat": 28.51976,
    "long": 77.22669,
    "place": "Palomino",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "3635f58c-9007-4adc-be17-d70c3e594de9",
    "lat": 28.5635,
    "long": 77.38411,
    "place": "Café Juanita",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "364bf926-459e-43eb-aa38-f1cb01204b5b",
    "lat": 28.55015,
    "long": 77.29048,
    "place": "Yarrow Bay Grill",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "366cc565-886b-4874-94c3-0aeeaa41fa05",
    "lat": 28.69025,
    "long": 77.11722,
    "place": "Bis On Main",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "36987319-421d-48b7-933b-6efa5ffea2f2",
    "lat": 28.58194,
    "long": 77.37316,
    "place": "Herb Farm",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "36acc7c6-c562-438f-bd7f-4a02378d44eb",
    "lat": 28.45159,
    "long": 77.01877,
    "place": "Il Fornaio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "36bec04a-bcb2-41fe-8064-90de424c085e",
    "lat": 28.5789469,
    "long": 77.3818183,
    "place": "Palomino",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "36cb07d8-d2c6-4c4a-ba35-d83606d75cdd",
    "lat": 12.92625,
    "long": 77.55771,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "36d04532-ad8d-45de-bf81-3e42e43aabe0",
    "lat": 23.05567,
    "long": 72.65279,
    "place": "Il Fornaio",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "36eb944d-f2cf-4af6-99c1-f53fc9149dd8",
    "lat": 19.14232,
    "long": 72.84226,
    "place": "Buca di Beppo",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "37023a57-b9b1-459a-bbeb-06cd8842623d",
    "lat": 28.49119,
    "long": 77.09352,
    "place": "Coho Cafe",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "3709145d-5b49-4ca2-9fe0-d5a4887ff97a",
    "lat": 28.3937,
    "long": 76.96901,
    "place": "Cutters Bayhouse",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "37622ab7-041f-4378-8d3a-1de2d4d537cd",
    "lat": 28.65791,
    "long": 77.11902,
    "place": "Mac and Jack’s Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "37872ddc-2aa8-458c-9c53-1fcced9da068",
    "lat": 19.23653,
    "long": 72.97259,
    "place": "Buca di Beppo",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "379e45d0-dcc5-436c-91b6-f78eeb6a009a",
    "lat": 18.49148,
    "long": 73.79221,
    "place": "Salish Lodge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "37bbf28c-f468-43a8-82d4-aafdb90353a0",
    "lat": 28.68364,
    "long": 77.1318,
    "place": "Space Needle",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "37d91041-d4cd-4656-88d4-d95e26f3d79f",
    "lat": 28.71446,
    "long": 77.21315,
    "place": "Sea Garden",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "37e1e311-a207-4f83-afc1-27d32570836f",
    "lat": 28.5071507,
    "long": 77.2357335,
    "place": "Mac and Jack’s Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "37e54272-3aa5-455d-8e7c-565536e54e16",
    "lat": 13.0353963,
    "long": 77.5632253,
    "place": "Chutney's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "380ece39-95d9-4980-b451-d5221055875b",
    "lat": 28.56215,
    "long": 77.16303,
    "place": "Maggiano's Little Italy",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "38164db1-93fd-4304-9d7e-8cb721d48b8c",
    "lat": 26.89641,
    "long": 75.75577,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "3843937a-1d43-4aa9-b0f3-a2af1f59ab64",
    "lat": 28.54883,
    "long": 77.23945,
    "place": "Italianissimo Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "3843ce3b-fd77-4387-a069-07e8e0621c77",
    "lat": 18.4644297,
    "long": 73.9047707,
    "place": "Wasabi Bistro",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "38648c68-136a-472f-8a6b-84523b68c978",
    "lat": 28.62674,
    "long": 77.29992,
    "place": "Soos Creek Wine Cellars",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "38814012-dc93-455d-ab27-5357531511de",
    "lat": 22.63138,
    "long": 88.43539,
    "place": "Salute of Bellevue",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "38a3bd5e-8d9d-47f7-a551-7e3c4cf56f3f",
    "lat": 22.49438,
    "long": 88.33772,
    "place": "Dahlia Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "390c4ba6-b488-449f-942d-e04c9b39d9ef",
    "lat": 28.36543,
    "long": 77.33101,
    "place": "Herb Farm",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "39203b45-3f36-4fea-8d6b-1bb321098e85",
    "lat": 28.6491,
    "long": 77.37347,
    "place": "Pike Brewing Company",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "3952c3db-2fca-4576-9170-2c6aa3974ae9",
    "lat": 28.4499389,
    "long": 77.0167965,
    "place": "Dahlia Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "39995d22-d6bf-40a6-87c9-2bd0d7d49678",
    "lat": 28.36587,
    "long": 76.94371,
    "place": "Palomino",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "39e588fe-7e90-4d37-a0cb-fdb1f428ae27",
    "lat": 28.62632,
    "long": 77.43819,
    "place": "Manzana Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "39f992c2-8923-4bfc-b860-fda45d5bcc7e",
    "lat": 28.65334,
    "long": 77.30191,
    "place": "Rover's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "3a5c35e7-c13f-4902-bda8-050975deac0d",
    "lat": 26.810232,
    "long": 75.848136,
    "place": "Wasabi Bistro",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "3a6b3c0d-1389-403d-b8a4-fa61f40d462a",
    "lat": 28.57941,
    "long": 77.36585,
    "place": "The Tasting Room",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "3a6faf48-d817-4a5d-a66b-7364cdf3cd8a",
    "lat": 28.6385643,
    "long": 77.1836212,
    "place": "Italianissimo Ristorante",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "3a76d6b1-ae69-434b-8788-ea6a72dbf7b4",
    "lat": 28.62748,
    "long": 77.31105,
    "place": "The Tasting Room",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "3ab9cd60-6571-4a54-9ac6-093306e57e58",
    "lat": 28.4266895,
    "long": 77.0950288,
    "place": "Manzana Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "3abea622-6880-4160-bcf0-e2623585f59c",
    "lat": 28.512482,
    "long": 77.0645714,
    "place": "Royal India",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "3abf9470-4d0c-4d88-b2b3-32f581aa6b60",
    "lat": 28.5574,
    "long": 77.2833,
    "place": "Wasabi Bistro",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "3ac873a9-763e-42cf-81d5-0e94949331f0",
    "lat": 28.7065292,
    "long": 77.0985168,
    "place": "Royal India",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "3b09ab3e-46c7-4940-8870-c34c81e53a70",
    "lat": 28.5682,
    "long": 77.32411,
    "place": "Space Needle",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "3b5e0f3e-1f01-47cd-9940-f8bf9b3c10fd",
    "lat": 18.95514,
    "long": 72.8359,
    "place": "Space Needle",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "3b65c08f-1a58-4a58-a72e-14f5663830d5",
    "lat": 22.61808,
    "long": 88.40809,
    "place": "Wild Ginger",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "3bccc78f-e2a4-4626-9334-205bba96604a",
    "lat": 28.47706,
    "long": 77.0772,
    "place": "Maggiano's Little Italy",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "3bcd9c2d-7f67-4e8d-a692-b57599932997",
    "lat": 17.41209,
    "long": 78.55735,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "3bf44333-653f-4e6e-b808-1e949d3af922",
    "lat": 19.04886,
    "long": 73.07078,
    "place": "Sea Garden",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "3c38398d-2623-4e62-8670-153d69f41863",
    "lat": 26.86624,
    "long": 80.94349,
    "place": "Kaspar's",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "3c4656be-999b-4f07-baf3-98ff014bae53",
    "lat": 22.5009267,
    "long": 88.3476781,
    "place": "Salute of Bellevue",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "3c4b6052-e7be-4e6e-bdba-1ed0d3b486bd",
    "lat": 26.84334,
    "long": 80.88356,
    "place": "Dahlia Lounge",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "3c5bcfcd-be26-446b-aab4-ab129458fe37",
    "lat": 22.56656,
    "long": 88.47148,
    "place": "Columbia Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "3c6f7f17-c219-4909-871d-a5cd7520758e",
    "lat": 28.4390317,
    "long": 77.072324,
    "place": "Cutters Bayhouse",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "3ccfdf20-94a7-4ba2-9028-06e7255f1576",
    "lat": 19.07391,
    "long": 72.82409,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "3cf1b2c2-9502-497a-89ee-148c79cb0d2a",
    "lat": 26.80406,
    "long": 80.9079,
    "place": "Dahlia Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "3d55e8bc-0667-4498-a797-a3129b324048",
    "lat": 28.4266066,
    "long": 77.085132,
    "place": "Kaspar's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "3d5fbe52-a9f2-4cf6-b827-b8cd981ff0b2",
    "lat": 28.4559084,
    "long": 76.9848706,
    "place": "Salute of Bellevue",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "3d64e186-064a-42b4-9805-108254a14d06",
    "lat": 28.63277,
    "long": 77.31228,
    "place": "Maggiano's Little Italy",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "3d7c6e92-2010-4174-a742-46b740fb26f6",
    "lat": 28.49703,
    "long": 77.14322,
    "place": "I Love Sushi",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "3d8a1799-c3bb-4342-98d6-ea6b6687a640",
    "lat": 28.51978,
    "long": 77.22668,
    "place": "Herb Farm",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "3db674bf-2ab3-4b1f-bc5a-d011dabf9f1f",
    "lat": 28.71816,
    "long": 77.17764,
    "place": "Trader Vic's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "3dcdb5b4-4a0f-4d7e-909c-a3628dc88d73",
    "lat": 28.55696,
    "long": 77.06736,
    "place": "Coho Cafe",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "3dde6960-abd9-4abe-8450-53e240defeae",
    "lat": 12.88103,
    "long": 77.6184733,
    "place": "Yarrow Bay Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "3de007a6-5a79-4325-949d-6a23d6417622",
    "lat": 28.62761,
    "long": 77.31148,
    "place": "Redhook Ale Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "3df4de76-b5c1-4ca9-9202-57611bda65d7",
    "lat": 28.45413,
    "long": 77.04213,
    "place": "Rover's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "3e03e6fd-b51e-45bf-b78c-a4b9b6daa230",
    "lat": 19.1076393,
    "long": 72.9995728,
    "place": "Mac and Jack’s Brewery",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "3e0c4d2b-b77c-45fb-9969-3cab2aa82e18",
    "lat": 19.19084,
    "long": 72.82029,
    "place": "Cutters Bayhouse",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "3e1429a5-e30d-4ddf-983b-bc40416e0195",
    "lat": 28.55707,
    "long": 77.06736,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "3e56cbd5-f712-4754-9520-ef2c4bb0cd59",
    "lat": 28.6366505,
    "long": 77.2394328,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "3e802471-937b-415c-bad1-6daa18c964fe",
    "lat": 28.57894,
    "long": 77.38168,
    "place": "Dahlia Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "3e8454b6-da6e-4943-86c5-71b0a396847a",
    "lat": 19.1076393,
    "long": 72.9995728,
    "place": "Mac and Jack’s Brewery",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "3e8ac542-75e6-4202-b471-285944f9e75c",
    "lat": 28.6328893,
    "long": 77.2720913,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "3e8cc374-18a6-4565-b11f-bf7b5210961a",
    "lat": 28.45378,
    "long": 77.04098,
    "place": "Desert Fire",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "3e9b4e33-622b-4866-be11-e1f96cdcf08b",
    "lat": 28.634539,
    "long": 77.3530191,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "3ec9f179-cd4e-4383-be93-ea108f4dd87a",
    "lat": 28.64476,
    "long": 77.4204,
    "place": "Wasabi Bistro",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "3edb6828-f780-4d48-b393-ee22d5f19018",
    "lat": 28.63622,
    "long": 77.18045,
    "place": "Buca di Beppo",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "3ef37dbc-dbc0-4202-851d-70e89b7a8fd7",
    "lat": 28.48348,
    "long": 77.01868,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "3f145b98-3de8-4860-9dfc-e223e8400d73",
    "lat": 28.66985,
    "long": 77.32838,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "3f1a9b16-07d2-4948-8f52-01d20dce5bb9",
    "lat": 28.49679,
    "long": 77.1802,
    "place": "Sea Garden",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "3f3a8e0c-69a5-4029-aed8-52bcb5145984",
    "lat": 12.9214817,
    "long": 77.5197406,
    "place": "Rover's",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "3f415a42-4bdc-42b7-8f98-72647d3ee2d7",
    "lat": 18.49148,
    "long": 73.79221,
    "place": "Columbia Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "3f4409b2-f519-43ed-8637-5489b0686fb5",
    "lat": 28.7106142,
    "long": 77.1401031,
    "place": "Café Juanita",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "3f90a5b8-be15-46d9-a95b-048a4c6322e3",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "Rover's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "3f9ae8fd-29dd-492b-b041-2718abd80f64",
    "lat": 22.61078,
    "long": 88.40862,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "3fb7d02c-1abb-4bf8-9c93-8ffce3cfa3d2",
    "lat": 28.6161768,
    "long": 77.04618227,
    "place": "Assagio",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "3fbd3535-e656-41ac-aed1-e69750e46040",
    "lat": 23.10912,
    "long": 72.58794,
    "place": "Chateau Ste Michelle Winery",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "3ffc04ef-cffb-4660-bb2e-21ff60113d7d",
    "lat": 26.89766,
    "long": 80.9784,
    "place": "Italianissimo Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4050b030-8af9-4a06-b38c-36443b48ded8",
    "lat": 23.055,
    "long": 72.63162,
    "place": "Rover's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "4078b3a9-dee0-4f9e-a240-0885c5c87f32",
    "lat": 28.51978,
    "long": 77.22669,
    "place": "Salish Lodge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "40793ac5-0769-4bcf-91d0-c01ef3cf6e23",
    "lat": 28.5419011,
    "long": 77.3348451,
    "place": "Desert Fire",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "408d47e4-5a21-420e-b082-de6faf2e031f",
    "lat": 22.4942097,
    "long": 88.3890481,
    "place": "Coho Cafe",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "40a5edfd-6995-41ab-9b88-a9ea14d247eb",
    "lat": 28.5916616,
    "long": 77.3750196,
    "place": "Il Fornaio",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "40ab0830-dc69-4798-be77-a3272884ff4d",
    "lat": 28.45994,
    "long": 77.08887,
    "place": "Soos Creek Wine Cellars",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "40b862cf-3ff2-476a-8601-33d8d66c5486",
    "lat": 23.055,
    "long": 72.63162,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "40c48424-702d-4901-b9e7-43dfa3b07442",
    "lat": 28.58655,
    "long": 77.36636,
    "place": "Isabella Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "41637746-d1c6-4497-9d36-5ca7cc502214",
    "lat": 26.83614,
    "long": 80.88254,
    "place": "Sea Garden",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "41b39925-41de-4378-950c-14a00b2c22f1",
    "lat": 26.8343,
    "long": 80.92519,
    "place": "Uptown China Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "41bc7a47-cffe-496b-9da4-8099af15a54e",
    "lat": 17.464668,
    "long": 78.3095571,
    "place": "Trader Vic's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "41dc4932-a645-40a2-a129-a036be1805f5",
    "lat": 28.5850391,
    "long": 77.356831,
    "place": "Buca di Beppo",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4200cceb-df7e-4466-8ce0-ad681af5a44f",
    "lat": 12.8831041,
    "long": 77.5695627,
    "place": "Dahlia Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "42020a65-1bc0-4f70-9679-bf8b7427c9bb",
    "lat": 12.83353,
    "long": 77.70163,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "420f3790-f8f7-49bc-ab66-254d86e6dc88",
    "lat": 28.47674,
    "long": 77.04774,
    "place": "Bis On Main",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "42534e0e-cc9d-4ed4-96b7-ab9dd2b5fb9c",
    "lat": 28.70366,
    "long": 77.10371,
    "place": "Italianissimo Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "42549233-27b4-4b88-b553-e5946415c934",
    "lat": 12.88319,
    "long": 77.56968,
    "place": "Mac and Jack’s Brewery",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4255d490-21ad-42ec-93b1-b5b79626e776",
    "lat": 28.66784,
    "long": 77.14246,
    "place": "Barking Frog",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "42659f13-2406-48a6-a2b2-ac0b9f5a88ff",
    "lat": 17.39917,
    "long": 78.41779,
    "place": "Redhook Ale Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "42ac4fee-16cf-40b3-b027-8c6468019f4b",
    "lat": 28.6330519,
    "long": 77.3122682,
    "place": "Manzana Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "42cdb418-4753-4ce8-9bc0-181a320e8a29",
    "lat": 28.400489,
    "long": 77.0917846,
    "place": "Royal India",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "42d3da3d-a9bf-41a6-8f74-853a4450e3d7",
    "lat": 13.02201,
    "long": 77.68798,
    "place": "Barking Frog",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "42de5cc2-8445-4c7d-81f1-95169014e014",
    "lat": 22.60214,
    "long": 88.37627,
    "place": "Salish Lodge",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "430ee0f3-e93a-462e-a662-bcafd32e019f",
    "lat": 18.9548517,
    "long": 72.8314037,
    "place": "Trader Vic's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "43133566-dda7-4e3a-9dc2-dd1eaa193f2d",
    "lat": 19.10578,
    "long": 72.9001,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "432ac40c-c02f-40b3-b782-7ab4802b9cf3",
    "lat": 23.05608,
    "long": 72.6522,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "432c82d5-59f0-47c4-84ca-b8ffc6d3b8a2",
    "lat": 28.69225,
    "long": 77.32896,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "4337f449-770b-4ef6-8469-46853c139280",
    "lat": 22.4609755,
    "long": 88.3102533,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4366b0cc-1ba4-4642-82ae-535de7d0f0f7",
    "lat": 22.48861,
    "long": 88.38957,
    "place": "Assagio",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "43a710d6-020a-4271-966e-c7002eb59e53",
    "lat": 28.63468,
    "long": 77.08465,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "43e16382-1681-4922-9ee4-384756320dca",
    "lat": 26.88754,
    "long": 80.95903,
    "place": "Trader Vic's",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "43e2b256-b19b-4a2e-b7e2-d11fdc7d87f5",
    "lat": 28.49591,
    "long": 77.08446,
    "place": "Il Fornaio",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "441363c5-6111-4682-b2f2-28dbd5b863c3",
    "lat": 22.61389,
    "long": 88.43273,
    "place": "Rover's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "445090f8-78ed-41d9-8296-02748f215df6",
    "lat": 28.63013,
    "long": 77.30676,
    "place": "Columbia Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "445638ff-7334-47cb-8d65-07eccb4f6790",
    "lat": 12.95794,
    "long": 77.69448,
    "place": "Redhook Ale Brewery",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "446ed686-18c5-48a1-941e-fb37097fa3b7",
    "lat": 12.96714,
    "long": 77.74104,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "448318f2-c33c-4c7f-bf30-f18fabe5a65d",
    "lat": 28.47873,
    "long": 77.5149,
    "place": "Italianissimo Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "44957535-80ba-4000-9c3d-89c169f45a2e",
    "lat": 13.1190665,
    "long": 77.5799718,
    "place": "Kaspar's",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "44963801-6d3f-4fdb-baf7-ff00aa1d6271",
    "lat": 26.81023,
    "long": 75.84814,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "44baa6eb-12e5-4205-ac8c-332adc7072fc",
    "lat": 28.72336,
    "long": 77.08426,
    "place": "Mac and Jack’s Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4506751e-3cb4-4e40-918d-cef755ac9593",
    "lat": 28.6218186,
    "long": 77.1222418,
    "place": "Trader Vic's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "450ec432-007a-4daa-98e3-6c56688fd479",
    "lat": 13.03856,
    "long": 77.66203,
    "place": "Tap House Grill",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4516a28e-60ef-4555-8c45-0bd4038ae54b",
    "lat": 28.50173,
    "long": 77.37711,
    "place": "Sea Garden",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "45173ec7-1cab-42a7-9b5e-c46c8318789e",
    "lat": 28.71201,
    "long": 77.15297,
    "place": "Rover's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "451da96e-a8cc-455c-984d-8c3c41f16c8d",
    "lat": 28.51289,
    "long": 77.20697,
    "place": "Royal India",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "455bfe23-424a-44ba-9d51-087ad694ae53",
    "lat": 28.6330519,
    "long": 77.3122682,
    "place": "Soos Creek Wine Cellars",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4567e1aa-ea39-4825-8b19-428ea73035ea",
    "lat": 22.95995,
    "long": 72.60943,
    "place": "Café Juanita",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "45785112-7a4e-4329-9f98-8356e1cc2975",
    "lat": 12.97776,
    "long": 80.25913,
    "place": "Tap House Grill",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "45d5e2c2-489b-4bbb-88f3-7119557e68fc",
    "lat": 28.52436843,
    "long": 77.3921271,
    "place": "Bis On Main",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "45dd4767-6d8e-4fa4-8cf6-32cc12d88c02",
    "lat": 23.03025,
    "long": 72.58826,
    "place": "Trader Vic's",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "46039328-489f-45f8-a671-ba9bab7dced7",
    "lat": 22.45725,
    "long": 88.30882,
    "place": "Café Juanita",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "46050cdb-d810-4438-b0cb-ad6ff756ad60",
    "lat": 12.88103,
    "long": 77.6184733,
    "place": "Coho Cafe",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "46098ab8-bc6b-4647-bbfa-b7063b2e1d6d",
    "lat": 28.6330519,
    "long": 77.3122682,
    "place": "Buca di Beppo",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "461fab30-2c26-446c-8475-7e5ac99e0e99",
    "lat": 28.36209,
    "long": 77.28375,
    "place": "Yarrow Bay Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "46472807-2948-4a80-9375-63f29c3618ef",
    "lat": 12.91106,
    "long": 77.56626,
    "place": "Yarrow Bay Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "46a637ec-556e-4431-95bb-2bbb89d101c2",
    "lat": 19.20601075,
    "long": 72.96031713,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "46aa6fb0-5170-4304-ac4d-076a1eee1647",
    "lat": 18.53895,
    "long": 73.88649,
    "place": "Soos Creek Wine Cellars",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "46edb6ff-f758-474c-bc67-4989b776046b",
    "lat": 13.02094,
    "long": 77.69313,
    "place": "Wasabi Bistro",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "46f7d9ef-21de-4b35-9567-645a1a4a0eb3",
    "lat": 28.5196517,
    "long": 77.1633732,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "46fc8224-705c-4de1-9664-095add0ccd84",
    "lat": 28.56319,
    "long": 77.21441,
    "place": "Typhoon",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "47041baa-64c0-4345-ae8d-9c31bea4fd19",
    "lat": 28.4075365,
    "long": 77.3260768,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "471e1ed6-7afa-467e-8996-afdd6147acdf",
    "lat": 28.69335,
    "long": 77.33122,
    "place": "Maggiano's Little Italy",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "475a6c0b-bd76-4ca8-a243-4ea892e4341e",
    "lat": 28.43082,
    "long": 77.10974,
    "place": "Italianissimo Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "477dbc0a-8554-48c5-aff6-06a9e689dbd2",
    "lat": 26.8826179,
    "long": 80.9983378,
    "place": "Wasabi Bistro",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "478c8240-ea29-4a4d-a41e-e53f4b9efca1",
    "lat": 12.8993548,
    "long": 77.69580388,
    "place": "Chutney's",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "47a20c11-f5da-41dd-ab35-61ed50786196",
    "lat": 23.03025,
    "long": 72.58826,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "47a2858b-eaab-4d74-a8d6-df4e7dffe082",
    "lat": 28.5789,
    "long": 77.38171,
    "place": "Bis On Main",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "47a29e3b-622b-436d-984c-088a2e298389",
    "lat": 28.6267384,
    "long": 77.2999215,
    "place": "Isabella Ristorante",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "47b204d4-d80d-452b-91ec-ec83f5ead3c5",
    "lat": 22.59095,
    "long": 88.3415,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "47cca6d1-f896-49c1-9eb5-5a783b0e603a",
    "lat": 23.1081723,
    "long": 72.5914872,
    "place": "Salute of Bellevue",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "47fc4869-be39-44d2-b850-3b56393f2f0b",
    "lat": 28.70528,
    "long": 77.19765,
    "place": "Assagio",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "4821acbe-ac26-42ef-bd05-54dbbadb5bf5",
    "lat": 28.68961,
    "long": 77.11867,
    "place": "Soos Creek Wine Cellars",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "48863b2d-3dbc-4704-b198-74b6713ad8ec",
    "lat": 28.6323,
    "long": 77.27341,
    "place": "Space Needle",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "48a01400-aae0-4ed7-926c-885ca0c0a3f5",
    "lat": 26.87431,
    "long": 80.90628,
    "place": "Sans Souci",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "48c4641f-aaad-4eb2-8374-a06c0bb1a1ee",
    "lat": 26.8559472,
    "long": 80.8869147,
    "place": "Sans Souci",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "48f2f18b-2f24-48fa-9b37-bcc045073137",
    "lat": 28.51976,
    "long": 77.22669,
    "place": "Barking Frog",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "48ff3842-92d0-437b-b6ce-f316dff4db1c",
    "lat": 19.0775351,
    "long": 72.8602718,
    "place": "Columbia Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4940e429-f981-4f60-a597-73e86fac2fdf",
    "lat": 17.5062,
    "long": 78.33048,
    "place": "Salute of Bellevue",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "495a44dc-400d-4ce0-acb8-29d5fba79306",
    "lat": 28.70528,
    "long": 77.19767,
    "place": "Uptown China Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "49ad0552-0d14-4541-9210-43a91acbdfcb",
    "lat": 28.47681,
    "long": 77.04771,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "49ad1f25-9a10-4a88-88ad-13c4b096857c",
    "lat": 28.36543,
    "long": 77.33101,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "49d1e7d3-1c3f-4f9d-8834-de92b9e4cb07",
    "lat": 18.98044,
    "long": 72.84131,
    "place": "Palomino",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "49e9a103-2efa-44ee-84b1-4f886e5db031",
    "lat": 28.63659,
    "long": 77.33837,
    "place": "Columbia Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4a02e3d1-e047-456e-a03d-afaacd75baf0",
    "lat": 28.65463,
    "long": 77.35094,
    "place": "Kaspar's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "4a1346d2-26c4-44ce-8b2c-725c21e3dd5d",
    "lat": 19.1784515,
    "long": 72.8530593,
    "place": "Pike Brewing Company",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4a3f91bf-e29f-41a9-9ef6-095f0dc78dfe",
    "lat": 28.37163,
    "long": 77.31435,
    "place": "Herb Farm",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "4a5ae0d0-cd20-4fca-be0c-ef636d551993",
    "lat": 28.4559084,
    "long": 76.9848706,
    "place": "Herb Farm",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "4a6f2958-14a6-4f99-93d3-ada133d26c31",
    "lat": 23.01824,
    "long": 72.56138,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4af33d5e-6bda-442d-bc5a-6ba43741b017",
    "lat": 28.71068,
    "long": 77.1401,
    "place": "Royal India",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4b18ff3a-940a-4036-9fcd-5eb1392596c5",
    "lat": 18.95533,
    "long": 72.83604,
    "place": "Coho Cafe",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "4b1aa72c-d55c-40c6-927d-abccdb7c2e5f",
    "lat": 28.36039,
    "long": 77.32584,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4b244cd0-3071-4a85-ba89-29f04a58fb2d",
    "lat": 23.02716,
    "long": 72.58631,
    "place": "Mac and Jack’s Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4b44f4a4-f009-4a10-a8dc-70a91941398c",
    "lat": 12.9955,
    "long": 77.67959,
    "place": "Italianissimo Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "4b6420ca-05eb-431a-a3f8-15979ec24d7a",
    "lat": 28.6126196,
    "long": 77.4242954,
    "place": "Kaspar's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4b8ac4b6-591b-43e8-a117-ad86baecd5df",
    "lat": 28.64333,
    "long": 77.3354,
    "place": "Sea Garden",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4b8fb43a-42d4-406a-8397-1c1a81a2f279",
    "lat": 22.57887,
    "long": 88.38853,
    "place": "Buca di Beppo",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "4bb5c2c3-1920-4fe7-b008-2ff3bcef1086",
    "lat": 23.10912,
    "long": 72.58817,
    "place": "Chateau Ste Michelle Winery",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4bd4d491-6e63-4a40-8e2d-ccc98e5dbe55",
    "lat": 28.40872,
    "long": 77.03516,
    "place": "I Love Sushi",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4bdf2347-71bc-48cf-bab8-b4ae05ec8d9c",
    "lat": 12.9214817,
    "long": 77.5197406,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "4c92eb77-0177-4aa2-a9dd-c674e80601d0",
    "lat": 28.6343227,
    "long": 77.0838727,
    "place": "Redhook Ale Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "4c9fa46d-f3b8-46a0-8521-6fde333e5329",
    "lat": 28.6366387,
    "long": 77.2791422,
    "place": "Bis On Main",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4cb024ef-97c9-4d31-af17-b4177adac0b7",
    "lat": 28.6379272,
    "long": 77.3615437,
    "place": "Barking Frog",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "4cb78f68-0cd4-447f-bcf3-7690f26226b9",
    "lat": 28.68309,
    "long": 77.36382,
    "place": "Royal India",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "4ccf257c-9f71-4a5a-868e-f251d431678f",
    "lat": 28.63925,
    "long": 77.37125,
    "place": "Manzana Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "4ce97cbf-a6b8-4756-908f-975f60b2eec4",
    "lat": 26.926095,
    "long": 75.7906767,
    "place": "Uptown China Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "4d09e2e7-10c0-4e23-bdd7-a54267d81669",
    "lat": 28.66956,
    "long": 77.33101,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "4d15d081-36fb-4760-9885-f52adbc9ac0a",
    "lat": 22.50123,
    "long": 88.34768,
    "place": "Redhook Ale Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4d23f1f3-b1c0-4950-9197-ce134375e666",
    "lat": 28.55061,
    "long": 77.29159,
    "place": "Assagio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4d3cc165-f726-40ec-b7cb-9b9995dfbe7e",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4d52014a-cdea-427f-8881-747967d9222c",
    "lat": 28.57938,
    "long": 77.36582,
    "place": "Tap House Grill",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4d68e89e-1ee3-4639-ac7d-438ccabaf572",
    "lat": 26.85545,
    "long": 75.78532,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4d6ecaab-1883-44e2-aeee-68468dd6a3dd",
    "lat": 28.5661,
    "long": 77.22022,
    "place": "Uptown China Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "4d731235-b3cf-4830-8a39-dee3bdad4cfe",
    "lat": 22.52531,
    "long": 88.39469,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4d73e7a6-8033-4f89-b3fb-39ba65508f75",
    "lat": 28.5661,
    "long": 77.22022,
    "place": "Kaspar's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4d84c491-b6b6-4e88-902f-d315b4261986",
    "lat": 26.84218,
    "long": 80.93637,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4dce1534-b17f-40c6-8524-5f715fc34bff",
    "lat": 12.96842,
    "long": 77.73992,
    "place": "Salute of Bellevue",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "4de63ccb-16cd-48e6-ad08-22fad11c1405",
    "lat": 19.222523,
    "long": 72.8660529,
    "place": "The Tasting Room",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "4e06a565-2294-489b-9939-1fa1bbd0a2fc",
    "lat": 28.4709304,
    "long": 76.9965945,
    "place": "Maggiano's Little Italy",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4e0bad6c-3024-4b43-bfc1-840bf285833d",
    "lat": 28.41947,
    "long": 77.32594,
    "place": "Sans Souci",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "4e0e6691-8641-4856-add7-2d59d5cbb7a5",
    "lat": 22.55278,
    "long": 88.35549,
    "place": "Kaspar's",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "4e3e55bc-967d-4159-adf1-f93497de4b2f",
    "lat": 28.56729,
    "long": 77.25262,
    "place": "Manzana Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4e8ac124-e599-499d-9260-ec06e3d83944",
    "lat": 28.42324,
    "long": 77.06349,
    "place": "Maggiano's Little Italy",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "4e9987b4-979e-4def-94d3-ef241826bc1b",
    "lat": 26.77743,
    "long": 80.91196,
    "place": "Wasabi Bistro",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4f13d407-1662-44a5-b4cd-a05012f23107",
    "lat": 23.10813,
    "long": 72.59151,
    "place": "Il Fornaio",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "4f29ae3f-2efa-4c04-a898-e6398cd98159",
    "lat": 28.4297506,
    "long": 77.0945954,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "4f2fc864-ce8f-4dcb-81a2-87948f9d6bd2",
    "lat": 23.01855,
    "long": 72.56148,
    "place": "Desert Fire",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4f45855c-989b-4a7b-845c-c89e6d699daa",
    "lat": 13.02448,
    "long": 77.69728,
    "place": "Manzana Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4f516bf9-16d1-4f8a-8beb-4718952e3144",
    "lat": 28.56373,
    "long": 77.21215,
    "place": "Royal India",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4f570d3a-1a34-4d06-8a2f-a08eaeea59a5",
    "lat": 22.5473616,
    "long": 88.366274,
    "place": "Cutters Bayhouse",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "4f80d0f8-872f-4c44-ac51-7e29619ae7eb",
    "lat": 28.49653,
    "long": 77.14406,
    "place": "Sea Garden",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "4f86d969-7551-4c38-8a8a-78d0685088b2",
    "lat": 28.62777,
    "long": 77.3025,
    "place": "Barking Frog",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "50370a39-7985-4f7e-acc6-69615f29c00b",
    "lat": 28.7165358,
    "long": 77.1260448,
    "place": "Chutney's",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "504cd0e2-1cb0-4314-9f7c-98fccf380172",
    "lat": 19.26484,
    "long": 72.9654,
    "place": "Salute of Bellevue",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "5066cb45-52ae-4b63-8f95-7137307bfa15",
    "lat": 23.06007,
    "long": 72.65291,
    "place": "Sea Garden",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "506e57ae-d004-42dd-a74b-1102363b5e03",
    "lat": 18.9250649,
    "long": 72.8269712,
    "place": "Salute of Bellevue",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "507de758-a039-4afc-8efe-df3fd900acb1",
    "lat": 28.56729,
    "long": 77.25262,
    "place": "Desert Fire",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "50ffc5ee-46a3-421f-9704-7d5c16cade14",
    "lat": 28.63454,
    "long": 77.35302,
    "place": "Space Needle",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "511b7ff8-c2ec-4bec-9870-6cfe5d621aa2",
    "lat": 22.572,
    "long": 88.35695,
    "place": "Pike Brewing Company",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "5126268a-30f6-4b90-826c-ee03381d5fab",
    "lat": 22.63138,
    "long": 88.43539,
    "place": "Herb Farm",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "5127c61b-737a-49ae-84cd-34150e01d139",
    "lat": 28.5664,
    "long": 77.35936,
    "place": "Salish Lodge",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "51921707-1e91-4119-8591-13ed967ab1ff",
    "lat": 28.42392,
    "long": 77.10358,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "51aed74b-6da3-44c2-8a23-05551e9700b4",
    "lat": 28.615158,
    "long": 77.3460408,
    "place": "Manzana Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "51d9f641-f972-41cf-b769-9167f60154d4",
    "lat": 28.6346953,
    "long": 77.0846505,
    "place": "Herb Farm",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "520a9514-3605-439f-a159-84164abb8d65",
    "lat": 17.44633,
    "long": 78.38704,
    "place": "Soos Creek Wine Cellars",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "522a6ae5-9644-4fee-a6ee-8000ac0263ba",
    "lat": 28.70798,
    "long": 77.17134,
    "place": "Sea Garden",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "523a9fc5-3748-4ff7-b04e-b4a1762f9586",
    "lat": 19.07391,
    "long": 72.82409,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "52714eee-18d5-4b69-b88c-d82cddf00ce7",
    "lat": 18.92488,
    "long": 72.82723,
    "place": "Palomino",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "527c8d3c-3d01-45e3-9b5f-a6cf94fb40b2",
    "lat": 28.5445186,
    "long": 77.27582,
    "place": "Mac and Jack’s Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "5297cd6e-9ded-4609-bee8-34ee4df46da4",
    "lat": 28.71819105,
    "long": 77.1775377,
    "place": "Maggiano's Little Italy",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "529dd47d-6336-4825-a291-5bb6c13d5895",
    "lat": 19.019,
    "long": 73.0162,
    "place": "Salish Lodge",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "52a11152-f7df-4ed0-8a43-c4a39a0c8352",
    "lat": 28.49925,
    "long": 77.19621,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "52a179f3-7efb-40aa-a921-4b7b035fbab8",
    "lat": 19.01395,
    "long": 72.84354,
    "place": "Rover's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "52c83f72-852f-498d-998c-10edc3407880",
    "lat": 28.57443,
    "long": 77.22838,
    "place": "Desert Fire",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "52c8ed72-c417-47d3-b959-c6fc15a771e7",
    "lat": 28.56795,
    "long": 77.34367,
    "place": "Typhoon",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "52f6c101-9ac9-4f2e-864a-9189a139a92e",
    "lat": 28.61019,
    "long": 77.34515,
    "place": "Rover's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "530059ad-33ba-428f-ad7e-6de32138b552",
    "lat": 28.45295,
    "long": 77.06154,
    "place": "Typhoon",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "530a1531-a52d-46b7-94b4-32f67fd46695",
    "lat": 19.19084,
    "long": 72.82029,
    "place": "Soos Creek Wine Cellars",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "530ca3a5-5e67-4ab8-a9ac-2e890a5dfe57",
    "lat": 28.4266066,
    "long": 77.085132,
    "place": "Columbia Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5346fb47-7f6a-4f5c-ac10-d476e16dc27e",
    "lat": 28.579477,
    "long": 77.2580933,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "534caf0d-6fdc-48a5-a57c-868a07a30f65",
    "lat": 28.41242,
    "long": 77.05731,
    "place": "The Tasting Room",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "5359a405-d26b-4d94-90a8-4786bf6289d4",
    "lat": 28.52736435,
    "long": 77.39129248,
    "place": "Soos Creek Wine Cellars",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "536541a3-1cb5-4764-9cbf-c44e1ce37faf",
    "lat": 12.9935,
    "long": 80.20046,
    "place": "Cutters Bayhouse",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "53af212c-504c-4c25-a261-c644462197b9",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "I Love Sushi",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "53cf660f-bec1-4426-93b7-3fc211d48512",
    "lat": 13.03181,
    "long": 77.67385,
    "place": "Desert Fire",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "53d487f3-98a7-47bd-b09d-3a95b0713d88",
    "lat": 28.39915,
    "long": 77.29529,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "53d7c582-66aa-4c61-873c-7c12445f6c66",
    "lat": 17.4329345,
    "long": 78.4600973,
    "place": "I Love Sushi",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "53dd8516-3b6c-4468-993e-029f6f918103",
    "lat": 28.6385643,
    "long": 77.1836212,
    "place": "Italianissimo Ristorante",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "53ebe7db-2a2f-4338-9370-6d0d3b828668",
    "lat": 13.00428,
    "long": 77.75483,
    "place": "Columbia Winery",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "540deebb-793e-459c-8696-8bd0f49e81c2",
    "lat": 28.6387,
    "long": 77.34114,
    "place": "Salute of Bellevue",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "541702e8-da36-4e48-8634-15b61284caa4",
    "lat": 12.93698,
    "long": 77.56929,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "54267f5f-1a90-4498-9aca-415577088618",
    "lat": 28.4302789,
    "long": 77.1012711,
    "place": "Palomino",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "542d4628-79ba-4cbf-8d06-292101627b71",
    "lat": 28.6378598,
    "long": 77.37925235,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "54448c38-9c9e-42e9-b600-41d70260519f",
    "lat": 28.6385643,
    "long": 77.1836212,
    "place": "Dahlia Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "545d0d5d-af45-4399-ab7f-fe672555d014",
    "lat": 28.6872161,
    "long": 77.132511,
    "place": "Trader Vic's",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "5465d33a-4de6-489b-8ef7-9d7027eac2f5",
    "lat": 28.61925,
    "long": 77.4226,
    "place": "Mac and Jack’s Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "54a6e8f9-3aa1-4895-b6cf-cb6ea5efe247",
    "lat": 12.95651,
    "long": 77.55409,
    "place": "Wild Ginger",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "54c7bde1-03a7-4b3f-ad75-ba919678b375",
    "lat": 28.39573,
    "long": 77.05946,
    "place": "Assagio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "55098902-5c9d-4752-aebe-876cec4baae4",
    "lat": 28.65930073,
    "long": 77.14029933,
    "place": "Salute of Bellevue",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "550e5422-7854-4268-815e-7c88df4814d9",
    "lat": 28.50507,
    "long": 77.23018,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "55354ea8-46d8-40fb-8754-0336cfcf0cf5",
    "lat": 19.23657,
    "long": 72.97263,
    "place": "Royal India",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5539f37c-2a52-4fb0-9e2a-4ad4a1d16657",
    "lat": 19.0757,
    "long": 72.82612,
    "place": "Market Cellar Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "55412b7a-5a02-4090-8fc3-289ce75394c2",
    "lat": 22.49478,
    "long": 88.38827,
    "place": "Typhoon",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "558ce268-634d-4219-b13d-4ddf8afee8f9",
    "lat": 28.46598,
    "long": 76.99941,
    "place": "Market Cellar Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "55b17d54-97e9-4c87-9c58-4f174c90090d",
    "lat": 28.50173,
    "long": 77.37711,
    "place": "Mac and Jack’s Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "55b4dba6-a895-49eb-a9cb-e57648251d23",
    "lat": 26.78077,
    "long": 80.96076,
    "place": "Royal India",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "55d14af9-ddc0-49ba-84d3-cc14e622da6b",
    "lat": 19.07405,
    "long": 72.82401,
    "place": "Cutters Bayhouse",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "55daf43f-a2b6-466b-a6ff-d674125fb3d0",
    "lat": 28.58653,
    "long": 77.36632,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "563fb86a-794f-4032-b10e-9a5315a3a460",
    "lat": 19.0123063,
    "long": 72.8422034,
    "place": "Sans Souci",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "56589d14-9642-4c9d-a671-0d81b84857cd",
    "lat": 12.96441,
    "long": 77.64945,
    "place": "Pike Brewing Company",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "5673cf89-d726-45fd-bf6e-e672245f88c8",
    "lat": 18.6010616,
    "long": 73.7833899,
    "place": "I Love Sushi",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "567c422c-5b6e-4ad9-92df-4c9e78b92c85",
    "lat": 28.7065292,
    "long": 77.0985168,
    "place": "Royal India",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "56834e20-a373-464e-9a39-307f1441b356",
    "lat": 22.6208598,
    "long": 88.3487516,
    "place": "Italianissimo Ristorante",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "56bc089a-7bdf-49d1-a0ab-de6e64be3fcf",
    "lat": 28.4709304,
    "long": 76.9965945,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "56ebca7e-12dd-4619-979b-6355529749e4",
    "lat": 28.43092,
    "long": 77.10097,
    "place": "Desert Fire",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "571e3e71-f1d5-4829-bee5-bbe9da392aff",
    "lat": 28.63784,
    "long": 77.31289,
    "place": "Italianissimo Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5753b1c5-9131-41ae-8ddb-a07936ee60d6",
    "lat": 28.46555,
    "long": 77.00583,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "576eca36-20f7-4821-95f6-69ab98b114f8",
    "lat": 12.9844795,
    "long": 77.5584161,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "57765875-a6bd-4b70-994c-74cec7896559",
    "lat": 28.4447122,
    "long": 77.066974,
    "place": "Barking Frog",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "5783f73b-b5b6-4269-9fad-da5737431233",
    "lat": 28.7165358,
    "long": 77.1260448,
    "place": "Chutney's",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5792c510-c236-4270-bd9e-c1cef8930a91",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "I Love Sushi",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "57bdd3b9-cdb6-458b-986b-6bd8fbee5142",
    "lat": 28.46637,
    "long": 77.03017,
    "place": "The Tasting Room",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "57d5ff94-f51d-457b-873f-d40179ae30a8",
    "lat": 12.9818786,
    "long": 77.6155783,
    "place": "Typhoon",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "585088f3-5bb6-44d8-a167-49f3ebe9b39a",
    "lat": 28.35795,
    "long": 77.32904,
    "place": "Wasabi Bistro",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "585b0862-c344-431c-9113-92448e2201c3",
    "lat": 28.56304,
    "long": 77.38419,
    "place": "Italianissimo Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "586feaf3-27cc-4370-b1e6-7679e11cf61b",
    "lat": 22.48866,
    "long": 88.38945,
    "place": "Sea Garden",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "58713072-7d80-4085-91e4-ef6a5d7c5c00",
    "lat": 12.90072,
    "long": 77.65379,
    "place": "The Calcutta Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5880be39-3915-4283-b650-d099c0beabfb",
    "lat": 28.67155,
    "long": 77.08491,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5881b156-6b92-4a0b-a0ae-5aa597599ce5",
    "lat": 28.64469,
    "long": 77.33864,
    "place": "Trader Vic's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "58b7a306-f941-417c-b814-745486aff9e4",
    "lat": 26.8037984,
    "long": 80.9086885,
    "place": "Kaspar's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "58c5b0a5-b713-4a42-b94a-f92f83830d64",
    "lat": 28.646685,
    "long": 77.4151517,
    "place": "Redhook Ale Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "58d81fb8-7a34-41bd-b66c-87b75959a1a4",
    "lat": 12.83353,
    "long": 77.70163,
    "place": "Market Cellar Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "58fba60c-91ec-449a-b68f-8c9981159eec",
    "lat": 13.1190665,
    "long": 77.5799718,
    "place": "Salute of Bellevue",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "5914db9a-f767-4c26-8546-0c5bd61d1717",
    "lat": 28.68309,
    "long": 77.36377,
    "place": "Yarrow Bay Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "594369fc-3fd3-4b51-bf50-93752ad7d681",
    "lat": 22.95976,
    "long": 72.61096,
    "place": "Salish Lodge",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "5944df69-b3de-4731-81b7-0efd1e31dd43",
    "lat": 28.35954,
    "long": 77.32702,
    "place": "Soos Creek Wine Cellars",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "5953be41-abb0-41dc-b15e-41eeafee63e0",
    "lat": 28.59697,
    "long": 77.05178,
    "place": "Café Juanita",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "59553be4-fd97-43da-80e8-ccdfbedfdf9b",
    "lat": 28.4599372,
    "long": 77.0888657,
    "place": "Isabella Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "59595d45-856f-4ef8-a17d-d6b7b3878f3b",
    "lat": 28.56341,
    "long": 77.21354,
    "place": "Herb Farm",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "59673a60-ce22-428c-984a-5a7d13e00541",
    "lat": 12.95794,
    "long": 77.69448,
    "place": "Redhook Ale Brewery",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5969530f-ebb6-456a-bd90-9f91a950be81",
    "lat": 28.61262,
    "long": 77.42425,
    "place": "Sea Garden",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "596e376e-4e4a-4d11-bac8-dc053ab89e36",
    "lat": 28.4882887,
    "long": 77.0001589,
    "place": "Isabella Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "5991e1bf-ee45-4c09-a883-c8b699943419",
    "lat": 28.68337,
    "long": 77.31311,
    "place": "Typhoon",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "59b02f79-d6e6-4071-b0d8-dae9d956de3c",
    "lat": 28.5416063,
    "long": 77.1235665,
    "place": "Salish Lodge",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "59ce2199-da9c-47f7-9ffc-b438b94ac50d",
    "lat": 23.0185,
    "long": 72.56155,
    "place": "Café Juanita",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "59f8b009-8afd-458c-89c1-b65f88e2dd8b",
    "lat": 26.84489,
    "long": 81.00477,
    "place": "Redhook Ale Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5a3b027f-3bd7-45ef-a31e-f1631c03e509",
    "lat": 28.59483,
    "long": 77.33853,
    "place": "I Love Sushi",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "5a511a04-1e10-42cf-8930-ecc81cf7ae58",
    "lat": 28.7065292,
    "long": 77.0985168,
    "place": "Barking Frog",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "5a610abd-c0f1-46f4-bd74-0226044a05f1",
    "lat": 28.71157,
    "long": 77.14986,
    "place": "Royal India",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "5a68afd6-c88b-44cc-91ec-1183287c2f0a",
    "lat": 28.63622,
    "long": 77.1804,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "5a69b3b7-a4b5-4b3b-bee3-3bdf576345eb",
    "lat": 22.57188,
    "long": 88.35735,
    "place": "Desert Fire",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "5a84f5d3-dd29-4f73-a783-dfad01ecced7",
    "lat": 28.63545286,
    "long": 77.3646536,
    "place": "Columbia Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "5a914bef-b6a7-441b-ab74-17705df3fef4",
    "lat": 28.56997,
    "long": 77.36149,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5a9f2ff4-23f0-462c-9dbe-f2e522be3e85",
    "lat": 18.98044,
    "long": 72.84131,
    "place": "Barking Frog",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "5ac041b8-6a7f-4417-a281-e6a036db69fa",
    "lat": 12.9801724,
    "long": 77.6777569,
    "place": "Sea Garden",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5ac78e4b-fdc2-4fd2-bdae-c12b0772b27f",
    "lat": 18.45977,
    "long": 73.7852,
    "place": "Dahlia Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5ad34fdc-f7d6-4384-8147-4ac10a738a07",
    "lat": 12.99351,
    "long": 80.20107,
    "place": "Royal India",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5ad88e5a-a8ac-4221-84eb-d18328749790",
    "lat": 28.45131235,
    "long": 76.9887128,
    "place": "Yarrow Bay Grill",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "5aefc58d-36c8-418e-98cd-8c9ea7400fcf",
    "lat": 28.57893,
    "long": 77.3818,
    "place": "Yarrow Bay Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5b4a5e50-1ed5-4b55-8aa1-3019426288ba",
    "lat": 26.90417,
    "long": 80.95775,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "5b65592c-edcb-45da-9db0-0d8e0123f173",
    "lat": 28.4956222,
    "long": 77.293849,
    "place": "Café Juanita",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "5b84625c-34eb-4207-a1a7-7c589e825c8d",
    "lat": 28.49615,
    "long": 77.14408,
    "place": "Salute of Bellevue",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "5b9819ed-dbe2-49e1-a8ff-b34ff715dd4d",
    "lat": 12.9503643,
    "long": 77.6160986,
    "place": "Uptown China Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5b9a3331-4b96-46d2-be57-f7be92d8be36",
    "lat": 28.68859,
    "long": 77.29799,
    "place": "Cutters Bayhouse",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5bac7270-d7f9-4c79-b8c5-5305a8b0c698",
    "lat": 28.5571,
    "long": 77.2869,
    "place": "Salish Lodge",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5bb81c20-1b36-4c7f-8f40-de6f392b3369",
    "lat": 28.49912,
    "long": 77.29242,
    "place": "Wild Ginger",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "5bccf263-a4d6-40e1-8f86-bf2ab31372e6",
    "lat": 28.58139,
    "long": 77.32705,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5be1b099-715f-4853-92eb-6a991ba4779a",
    "lat": 28.49539,
    "long": 77.17884,
    "place": "Typhoon",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5bf1d83d-dbdf-440c-b494-28f29a5c90e9",
    "lat": 28.57188,
    "long": 77.22495,
    "place": "Cutters Bayhouse",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5c27e183-a03e-4a4a-b504-7931da1e3ea9",
    "lat": 19.12204,
    "long": 72.82579,
    "place": "Tap House Grill",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "5c9ceb44-1d40-4c00-9d2b-f63df1515773",
    "lat": 12.95792,
    "long": 77.69445,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5cc12ba6-8638-422f-b555-7ba8da2300b6",
    "lat": 28.6533,
    "long": 77.30209,
    "place": "Desert Fire",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5ccd428c-644e-40d6-a72f-1defdfe06fe8",
    "lat": 28.4882887,
    "long": 77.0001589,
    "place": "Trader Vic's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5cd0c31b-6836-4cae-bdfd-cf8339e63bb6",
    "lat": 23.06018,
    "long": 72.65218,
    "place": "Assagio",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "5cd0f820-5213-4a73-b7a7-46fe2d9bc8e0",
    "lat": 26.90493,
    "long": 80.95817,
    "place": "Café Juanita",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5cd5e715-151c-426c-8a05-6295a5ac0f18",
    "lat": 28.6163725,
    "long": 77.2829117,
    "place": "Sea Garden",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "5d1bb5ae-fd7a-4b86-8de5-8532314c7e8e",
    "lat": 28.66051,
    "long": 77.35901,
    "place": "Soos Creek Wine Cellars",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5d554e6d-d118-496f-bff0-c3cef720a693",
    "lat": 12.96479,
    "long": 77.65767,
    "place": "Cutters Bayhouse",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "5d5ac1fe-31f5-4b26-bec8-fddd57e71f02",
    "lat": 28.60283,
    "long": 77.37911,
    "place": "Maggiano's Little Italy",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "5d6b76f7-d3b5-4372-ab70-09cd3b108030",
    "lat": 12.88333,
    "long": 77.56975,
    "place": "Soos Creek Wine Cellars",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5d7b0c71-cc61-430e-b1e2-273ffd620e60",
    "lat": 12.88333,
    "long": 77.56975,
    "place": "Desert Fire",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "5d7b8279-2e67-496a-967e-7a983be8baae",
    "lat": 28.6125308,
    "long": 77.424336,
    "place": "Uptown China Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "5d910340-be5c-4dff-8429-d2aa9829d403",
    "lat": 28.50173,
    "long": 77.37711,
    "place": "Market Cellar Winery",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "5da936ff-b2ae-4e20-8756-acc54f06569f",
    "lat": 28.557835,
    "long": 77.3766383,
    "place": "Tap House Grill",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5de59534-1430-4d84-b93d-15e027dec1d8",
    "lat": 28.62652,
    "long": 77.05782,
    "place": "Mac and Jack’s Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "5df381e1-e439-4902-86d7-9cf97bc0b1a9",
    "lat": 28.71157,
    "long": 77.14986,
    "place": "Royal India",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5e24e796-7699-40aa-b3d5-0d2d10da0ba3",
    "lat": 12.97509,
    "long": 80.24561,
    "place": "Salish Lodge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5e834fdb-ebe6-4125-a59f-da317eab46db",
    "lat": 28.63,
    "long": 77.10156,
    "place": "Sans Souci",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "5ea6639b-63d5-4413-ab41-dcf9adc38ec0",
    "lat": 17.5062,
    "long": 78.33048,
    "place": "Herb Farm",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "5ec2c1f3-aae1-4b70-90b1-7a6aa4625176",
    "lat": 28.43665,
    "long": 77.11534,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5ecb39d9-608a-46e1-a9d3-600f8a88527d",
    "lat": 23.10719436,
    "long": 72.59102586,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "5ee04fde-eb56-4025-81e4-22354296828b",
    "lat": 28.51978,
    "long": 77.22669,
    "place": "Columbia Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "5ee3f8c1-af38-49a6-9562-359431cddb8e",
    "lat": 22.571939,
    "long": 88.3573112,
    "place": "The Tasting Room",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5eeb0133-ed7a-4b68-8747-154af4d76211",
    "lat": 28.5382228,
    "long": 77.2305693,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5ef2dad9-fd06-4724-9ced-86d3b5f8e09c",
    "lat": 22.5366025,
    "long": 88.3496555,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "5f09f333-86bb-43e0-a777-534077767797",
    "lat": 28.57518,
    "long": 77.1387,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "5f47a8af-32b6-49f8-9be9-53787605ab81",
    "lat": 28.66995,
    "long": 77.4478,
    "place": "Dahlia Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5f530904-a613-4c6c-91d4-ff35148c7b68",
    "lat": 19.07847,
    "long": 72.88056,
    "place": "Desert Fire",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "5f644843-c4e8-4494-8d8a-677cec0b41ef",
    "lat": 28.55389,
    "long": 77.3417,
    "place": "Café Juanita",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "5f8b65f8-6cd3-4adb-934f-12609dd5c206",
    "lat": 28.57075,
    "long": 77.17241,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5fcd8397-e06b-4319-9049-bb0776e4731d",
    "lat": 18.95525,
    "long": 72.83603,
    "place": "Sans Souci",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "5fd12111-5e08-4f82-895a-dd5fec4f439b",
    "lat": 12.95794,
    "long": 77.69448,
    "place": "Yarrow Bay Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "604bd411-5e7d-4155-bcda-cddead11002f",
    "lat": 26.961765,
    "long": 75.7236933,
    "place": "Market Cellar Winery",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "60657cc0-79fe-4ab5-a5e1-070f97c9168b",
    "lat": 28.4775554,
    "long": 77.0085042,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "6166ef8d-beed-4635-b3eb-463a7047bd42",
    "lat": 23.055,
    "long": 72.63162,
    "place": "Kaspar's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "6183bac4-b1ab-4ece-9730-aff636c9b12f",
    "lat": 28.5889147,
    "long": 77.0768528,
    "place": "Coho Cafe",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "61a66acb-be34-4753-b031-66fdfbfd9b50",
    "lat": 28.62325,
    "long": 77.29484,
    "place": "Trader Vic's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "61d74b97-220f-4f56-a958-4420da416295",
    "lat": 28.45811,
    "long": 77.05626,
    "place": "Mac and Jack’s Brewery",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "61e1bf46-8f71-4b74-9908-8bd463777011",
    "lat": 28.465208,
    "long": 77.545986,
    "place": "Space Needle",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "620bb964-8d4d-47e1-8a05-6e3e16d019e1",
    "lat": 28.50853,
    "long": 77.01992,
    "place": "Soos Creek Wine Cellars",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "621e74e4-4d16-4587-b1bd-fe03222717ca",
    "lat": 22.61085,
    "long": 88.43383,
    "place": "Yarrow Bay Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "622e24d9-d0c1-4e92-b9cf-9a4dc5536c76",
    "lat": 26.88294,
    "long": 80.99815,
    "place": "The Tasting Room",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "624c216c-be63-4d76-ab37-963f209e3ed4",
    "lat": 18.49133,
    "long": 73.79208,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "6256ba15-c69d-4d85-b6f3-41e2ab050324",
    "lat": 28.62632,
    "long": 77.43819,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "627f2a40-f690-427f-bce6-f93201901ab2",
    "lat": 26.87432,
    "long": 75.7907,
    "place": "Manzana Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "628b9a38-3f98-4e98-97ab-25e6d8b27bc6",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "Kaspar's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "62a987f3-32f0-4d18-8b5f-dbed63c46888",
    "lat": 28.7108634,
    "long": 77.2139319,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "62d2a664-25fa-4d05-9116-7e75cf550a83",
    "lat": 28.48432,
    "long": 77.01476,
    "place": "Wild Ginger",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "62d8f025-1114-44c6-abf4-e56ec5ea72a6",
    "lat": 28.56836,
    "long": 77.34621,
    "place": "Wild Ginger",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "630031f8-986d-403a-8018-2918f96bbb67",
    "lat": 22.54386,
    "long": 88.36308,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "63020d63-b839-499e-82b4-e5b7688b9d5f",
    "lat": 28.6872161,
    "long": 77.132511,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "631a45c6-6f26-4a2a-b0c2-e069d2f45585",
    "lat": 28.6753428,
    "long": 77.220344,
    "place": "Buca di Beppo",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "633bb807-4728-4e51-9b3c-4654a435bfe8",
    "lat": 26.80379,
    "long": 80.90842,
    "place": "Rover's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "63988157-6b5c-4f96-a55a-4b4b5b5f911c",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "Columbia Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "63b5fe28-a34f-4af0-84b4-0256756fea9d",
    "lat": 28.635628,
    "long": 77.283066,
    "place": "I Love Sushi",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "63c28588-aa31-44e2-ae92-f25c35ec93e2",
    "lat": 28.7065292,
    "long": 77.0985168,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "63cc7737-5e3e-424f-b03d-f3643559f18d",
    "lat": 22.4609755,
    "long": 88.3102533,
    "place": "Barking Frog",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "63e0d1b9-bcdb-47fc-848c-2d6a222ac2d5",
    "lat": 18.955,
    "long": 72.83564,
    "place": "Assagio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "646dcda7-6172-4126-9fa0-3b38fdad260c",
    "lat": 28.45291,
    "long": 77.06154,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "648ce4b0-7e06-46e5-a4b7-ed5266776028",
    "lat": 12.96479,
    "long": 77.65767,
    "place": "Sans Souci",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "64a17051-87b7-4774-91b6-fa079a7166de",
    "lat": 28.5921092,
    "long": 77.0350934,
    "place": "Il Fornaio",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "64a56287-fef3-45b7-9379-edb08e76a0c4",
    "lat": 26.8794116,
    "long": 81.0065383,
    "place": "Italianissimo Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "64af5f57-4228-4d10-a1c9-469599caf53f",
    "lat": 28.39688,
    "long": 76.96964,
    "place": "Sans Souci",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "64c12d45-5888-4e4c-8de5-5abf52dc4523",
    "lat": 22.60214,
    "long": 88.37627,
    "place": "Isabella Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "64cd95e0-7f22-4664-85e2-f9da83883934",
    "lat": 22.59029,
    "long": 88.3387,
    "place": "Herb Farm",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "64e7d13e-7afa-4f6c-a42e-9cda0fe51a76",
    "lat": 28.4255994,
    "long": 77.0549768,
    "place": "Soos Creek Wine Cellars",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "65103551-8c88-4430-9c73-16b9bf164e9a",
    "lat": 28.39688,
    "long": 76.96964,
    "place": "Bis On Main",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "6513b6ca-c0d4-413c-afd2-00f8eb93193d",
    "lat": 28.59152,
    "long": 77.30931,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "651e972b-3177-43c9-a034-c81077375a23",
    "lat": 28.5633,
    "long": 77.35013,
    "place": "Uptown China Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "652079dd-990c-45b0-a415-f5acb272b18e",
    "lat": 28.66995,
    "long": 77.4478,
    "place": "Columbia Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "65772465-6329-41a4-ab24-2429ba424edd",
    "lat": 28.59374,
    "long": 77.11702,
    "place": "Maggiano's Little Italy",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "657c8a2c-d9a6-4f9c-8aa4-0b1d9055b281",
    "lat": 18.95609,
    "long": 72.82948,
    "place": "Palomino",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "65a2ea2a-a014-4216-9cd8-0cfc5c13e7d4",
    "lat": 28.5071507,
    "long": 77.2357335,
    "place": "I Love Sushi",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "65d478ed-544b-43ac-b82d-32d1816822be",
    "lat": 28.63113,
    "long": 77.27273,
    "place": "Market Cellar Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "663511f5-59f9-4c26-a9e0-6e8787107fde",
    "lat": 28.48927,
    "long": 77.09308,
    "place": "Coho Cafe",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "663bc4cd-6e98-40e5-8933-fcb1de809b64",
    "lat": 26.84334,
    "long": 80.88356,
    "place": "Dahlia Lounge",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "664aa351-bf77-4473-aa08-0ef32f33a22e",
    "lat": 19.20601075,
    "long": 72.96031713,
    "place": "Salish Lodge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "664e028c-5706-41c2-b480-241056a819d8",
    "lat": 12.9671388,
    "long": 77.7410433,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "66618caf-ef60-4e8f-8100-27077d62a563",
    "lat": 28.4658,
    "long": 77.01884,
    "place": "Space Needle",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6687a696-4788-4a3e-8d73-7a04236fecd8",
    "lat": 28.70528,
    "long": 77.19765,
    "place": "Café Juanita",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "66a5ab53-99f8-4e58-8954-614256c28381",
    "lat": 28.42526,
    "long": 77.04157,
    "place": "Rover's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "66b05e15-5335-4277-b6f6-dbcaa901b425",
    "lat": 28.5921577,
    "long": 77.3094043,
    "place": "Il Fornaio",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "66e2dca1-a1b2-4154-8872-b03e9e434631",
    "lat": 28.63,
    "long": 77.10156,
    "place": "Sans Souci",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "66f4ed5c-6250-4754-8810-0af61d0a9b42",
    "lat": 28.46547,
    "long": 77.00549,
    "place": "Pike Brewing Company",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "670e85d5-3793-41ff-87bf-cb25eae9f166",
    "lat": 28.7165358,
    "long": 77.1260448,
    "place": "Herb Farm",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "6742ede0-04fb-4229-83c9-cf761846421b",
    "lat": 28.46047,
    "long": 77.04213,
    "place": "Pike Brewing Company",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "67b87b21-24f9-4805-b31f-64c234eed2af",
    "lat": 22.95978,
    "long": 72.61059,
    "place": "Space Needle",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "67c06ba0-0a0b-4a3d-8b5a-14938c4f0893",
    "lat": 19.12114,
    "long": 72.84915,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "67ed89b8-4c92-41d8-a731-c94df114c84a",
    "lat": 26.88558,
    "long": 81.00721,
    "place": "Mac and Jack’s Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "6819e184-fac2-4dac-a454-cfec96de213c",
    "lat": 19.1204017,
    "long": 72.8694867,
    "place": "Mac and Jack’s Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "6826c5f4-43d4-4df4-8095-ae48fef69641",
    "lat": 28.4447122,
    "long": 77.066974,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "68400a7c-cd34-4be9-bae7-a6b6b25ede19",
    "lat": 23.1081723,
    "long": 72.5914872,
    "place": "Sea Garden",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "684afa60-a6b6-443e-9d7a-256ef7e6cb64",
    "lat": 28.62632,
    "long": 77.43819,
    "place": "Desert Fire",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "686848ce-cd98-4a09-ae8f-c6b8fbc0bb77",
    "lat": 22.5791915,
    "long": 88.4804969,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "6871907e-4acd-4a39-ab6d-ab03b4fe10db",
    "lat": 19.01186,
    "long": 72.8422,
    "place": "Trader Vic's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "687750a2-d023-4fc4-aed8-a35504e2f168",
    "lat": 22.57199,
    "long": 88.35692,
    "place": "Mac and Jack’s Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "6883c575-cc32-441f-9db7-3d71da26d4ad",
    "lat": 28.63372,
    "long": 77.34193,
    "place": "Il Fornaio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6886dd0f-9485-4011-97f2-9407bd9f6012",
    "lat": 28.6867006,
    "long": 77.309384,
    "place": "Desert Fire",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "68a72661-dfd0-4786-aabf-cca4b247ae6f",
    "lat": 19.07394,
    "long": 72.88359,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "68ca46bf-7a9d-4d69-a3bb-c29f495ab993",
    "lat": 26.84213,
    "long": 80.93633,
    "place": "Sea Garden",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "68ccd4be-b8ca-482f-a5fe-f03cfd5392f9",
    "lat": 26.84001,
    "long": 75.81046,
    "place": "Mac and Jack’s Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "690c5012-db41-4ff1-84b7-aaf03389b98c",
    "lat": 28.6918594,
    "long": 77.2010123,
    "place": "Assagio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6919b569-fd3b-4cb2-856a-30483440a422",
    "lat": 28.36039,
    "long": 77.32584,
    "place": "Royal India",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "696415a2-0abb-4dfb-9408-af7449e0a365",
    "lat": 28.615158,
    "long": 77.3460408,
    "place": "Uptown China Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "698ad83a-692d-4408-aa0a-989b5511b749",
    "lat": 26.77743,
    "long": 80.91196,
    "place": "Buca di Beppo",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "699b444f-a9e0-4a57-b0a5-ccd23a70cf39",
    "lat": 28.5519447,
    "long": 77.3505566,
    "place": "Pike Brewing Company",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "69cf1eda-45d8-4434-84e4-cdeaf2b01677",
    "lat": 28.62984726,
    "long": 77.04750584,
    "place": "Dahlia Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "69fe3e11-75f8-4957-b39c-edbfca5d6140",
    "lat": 28.63277,
    "long": 77.31228,
    "place": "Maggiano's Little Italy",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6a1d8044-2a1d-43a4-8777-5bf9c3f63095",
    "lat": 19.13608,
    "long": 72.86498,
    "place": "Dahlia Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "6a1dbd79-c9f6-44a4-bbef-3d78f65c9a4e",
    "lat": 17.5062,
    "long": 78.33048,
    "place": "Assagio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6a3207fc-2c8a-4e42-a5dc-317d30c4514b",
    "lat": 28.6632481,
    "long": 77.3325315,
    "place": "Assagio",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "6a49ac06-314f-454e-a433-2f9e76eab461",
    "lat": 28.63254,
    "long": 77.04451,
    "place": "Cutters Bayhouse",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6a516c52-116f-4cf1-9e3c-9781a51a285f",
    "lat": 28.61665,
    "long": 77.04272,
    "place": "Trader Vic's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "6a5e672e-9cd5-4e74-96b2-385bdbc735b3",
    "lat": 22.48948,
    "long": 88.31376,
    "place": "Mac and Jack’s Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6a6276b2-8bfb-415a-85f2-f794a48428b4",
    "lat": 28.502566,
    "long": 77.02653,
    "place": "Isabella Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "6a906759-9a7e-415b-a7e4-3549bf2556f7",
    "lat": 26.84002,
    "long": 75.81046,
    "place": "Pike Brewing Company",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6aa91de6-59f6-4f67-80c9-1e5171c475e3",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "6aded0e4-b539-4e8c-87e5-fef68e60e95b",
    "lat": 28.62724,
    "long": 77.30854,
    "place": "Buca di Beppo",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6aea0467-1554-4b3e-9180-ecf4a4ccd842",
    "lat": 26.9326741,
    "long": 75.7951361,
    "place": "Mac and Jack’s Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6aec7e2a-1cd8-4735-838c-ef00c35bff61",
    "lat": 18.9641301,
    "long": 72.833509,
    "place": "Chutney's",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "6b02532c-79c9-41a3-9cfd-5af0731570d6",
    "lat": 12.9214817,
    "long": 77.5197406,
    "place": "Italianissimo Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6b4e2573-126b-49a6-9061-c84306f1797d",
    "lat": 28.72336,
    "long": 77.08426,
    "place": "Desert Fire",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6bac99ee-83d1-4778-89e1-78bb24dc8cbf",
    "lat": 28.6871071,
    "long": 77.3666599,
    "place": "Mac and Jack’s Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6bacf90b-ff77-42d8-b3e7-e193f9772807",
    "lat": 28.57909,
    "long": 77.38177,
    "place": "Salish Lodge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6bb2d94e-f0de-4570-9a04-b80412c1f45c",
    "lat": 28.7015348,
    "long": 77.0824682,
    "place": "Bis On Main",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6bbabb40-c3f6-4293-8609-fd5b9d5f13f2",
    "lat": 28.50308,
    "long": 77.02709,
    "place": "Trader Vic's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6bd49be8-d1e9-47c4-a30a-70aaaaedc462",
    "lat": 28.6441516,
    "long": 77.3449637,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "6be232b1-f90b-48ab-8557-1c5b56c555b5",
    "lat": 12.8993548,
    "long": 77.69580388,
    "place": "Chutney's",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6c05b39f-912e-4ecc-8374-6227727bd38b",
    "lat": 28.4775554,
    "long": 77.0085042,
    "place": "Wild Ginger",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6c0acdf3-6d1d-4d6e-86d7-10680836a7b8",
    "lat": 12.96779,
    "long": 77.67079,
    "place": "Il Fornaio",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "6c237a96-9deb-4cfd-8e16-bed3eaffb9ad",
    "lat": 17.47672,
    "long": 78.54194,
    "place": "The Tasting Room",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6c43bc33-aa59-4d1c-abb4-81c93a38fbdc",
    "lat": 23.02716,
    "long": 72.58631,
    "place": "I Love Sushi",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "6c597255-2e8b-4325-83a3-19286664dcd5",
    "lat": 18.63293,
    "long": 73.79746,
    "place": "Il Fornaio",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "6c67883b-590b-4ac4-811c-ceef9a335eed",
    "lat": 12.82053,
    "long": 77.70866,
    "place": "Rover's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "6c77125a-a7d6-4ab9-b551-a542e89dfc01",
    "lat": 17.5062,
    "long": 78.33048,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "6c8a431f-2071-435f-bf89-2fa93987c410",
    "lat": 28.57032,
    "long": 77.33753,
    "place": "Kaspar's",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "6c966baa-c25c-4a30-ab19-d2b587a6d256",
    "lat": 18.9548517,
    "long": 72.8314037,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6ca81ff8-a8fc-4211-ac8a-c3692d58940b",
    "lat": 28.71819105,
    "long": 77.1775377,
    "place": "Rover's",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "6cd3358d-c764-4d86-9503-8cee0e2c28f7",
    "lat": 28.7065292,
    "long": 77.0985168,
    "place": "The Calcutta Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "6d1e1fb0-63f8-4ca2-8428-7a8881cde47a",
    "lat": 28.52851,
    "long": 77.38777,
    "place": "The Calcutta Grill",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "6d4e35c0-2def-4ebc-90d9-92af8aa0fe70",
    "lat": 28.5560274,
    "long": 77.2208515,
    "place": "Dahlia Lounge",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6d5055b2-ac40-4864-91c1-51575bd68a97",
    "lat": 28.46033,
    "long": 77.04229,
    "place": "Barking Frog",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6d71205a-509e-4f88-9cba-8d147369a9f4",
    "lat": 28.68545,
    "long": 77.32189,
    "place": "Il Fornaio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6d9c3ddb-ba2c-4acf-b66f-f5bf56f27f83",
    "lat": 23.055,
    "long": 72.63162,
    "place": "Columbia Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6dc28a1e-1cea-4eb2-bcd2-1ebdbd0a99c6",
    "lat": 28.4276,
    "long": 77.03872,
    "place": "Wild Ginger",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "6dcf60e4-8c6a-444c-beb5-8348d5cd1aec",
    "lat": 28.55699,
    "long": 77.28691,
    "place": "Wild Ginger",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "6e4b3dc8-f888-48cd-af8c-f9326532a975",
    "lat": 12.92449,
    "long": 77.66704,
    "place": "Barking Frog",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6e7787dc-f53b-4df5-8e58-36ed23b2da12",
    "lat": 28.56022,
    "long": 77.21976,
    "place": "Wild Ginger",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6e8d6d2f-f2e3-40fc-a04e-0a29aafc5a26",
    "lat": 12.93697,
    "long": 77.56902,
    "place": "Bis On Main",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6ec70588-05fc-41a6-a097-f5e0532f2d21",
    "lat": 28.40205,
    "long": 77.08866,
    "place": "Wild Ginger",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6edaf53c-94a4-4c4e-a228-d817b58fa3d3",
    "lat": 18.9548517,
    "long": 72.8314037,
    "place": "Typhoon",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "6ee55e8c-f923-481d-8f6d-c98b7701d580",
    "lat": 22.5366025,
    "long": 88.3496555,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6efdb083-582a-4f02-a1c2-b057499ee2c2",
    "lat": 17.44594,
    "long": 78.38702,
    "place": "Manzana Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "6f03e0e6-ddce-479c-b98e-eb216aa7c913",
    "lat": 28.56104,
    "long": 77.22723,
    "place": "Sans Souci",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "6f06b7ac-31db-44fe-b55b-8b0623e3b5b5",
    "lat": 17.5062,
    "long": 78.33048,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6f3183f2-2dae-46d2-aa69-a3772775c33b",
    "lat": 19.1158,
    "long": 73.00284,
    "place": "Bis On Main",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6f429168-46a2-4227-9d1c-5f1b3dd08bbd",
    "lat": 28.62325,
    "long": 77.29484,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6f439c25-a416-45db-8d31-d89137790c8e",
    "lat": 19.1964566,
    "long": 72.8757323,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "6f6c0155-ff04-4b9b-968d-79b53ea8e724",
    "lat": 23.055,
    "long": 72.63162,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "6f9a04d4-bb27-4efd-8681-5047306d0a06",
    "lat": 28.4371,
    "long": 77.05496,
    "place": "Space Needle",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "6fbde000-ce28-4b98-8a27-7997e4d83388",
    "lat": 12.98919,
    "long": 77.69188,
    "place": "Sans Souci",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "6fe468d8-5a30-49ba-ae6f-215c670dc3d9",
    "lat": 23.03025,
    "long": 72.58826,
    "place": "Typhoon",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "702273b5-09fa-4cf0-8d91-90af5c369b13",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "Market Cellar Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7030b94f-77c4-4964-a5a1-fa4e0d8779e6",
    "lat": 12.83353,
    "long": 77.70163,
    "place": "Manzana Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "707c8467-17fa-4098-8a08-ab00daa9dd70",
    "lat": 28.488992,
    "long": 77.0124588,
    "place": "Royal India",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7081429a-89c4-4b63-a0d3-dee91f40d7d1",
    "lat": 28.64277,
    "long": 77.35314,
    "place": "The Tasting Room",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "710f20ab-6929-4bc8-a745-72d37c2e6aaf",
    "lat": 19.14234,
    "long": 72.84225,
    "place": "Maggiano's Little Italy",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "71162bca-45b0-432c-970e-a7a6f7a42712",
    "lat": 22.56752,
    "long": 88.4733,
    "place": "Buca di Beppo",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "716c0c62-816c-4ce3-bbf0-072e1a8f8f89",
    "lat": 28.7065292,
    "long": 77.0985168,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7199d8e9-568a-40c4-ad87-edf6ee7a4a7e",
    "lat": 28.56382,
    "long": 77.05936,
    "place": "Chutney's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "71c13dd5-869e-42c5-b0ab-c2544d315a71",
    "lat": 28.4266895,
    "long": 77.0950288,
    "place": "Soos Creek Wine Cellars",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "71dc7f80-b20d-475c-8a31-fe7ee36147a0",
    "lat": 28.68337,
    "long": 77.31311,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "71ec6d7f-f2c9-4959-a894-8be59347ff58",
    "lat": 18.980436,
    "long": 72.841311,
    "place": "Dahlia Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "71f33322-47dd-49f4-bee3-0b7ae32cc6f4",
    "lat": 19.10929,
    "long": 72.85507,
    "place": "Space Needle",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7256167c-314b-4ec2-a634-87ea5a17a97f",
    "lat": 19.1423,
    "long": 72.84224,
    "place": "Isabella Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "726ec02d-3f61-432d-b779-87634bcbc69b",
    "lat": 19.1488894,
    "long": 72.8318941,
    "place": "Yarrow Bay Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "728efb56-f490-4fd8-8ff1-0f9beb1e9bf3",
    "lat": 19.20601075,
    "long": 72.96031713,
    "place": "Columbia Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "72d25ba6-b7de-4cc2-b94d-684da6387198",
    "lat": 28.70364,
    "long": 77.10372,
    "place": "Rover's",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "72d698e8-7ee5-4c41-8efd-2371cc215e68",
    "lat": 28.46598,
    "long": 77.0068,
    "place": "Cutters Bayhouse",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "72e8b6f6-117c-422b-9ab4-f4d1ccd156d7",
    "lat": 28.4266895,
    "long": 77.0950288,
    "place": "Desert Fire",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "7302a555-2935-4bd3-af23-0c24eaaeaf9c",
    "lat": 28.65326,
    "long": 77.30203,
    "place": "Kaspar's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "73052402-f6f6-4b16-8da1-9f9b3b4ffbec",
    "lat": 17.5062,
    "long": 78.33048,
    "place": "Salish Lodge",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "731d8e48-0dec-4dc9-834a-0eae4ba36bee",
    "lat": 28.5732543,
    "long": 77.3803376,
    "place": "Tap House Grill",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "732121fd-8354-41de-a094-d36f921cb2e4",
    "lat": 28.67889,
    "long": 77.31292,
    "place": "Bis On Main",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7332e440-d4c9-4c84-9ad9-3d2c64a85767",
    "lat": 12.95628,
    "long": 77.69444,
    "place": "Bis On Main",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "73390d3e-d277-453b-8632-395f95674b4a",
    "lat": 19.08866,
    "long": 72.84188,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "735e4784-56d9-46e9-982b-41b851caa0a9",
    "lat": 12.9780466,
    "long": 77.6799831,
    "place": "Buca di Beppo",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "7362b1ee-d1e0-4f07-82f9-b13888f07a46",
    "lat": 23.055,
    "long": 72.63162,
    "place": "Market Cellar Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7368059e-5f69-425a-8a24-843b3d0d7bef",
    "lat": 23.055,
    "long": 72.63162,
    "place": "Manzana Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "7371aa88-bfec-4888-af52-185c195c8d56",
    "lat": 28.6332138,
    "long": 77.3122497,
    "place": "The Calcutta Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "737c2600-6587-477c-b6dd-c7d61705d338",
    "lat": 28.7165358,
    "long": 77.1260448,
    "place": "Assagio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "73fa2f48-d598-4081-8a9d-ac615b5911c1",
    "lat": 28.5886261,
    "long": 77.0734991,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "74484c78-39c2-4969-9936-9e8c45765dd0",
    "lat": 28.63308,
    "long": 77.31218,
    "place": "Palomino",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "74922a03-7750-44f7-96c5-24734a72aa26",
    "lat": 28.46552,
    "long": 77.00556,
    "place": "Redhook Ale Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "7499701d-6ac6-42ab-854d-bfd04ca839bd",
    "lat": 18.95525,
    "long": 72.8361,
    "place": "Bis On Main",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "74a18062-2d9e-4e93-ade9-91ad21d742a2",
    "lat": 28.6533,
    "long": 77.30209,
    "place": "Typhoon",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "74b1a52f-b9df-4ba3-b329-c229f3697ecd",
    "lat": 28.7052787,
    "long": 77.1976468,
    "place": "Il Fornaio",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "74eab141-beab-42a8-bb5a-e1150ec69006",
    "lat": 28.68544,
    "long": 77.32191,
    "place": "Salish Lodge",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "74ec2894-01cb-4b57-9abc-bfd0a8cfa1bf",
    "lat": 28.6779058,
    "long": 77.181285,
    "place": "Barking Frog",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "74ed701c-38af-4c6a-a89b-9af8c906020a",
    "lat": 28.60576,
    "long": 77.29476,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "74ed91b6-b8c0-4745-a397-1674848d9514",
    "lat": 19.26213,
    "long": 72.95999,
    "place": "Royal India",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "751b813a-94be-426b-819b-d01b2ead4f80",
    "lat": 19.04886,
    "long": 73.07078,
    "place": "Salute of Bellevue",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "753f41b5-8e7d-4938-a2f0-061513af6c78",
    "lat": 28.4664,
    "long": 77.03019,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "75491939-c8ab-4894-94a0-e8fc6cbb6fbc",
    "lat": 28.57961043,
    "long": 77.3643469,
    "place": "Space Needle",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "755c69c1-efea-42cf-996c-9073d089a053",
    "lat": 22.6021555,
    "long": 88.3762939,
    "place": "Buca di Beppo",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "756933ce-d1ed-499f-ad6b-aff9fe283649",
    "lat": 28.74256,
    "long": 77.11798,
    "place": "Desert Fire",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "7570a848-25bf-4cb5-928c-74dd5f998eb6",
    "lat": 28.71113,
    "long": 77.14566,
    "place": "Wasabi Bistro",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "7572800e-6d07-4495-b543-6784e1fdbc36",
    "lat": 28.68873849,
    "long": 77.11870499,
    "place": "Columbia Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "7572b515-bb76-4da2-8fc9-dbb8e44efb55",
    "lat": 19.1076393,
    "long": 72.9995728,
    "place": "Manzana Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "758f2259-eddd-47b7-8b9f-c353e7c2802a",
    "lat": 28.55015,
    "long": 77.29048,
    "place": "Space Needle",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "75be1e3a-d752-4f71-8994-1d8d3d015b1f",
    "lat": 28.6023246,
    "long": 77.0745418,
    "place": "Typhoon",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "75c3b2e1-5760-4ebb-82d3-af906cf621f8",
    "lat": 17.41212,
    "long": 78.55741,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "75dc2a33-61f0-417c-b4ec-9491df4afc0f",
    "lat": 28.465596,
    "long": 76.9964016,
    "place": "Sans Souci",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "75f88953-eefb-42ef-8b2f-fc57d5a6e88c",
    "lat": 28.4299,
    "long": 77.10127,
    "place": "Manzana Grill",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7610bd6b-c9df-4e8c-8380-30a46954c090",
    "lat": 28.64333,
    "long": 77.3354,
    "place": "Sans Souci",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "76315590-263d-4074-aac0-4a8f58bbb203",
    "lat": 17.5063,
    "long": 78.33031,
    "place": "Coho Cafe",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "7655e793-9857-4d0d-bb7e-4c6b1259fa1a",
    "lat": 28.68568,
    "long": 77.34823,
    "place": "Salute of Bellevue",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "76913e91-e9ef-4ee9-b674-6f3069c721a1",
    "lat": 28.4559084,
    "long": 76.9848706,
    "place": "Assagio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "76a6169b-a768-4e1b-87e2-2e746d9aaa5e",
    "lat": 28.5745459,
    "long": 77.2283789,
    "place": "Il Fornaio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "76e77026-8d4d-4f5e-bc53-af9bcce522d6",
    "lat": 28.50173,
    "long": 77.37711,
    "place": "Market Cellar Winery",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "76f69f27-6646-42e6-8af5-443a937ac6b7",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "Manzana Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "77148e9b-7a07-4a6c-936c-ca89c16555b5",
    "lat": 19.07847,
    "long": 72.88056,
    "place": "Mac and Jack’s Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "773ec494-eba1-47cc-a4c9-78aa84624530",
    "lat": 28.73609,
    "long": 77.16919,
    "place": "Manzana Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "77598a7f-4354-42f6-b5e7-b108afa8a230",
    "lat": 28.45382,
    "long": 77.04104,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7761279b-2b7c-4377-b0cf-bacfd991b45d",
    "lat": 28.59801,
    "long": 77.05161,
    "place": "Buca di Beppo",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "77662fae-1986-4530-a5cc-5c0a4fa3a82c",
    "lat": 12.92449,
    "long": 77.66704,
    "place": "Pike Brewing Company",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "778afadb-8bff-4321-bf19-12e2843cd74f",
    "lat": 28.49679,
    "long": 77.1802,
    "place": "Assagio",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "778efbec-f5b4-40c6-9339-494b74d3cb9d",
    "lat": 12.9123598,
    "long": 77.6323721,
    "place": "Cutters Bayhouse",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "778f52a9-ef66-426f-994b-7e7d8cb284cd",
    "lat": 28.4428163,
    "long": 77.0732252,
    "place": "Soos Creek Wine Cellars",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "77929ebe-1540-4361-a1c1-ae0398f71893",
    "lat": 22.58075,
    "long": 88.42544,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "779d72d9-303b-46c9-8122-09d1f102f1fe",
    "lat": 28.65322,
    "long": 77.10017,
    "place": "Yarrow Bay Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "77bae0b4-6e90-4ea6-b145-e78ec232c64a",
    "lat": 28.5071507,
    "long": 77.2357335,
    "place": "Pike Brewing Company",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "77c089ad-a364-44d6-b119-e6299a519b90",
    "lat": 12.924769,
    "long": 77.667354,
    "place": "The Calcutta Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "77d73385-0272-4c58-9d99-80c2cc279a81",
    "lat": 28.46577,
    "long": 77.01888,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "77d9b164-6894-4b4c-a95d-43d397a2fd3c",
    "lat": 28.61756,
    "long": 77.42643,
    "place": "Typhoon",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "77f4d417-6457-44d1-a5e6-3ef665b3af9b",
    "lat": 19.13714,
    "long": 72.86711,
    "place": "Typhoon",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "780cf308-dba6-4ad9-ac2f-70e2033799a4",
    "lat": 28.48997,
    "long": 77.09434,
    "place": "Sea Garden",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "781a3adc-b069-4edb-9f3f-449d41daea23",
    "lat": 12.91106,
    "long": 77.56626,
    "place": "Pike Brewing Company",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "78205b28-a709-4f86-8862-b1c06c67a1f5",
    "lat": 19.18647,
    "long": 72.83428,
    "place": "Il Fornaio",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "7829cdca-8491-4e34-89c7-78d6ec3388e3",
    "lat": 28.49538,
    "long": 77.17885,
    "place": "Buca di Beppo",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "7837978d-371a-49bc-8eec-ae2778cebb92",
    "lat": 28.5570683,
    "long": 77.2869259,
    "place": "Tap House Grill",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "78ce8f09-b13b-4981-a4a5-9161d839910a",
    "lat": 22.5366,
    "long": 88.34966,
    "place": "Salute of Bellevue",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "78dd12a4-7eb3-46d3-86fa-062ce27b7827",
    "lat": 19.0775351,
    "long": 72.8602718,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "78ebb9d4-e47a-49e2-b725-606390b98979",
    "lat": 12.92624,
    "long": 77.55771,
    "place": "Space Needle",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "78ec8367-974c-4597-9ff4-e4630a9b0aaa",
    "lat": 28.53887,
    "long": 77.21411,
    "place": "Herb Farm",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "78fb23aa-b1e7-4bd7-9cc6-7d90fefec779",
    "lat": 28.7165358,
    "long": 77.1260448,
    "place": "Salish Lodge",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "78fe83d7-7ba7-4583-925e-42b595491c51",
    "lat": 12.96608,
    "long": 77.64968,
    "place": "Italianissimo Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "79011cd0-700c-40d4-9cd4-ee9d55a4e700",
    "lat": 19.07392,
    "long": 72.82409,
    "place": "Rover's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "792c3d46-6412-4096-9b8c-702015e0c419",
    "lat": 12.88333,
    "long": 77.56975,
    "place": "The Tasting Room",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "796337b0-5ced-4198-a54c-c08dc0b68b92",
    "lat": 28.7245,
    "long": 77.08583,
    "place": "Pike Brewing Company",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "79935db0-c4f6-4453-bf06-841257b91297",
    "lat": 28.59483,
    "long": 77.33853,
    "place": "Pike Brewing Company",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "799a80ce-cd58-4702-a6ad-319bd4610125",
    "lat": 28.5071165,
    "long": 77.2358394,
    "place": "Wasabi Bistro",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "79af1e88-57a6-4d85-964f-056dad617ca8",
    "lat": 26.78104,
    "long": 80.95175,
    "place": "Italianissimo Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "79c18e5f-076a-4623-9fe7-594e191e41c5",
    "lat": 12.967139,
    "long": 77.7410435,
    "place": "Kaspar's",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "79e3c364-94c9-4ed0-9857-4f6ad612d665",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "Soos Creek Wine Cellars",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7a18edd8-884b-41e2-affa-a3fb28cdeb2d",
    "lat": 28.5071507,
    "long": 77.2357335,
    "place": "Trader Vic's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "7a327251-8346-468d-9f56-8c8d3d4e367f",
    "lat": 28.56655,
    "long": 77.16744,
    "place": "Salish Lodge",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7a37b37a-a5c0-4ead-b781-385337a9bc52",
    "lat": 28.50374,
    "long": 77.18376,
    "place": "Herb Farm",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "7a402fb1-6dcb-44e9-b301-db8105c7ec20",
    "lat": 12.90072,
    "long": 77.65379,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "7a730f95-0b8d-43ab-851d-4ce88fcba775",
    "lat": 28.50853,
    "long": 77.01992,
    "place": "Soos Creek Wine Cellars",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7a78eeea-95bf-41a7-8498-3144d68a2627",
    "lat": 28.47681,
    "long": 77.04771,
    "place": "Italianissimo Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7a805e72-b8e2-49b8-a8cb-b9684917a269",
    "lat": 28.7065292,
    "long": 77.0985168,
    "place": "Yarrow Bay Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "7a9b2316-9b7d-4001-99eb-3653af36d161",
    "lat": 18.95609,
    "long": 72.82947,
    "place": "Palomino",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "7a9f5e5b-9ba2-4285-9a4e-8de7d4b669db",
    "lat": 28.65033,
    "long": 77.27519,
    "place": "Royal India",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7ac85be5-46db-43ad-ab80-14e2015e6dc7",
    "lat": 28.49703,
    "long": 77.14322,
    "place": "Assagio",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "7af1c610-b250-496a-a551-e18b5141fb4e",
    "lat": 28.57361,
    "long": 77.39271,
    "place": "Pike Brewing Company",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "7af835ef-b129-4875-8afa-befa71b9d2b3",
    "lat": 28.3998202,
    "long": 77.3556479,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7b3b10c7-ac3b-4878-9820-07c7d86381f1",
    "lat": 28.68178,
    "long": 77.31795,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "7b3e048c-12e4-478b-9027-65fda061ac49",
    "lat": 19.26484,
    "long": 72.9654,
    "place": "Salute of Bellevue",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7b7aac8f-1d9e-43a2-a97c-c904b9e4d337",
    "lat": 26.89642,
    "long": 75.75565,
    "place": "Yarrow Bay Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7ba48a6b-aea7-4b74-9458-4a4cd435442f",
    "lat": 28.6872161,
    "long": 77.132511,
    "place": "Typhoon",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "7beb0321-1e6b-4798-82da-2a7af8b92c8e",
    "lat": 17.38498,
    "long": 78.43312,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7c5bab2f-6516-410d-8f2a-a169955730dd",
    "lat": 28.46002,
    "long": 77.0791,
    "place": "Desert Fire",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "7c7a477f-759e-4eb1-9e41-0cb6ccf41be0",
    "lat": 28.5718779,
    "long": 77.2249456,
    "place": "Tap House Grill",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "7c8b73dc-cc45-489a-93b7-a15cc507b880",
    "lat": 12.96713,
    "long": 77.74103,
    "place": "Café Juanita",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7c8df3ed-da1e-43a6-87ba-1f3c7b80c230",
    "lat": 28.6553,
    "long": 77.16446,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7c945cde-61e4-4223-9836-fe2e9482f12d",
    "lat": 28.57169,
    "long": 77.1651,
    "place": "Cutters Bayhouse",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "7d2eef81-109b-4137-86a3-bca9ff068476",
    "lat": 22.56752,
    "long": 88.4733,
    "place": "Buca di Beppo",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7d3ac196-1f66-4050-92c6-d2108aa0efd5",
    "lat": 28.65789,
    "long": 77.11899,
    "place": "Columbia Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7d43b131-bbb0-45fb-8d55-ee4a94466384",
    "lat": 28.4264,
    "long": 77.0773,
    "place": "Barking Frog",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "7dcd2be4-72d1-41b4-9f06-7ad435b0f6b4",
    "lat": 28.45378,
    "long": 77.04104,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7df49a97-24bb-48b8-9522-1013553d93ff",
    "lat": 23.00547,
    "long": 72.62121,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "7e4d38cc-6fab-49c3-9c63-ba6aed2fb8fb",
    "lat": 28.56826,
    "long": 77.32409,
    "place": "Space Needle",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7e8ab3e9-d0b4-4854-8e38-5f869ce8e45a",
    "lat": 28.72919,
    "long": 77.0901,
    "place": "The Calcutta Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "7e8ff2e4-bed4-4ef4-bed8-5f26b8aceedc",
    "lat": 19.19084,
    "long": 72.82029,
    "place": "Bis On Main",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7e94a83a-dec6-4f93-b68d-9f0c4a59a122",
    "lat": 28.45378,
    "long": 77.04098,
    "place": "Royal India",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "7e9ec56c-bafb-4c11-bebc-32e20ff10e53",
    "lat": 23.0642,
    "long": 72.52296,
    "place": "Trader Vic's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7ed23987-d4cb-4bfa-9654-31231a1dfe29",
    "lat": 28.63545286,
    "long": 77.3646536,
    "place": "Space Needle",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7ed7446a-58b6-47f2-8adf-d42f13284c24",
    "lat": 22.603022,
    "long": 88.372629,
    "place": "Sans Souci",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7f04f2e7-405f-4ce0-9045-5f5a704d5cd0",
    "lat": 28.557,
    "long": 77.35524,
    "place": "Salish Lodge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7f0db079-9763-437c-98a2-5da5c0a01c16",
    "lat": 28.57959,
    "long": 77.15147,
    "place": "Bis On Main",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7f342bf0-a52e-45ad-932e-ea0ee7c9aec3",
    "lat": 12.96775,
    "long": 77.67073,
    "place": "Wasabi Bistro",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7f435214-c290-43d9-ba22-da0ed6f2c8fe",
    "lat": 28.59787,
    "long": 77.05237,
    "place": "Uptown China Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7f7114af-1738-4fbe-9f73-e42272947f08",
    "lat": 18.5678333,
    "long": 73.9186317,
    "place": "Manzana Grill",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7fa1f687-5deb-44fb-bfc9-7f1b0936eb9e",
    "lat": 12.9115315,
    "long": 77.6823752,
    "place": "Isabella Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7fa487ca-18a6-47e7-88fa-b4abbe7493e0",
    "lat": 18.98044,
    "long": 72.84131,
    "place": "Barking Frog",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7fc48593-cbbd-48ff-9682-ede06081f5eb",
    "lat": 28.7245,
    "long": 77.08583,
    "place": "Chutney's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "7fe2834f-3ead-43c6-aaaa-dc79fa2b1305",
    "lat": 28.7065292,
    "long": 77.0985168,
    "place": "Coho Cafe",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7ff6c81d-d433-4f48-9128-f9970fffda76",
    "lat": 28.66984,
    "long": 77.32842,
    "place": "Yarrow Bay Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "7ffc1903-1796-4bbf-ac6f-7901f23b2903",
    "lat": 28.6390696,
    "long": 77.378886,
    "place": "Wild Ginger",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "8005c99e-132e-45c2-88c9-9a69757fcb1c",
    "lat": 28.60283,
    "long": 77.37911,
    "place": "Trader Vic's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "804f92e9-befc-4a7d-8d6b-c7c866b92beb",
    "lat": 28.57518,
    "long": 77.1387,
    "place": "The Tasting Room",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "806a2b4d-89b3-4a4d-aed0-aa42db900c64",
    "lat": 26.9162,
    "long": 75.70236,
    "place": "Typhoon",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "809533bc-df8c-454f-88f9-1444eb360ef2",
    "lat": 28.5541,
    "long": 77.33726,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "80e129cf-7b14-482f-a177-9f2cfb868a5a",
    "lat": 13.1002634,
    "long": 80.2801182,
    "place": "Market Cellar Winery",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "81009d9e-536d-463b-8b75-7a6dca6a883d",
    "lat": 26.81037,
    "long": 75.84769,
    "place": "Manzana Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "813a8060-7631-4bd2-a8de-0bfaa218a39c",
    "lat": 28.63215,
    "long": 77.30657,
    "place": "Salish Lodge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "81957eb9-d8a9-4f71-aac6-aab13d7ee9c7",
    "lat": 19.14199,
    "long": 72.8421,
    "place": "Assagio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "81bc90f1-220e-4b4c-843a-b9e8ce4caafa",
    "lat": 28.39654,
    "long": 76.96959,
    "place": "Buca di Beppo",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "81ea53c4-20f8-47d4-9466-d865afe86271",
    "lat": 23.0184861,
    "long": 72.5615402,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "822b143c-cb35-491c-b5e4-8edce1b3f6eb",
    "lat": 12.95367,
    "long": 77.62276,
    "place": "Cutters Bayhouse",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "823dcc8f-93e8-4d18-a820-77d991eb3ced",
    "lat": 28.4709304,
    "long": 76.9965945,
    "place": "Italianissimo Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "827d3ac0-b8a9-407d-8316-0117775b6bac",
    "lat": 28.56416,
    "long": 77.38495,
    "place": "Wasabi Bistro",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "82829e93-03d2-4e3b-a039-458006a581c9",
    "lat": 28.5864383,
    "long": 77.3662833,
    "place": "Assagio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "829c9b11-e96b-4bbc-a180-40401f7bcd5d",
    "lat": 12.95793,
    "long": 77.69446,
    "place": "Barking Frog",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "82a24883-685b-4086-8a97-a7e317eb7b06",
    "lat": 28.6871071,
    "long": 77.3666599,
    "place": "I Love Sushi",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "82b24979-cea5-4812-83a8-7d9262be6527",
    "lat": 28.40205,
    "long": 77.08866,
    "place": "Coho Cafe",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "82bd1eb0-5bb0-464b-b7ff-a6dfc8c5fb22",
    "lat": 18.955,
    "long": 72.83557,
    "place": "Royal India",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "830bf869-ac47-4900-83dc-87a5a0aac3b4",
    "lat": 28.5519447,
    "long": 77.3505566,
    "place": "The Calcutta Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "834d5bd1-8a54-4add-adf1-4d4f6b92d7f2",
    "lat": 23.10603,
    "long": 72.5909,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "83567c4b-2ccc-4bc1-b7f6-d2c5e76189ee",
    "lat": 28.70366,
    "long": 77.10371,
    "place": "Coho Cafe",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "83581afe-683e-4fde-b7ae-bbbcba6c44e9",
    "lat": 28.72665,
    "long": 77.16975,
    "place": "Redhook Ale Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8366906b-6681-4ade-a459-8459fa1c1eb6",
    "lat": 12.9241,
    "long": 77.6671,
    "place": "Mac and Jack’s Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "83673b25-2f4a-41ce-a06d-a46e7616b769",
    "lat": 28.46736,
    "long": 77.08113,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8372e63e-e1bb-4197-acb8-0f079e680512",
    "lat": 28.6023246,
    "long": 77.0745418,
    "place": "Sans Souci",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "837bb0b2-6f4b-4c24-9a4e-b4ef31751290",
    "lat": 19.0141,
    "long": 72.84275,
    "place": "Café Juanita",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "83bc80f8-41ae-4aef-b6d1-e04700cb5a95",
    "lat": 28.70528,
    "long": 77.19768,
    "place": "Kaspar's",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "83c62a42-0ce9-4ab3-85e5-af95db0ec03f",
    "lat": 28.40872,
    "long": 77.03516,
    "place": "Assagio",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "83c9e1ab-be0e-44be-8dd2-ec4dea2e6549",
    "lat": 19.07383,
    "long": 72.8234,
    "place": "Space Needle",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "83d2a543-de79-4d8a-8914-89c6d0c5f3e3",
    "lat": 28.46071622,
    "long": 77.07922788,
    "place": "Space Needle",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "83d4e39a-b136-4e06-8222-571eeaacb46a",
    "lat": 28.7165358,
    "long": 77.1260448,
    "place": "Isabella Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "83e3887b-8068-4692-9698-d2c6bbe809f9",
    "lat": 19.1152,
    "long": 73.00291,
    "place": "Cutters Bayhouse",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "83efd0b4-9018-4de8-b9e0-4e6d53c21ff9",
    "lat": 28.55709,
    "long": 77.28694,
    "place": "Salish Lodge",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "83fbcf64-22ed-4dc6-92ae-42aa5540ffde",
    "lat": 28.55592,
    "long": 77.22094,
    "place": "Dahlia Lounge",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "84049737-8138-4f1d-8cf6-5f48dda816a7",
    "lat": 22.56732,
    "long": 88.47365,
    "place": "Market Cellar Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "84362d7f-e0ff-4d24-a0fa-590ff21ce8c1",
    "lat": 28.43996,
    "long": 77.06317,
    "place": "Coho Cafe",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8465c87b-3c12-43a6-b44f-bae34ae7bb6a",
    "lat": 28.42752,
    "long": 77.10365,
    "place": "Pike Brewing Company",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "846bde2b-0e21-4af9-8bcc-f650f1e514ef",
    "lat": 28.48394,
    "long": 77.01859,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "847f3580-b55f-4f0e-926b-a514a985d765",
    "lat": 26.834547,
    "long": 80.9252696,
    "place": "Buca di Beppo",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "84965397-3390-46ac-acd6-371801ab9f12",
    "lat": 28.6063017,
    "long": 77.0636245,
    "place": "Rover's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "84c77036-d402-41ca-aa46-09baee595810",
    "lat": 19.15019,
    "long": 72.83201,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "850f741d-df1e-4ad5-a5f9-6f9a2ee1f718",
    "lat": 28.6390069,
    "long": 77.07441287,
    "place": "Cutters Bayhouse",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "8521ce68-bded-4325-a662-ca636e3349ca",
    "lat": 28.64153,
    "long": 77.10352,
    "place": "Soos Creek Wine Cellars",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8525d6dc-3a77-442f-9ad5-e379c099176a",
    "lat": 19.10489,
    "long": 72.87862,
    "place": "Il Fornaio",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8528f51b-54ca-42d4-8a86-e19903ab190c",
    "lat": 28.72665,
    "long": 77.16975,
    "place": "Kaspar's",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "852c4953-9d56-4606-b76f-c9a102862be0",
    "lat": 28.62037,
    "long": 77.37038,
    "place": "Mac and Jack’s Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "85489ec0-3679-47e9-83a8-ba09e923d2dc",
    "lat": 28.59469,
    "long": 77.34066,
    "place": "Il Fornaio",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "854afb11-325a-4edf-bedc-82161668a2e5",
    "lat": 28.43645,
    "long": 77.06004,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "854c6064-e43c-401b-ae38-13f9c0e3f4e7",
    "lat": 28.51641,
    "long": 77.20586,
    "place": "Maggiano's Little Italy",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "8558edba-a728-4949-b674-3705e5f0fdfc",
    "lat": 28.633,
    "long": 77.30493,
    "place": "Dahlia Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "8567abf8-7ba8-4b8f-a811-a0efc70b952b",
    "lat": 28.43645,
    "long": 77.06004,
    "place": "Yarrow Bay Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "8590a7c6-9dd7-4bf6-954f-8139a4ab1126",
    "lat": 28.6330519,
    "long": 77.3122682,
    "place": "Buca di Beppo",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8595ffbe-bec6-48e2-8c27-41a14678b8cb",
    "lat": 22.58035,
    "long": 88.42564,
    "place": "Sans Souci",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "8599ef30-1bb9-46e2-a9aa-0a5c762c042b",
    "lat": 12.967139,
    "long": 77.7410435,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "85ef272e-7ad9-4641-aa5a-1f5a582e3955",
    "lat": 12.9822529,
    "long": 77.6158592,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "861dd931-6b2d-4ee3-95b6-0f35f9c7cc09",
    "lat": 23.055,
    "long": 72.63162,
    "place": "Soos Creek Wine Cellars",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "861df4bb-699e-4311-a93d-c3759c434ea7",
    "lat": 28.47706,
    "long": 77.0772,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "8642f76d-11be-4f4b-bf70-77b78f70b582",
    "lat": 22.47568,
    "long": 88.36891,
    "place": "Uptown China Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "864a3d5c-e537-4047-b386-7cc0e97d74c4",
    "lat": 28.66052,
    "long": 77.35902,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "864fb176-f353-4f4a-bfa0-9b7e0d1e38ab",
    "lat": 18.96946,
    "long": 72.8255,
    "place": "I Love Sushi",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "865a8b2e-982b-461f-b802-6c1170cae387",
    "lat": 28.5546005,
    "long": 77.2895799,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "86741502-861c-4b8b-8a18-53eb45edcd0a",
    "lat": 28.56957,
    "long": 77.16807,
    "place": "Redhook Ale Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8697547a-6250-4720-9735-16be626445d3",
    "lat": 22.56752,
    "long": 88.4733,
    "place": "Soos Creek Wine Cellars",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "86c47a45-1b38-449c-9146-23901a5ed8ea",
    "lat": 28.55212,
    "long": 77.29886,
    "place": "Buca di Beppo",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "86e63d18-490e-42e9-b3f3-cdfe278decf1",
    "lat": 19.07579,
    "long": 72.82612,
    "place": "Manzana Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "86ecb881-bc38-4202-a58c-5f6067073536",
    "lat": 28.63611,
    "long": 77.18045,
    "place": "Bis On Main",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "86fc1ec5-efbf-4723-a7ae-c43f4050d64d",
    "lat": 28.53193,
    "long": 77.20791,
    "place": "Maggiano's Little Italy",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "873503e2-1966-4907-bdd6-8f2cfd330605",
    "lat": 28.6330519,
    "long": 77.3122682,
    "place": "Desert Fire",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "875ff7f2-709c-4936-b397-9240381db348",
    "lat": 23.10912,
    "long": 72.59097,
    "place": "Trader Vic's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8773f74c-eb85-4dfb-b3ad-38dd189d60b8",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "Desert Fire",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "87c30af3-5bd7-407a-bb27-a8b4755cbea6",
    "lat": 28.62784,
    "long": 77.30252,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "87f49206-21fe-4f94-a688-6967904f50a1",
    "lat": 19.10489,
    "long": 72.87862,
    "place": "Assagio",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "87f85290-40eb-4e2e-940c-ead9ee606f4d",
    "lat": 22.49519,
    "long": 88.3379917,
    "place": "Dahlia Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "8810b45b-a146-48af-957d-bf53db244833",
    "lat": 12.96071,
    "long": 77.7133,
    "place": "Isabella Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8815a40d-cc80-480c-beb4-dde8b8816232",
    "lat": 28.62704,
    "long": 77.11084,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "882268ab-dbba-4ec1-9eed-bedcffa84155",
    "lat": 13.11227,
    "long": 80.27277,
    "place": "I Love Sushi",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "88318d12-8d11-4653-a842-fc0f9a2a2d78",
    "lat": 28.67917,
    "long": 77.29048,
    "place": "Desert Fire",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "888aa83c-0824-4c1f-9c73-e40515785582",
    "lat": 28.65326,
    "long": 77.30205,
    "place": "Sea Garden",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "88c38d56-4c63-4558-b146-770d24daf473",
    "lat": 26.78104,
    "long": 80.95174,
    "place": "Chutney's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "88c82daf-054f-44c1-98f4-6626c8f9fdda",
    "lat": 12.99224,
    "long": 80.1978,
    "place": "Space Needle",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "88dbe267-0459-49c2-bb82-a512ee0180e9",
    "lat": 28.5714694,
    "long": 77.3845685,
    "place": "Columbia Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "88efff18-d331-48b0-b14e-12a84c630b0c",
    "lat": 28.47572,
    "long": 77.51778,
    "place": "Space Needle",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "88f6e839-5794-4c54-8b1d-9e2e37a8cded",
    "lat": 12.91375,
    "long": 77.62041,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "89184149-0d44-46dd-b4fa-5aa0cd404c9c",
    "lat": 28.63545286,
    "long": 77.3646536,
    "place": "Market Cellar Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "896372c9-61f5-4f2d-a873-bb0b26254556",
    "lat": 28.5087,
    "long": 77.0192,
    "place": "Trader Vic's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "89756d8c-4f5a-437a-821a-39c9189d2342",
    "lat": 26.80442,
    "long": 80.91034,
    "place": "Uptown China Restaurant",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "89b2570d-9182-4f24-8583-9fbe59599c63",
    "lat": 28.62567,
    "long": 77.43761,
    "place": "Redhook Ale Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "89c135b2-eab6-474d-8ac0-4ea2c73bbff2",
    "lat": 28.5071507,
    "long": 77.2357335,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "89cb9ba1-c1a9-47f9-83dd-3edaf65a1683",
    "lat": 22.61643,
    "long": 88.41209,
    "place": "Sans Souci",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "89e9ef66-2de2-46cf-849c-d602a7246e5c",
    "lat": 28.57949,
    "long": 77.15159,
    "place": "Space Needle",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8a04dbc7-f735-40b6-b2ce-996c23ade9f1",
    "lat": 28.5152,
    "long": 77.20414,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "8a278976-0f44-4238-8ed3-aea45360fd0a",
    "lat": 19.0338989,
    "long": 73.0718803,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8a45f05d-79ae-4d19-845a-810667139823",
    "lat": 28.6533,
    "long": 77.30209,
    "place": "Mac and Jack’s Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8a546fe6-2c86-4374-8206-2b6c3aafc151",
    "lat": 19.02008,
    "long": 73.01782,
    "place": "Sea Garden",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "8a6ccf14-4298-4078-88d3-c30df6bffd0b",
    "lat": 12.9818786,
    "long": 77.6155783,
    "place": "Sans Souci",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "8a8f7eea-900f-4aa3-bc27-e7b7244036f4",
    "lat": 12.97341,
    "long": 77.60246,
    "place": "Isabella Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "8a90d1eb-7a59-4c44-8e22-9c58b13a1cca",
    "lat": 28.5889147,
    "long": 77.0768528,
    "place": "Coho Cafe",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8a9326e0-1159-41f0-af6d-36c6763e3507",
    "lat": 28.67788,
    "long": 77.18126,
    "place": "Space Needle",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8aa7a3c9-1df9-4b32-b39a-fac0fce7422b",
    "lat": 28.6862494,
    "long": 77.2838548,
    "place": "Uptown China Restaurant",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "8aa9f5ba-53de-4515-aa47-207ce5d93026",
    "lat": 12.83353,
    "long": 77.70163,
    "place": "Soos Creek Wine Cellars",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8ab26fa1-2ce3-4391-bb99-9edac1ed7b4f",
    "lat": 28.4634,
    "long": 77.56355,
    "place": "The Calcutta Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8ac6a220-d129-4dec-8103-78065288e513",
    "lat": 28.49107,
    "long": 77.0931,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "8b30a549-1172-444b-b19a-93f44d25487f",
    "lat": 28.67894088,
    "long": 77.16024314,
    "place": "Maggiano's Little Italy",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "8b4232d0-8d1a-4c65-8dfa-1177c5a05bb3",
    "lat": 28.4994595,
    "long": 77.2343836,
    "place": "Salish Lodge",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "8b534e98-5726-4e19-9530-71086e09e43b",
    "lat": 19.1253,
    "long": 72.91842,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8b7b017d-fe26-4499-8b8c-53b913dc7e65",
    "lat": 28.4266066,
    "long": 77.085132,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "8b8dccbd-8f35-4772-b740-e1dfd19c9982",
    "lat": 28.5142,
    "long": 77.20494,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8b917459-3ef3-4c25-af0d-3672d9ae04e9",
    "lat": 28.6389693,
    "long": 77.0742992,
    "place": "Space Needle",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "8b9a55d7-868d-4660-955e-73b840f087a7",
    "lat": 19.0189746,
    "long": 73.0161658,
    "place": "Wild Ginger",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "8bbe12a0-9111-4883-8f38-ff6c8b1863b9",
    "lat": 28.70801,
    "long": 77.17149,
    "place": "Maggiano's Little Italy",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "8bcfcf1f-5c3d-4265-a7c9-e745d3eb69f9",
    "lat": 28.5789,
    "long": 77.38166,
    "place": "The Calcutta Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8bdea99d-6b33-45bc-b8cc-ffd926abf94a",
    "lat": 22.46282,
    "long": 88.33318,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8bf75a32-32fd-43c0-9ccf-811fab669a88",
    "lat": 28.58031,
    "long": 77.33024,
    "place": "Yarrow Bay Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "8c146da1-5984-4eb8-b038-ba3c1f9faeeb",
    "lat": 28.6533,
    "long": 77.30209,
    "place": "Royal India",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "8c522212-0551-4953-b76a-cb4991959977",
    "lat": 18.98044,
    "long": 72.84131,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "8cd983d8-4ef8-4220-8632-5a854808054a",
    "lat": 28.67897,
    "long": 77.33562,
    "place": "Palomino",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8d0dc097-5a68-4947-8a3b-07f01cbca656",
    "lat": 28.35677,
    "long": 77.29148,
    "place": "Cutters Bayhouse",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8d30cb43-0da6-48c3-8378-4c8a8f505553",
    "lat": 28.5519447,
    "long": 77.3505566,
    "place": "Bis On Main",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "8d379507-ff67-4d96-9040-138ebcb0503d",
    "lat": 22.61008,
    "long": 88.40938,
    "place": "Soos Creek Wine Cellars",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "8d49ec14-4a24-4836-bb8a-814335c9ee4c",
    "lat": 19.01223,
    "long": 72.84204,
    "place": "Herb Farm",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "8d5bc60d-e2bb-4fd9-8a97-75b62a00568c",
    "lat": 28.6862494,
    "long": 77.2838548,
    "place": "Uptown China Restaurant",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8d8ad519-6691-4f5d-873d-ea3205452651",
    "lat": 23.055,
    "long": 72.63162,
    "place": "Buca di Beppo",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "8da61cb0-54d5-4695-ad52-003b52232205",
    "lat": 12.96441,
    "long": 77.64945,
    "place": "Barking Frog",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8da6acd9-4649-4f19-a95b-104dccb56436",
    "lat": 28.5240194,
    "long": 77.1930409,
    "place": "Market Cellar Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8dc25d48-42f7-4a6b-b96b-6fe701d4c598",
    "lat": 28.64854,
    "long": 77.10932,
    "place": "Chutney's",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "8de25908-2733-4207-8536-168c93e731cc",
    "lat": 28.5661,
    "long": 77.22022,
    "place": "Sea Garden",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "8de39db0-6d2f-4636-bc5f-85f70296cefc",
    "lat": 18.6010616,
    "long": 73.7833899,
    "place": "Pike Brewing Company",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8dfff2e6-b10c-4a0c-a09c-a549e5c12c1a",
    "lat": 18.95526,
    "long": 72.83603,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "8e05058e-94c5-4014-8c42-25124a928f3c",
    "lat": 28.64585,
    "long": 77.1631,
    "place": "Trader Vic's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "8e05257a-03a2-4141-879a-d9213d961681",
    "lat": 28.52196,
    "long": 77.22684,
    "place": "Barking Frog",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "8e3932ca-b38e-4056-b715-aae4bc8f40a0",
    "lat": 19.12542,
    "long": 72.91894,
    "place": "Coho Cafe",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8e42c3a0-a76d-43fb-855e-179e3d02bbc8",
    "lat": 28.56103,
    "long": 77.22723,
    "place": "Buca di Beppo",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "8e4514bf-3f52-4718-86cd-3e9fadc1c609",
    "lat": 28.5741418,
    "long": 77.39024,
    "place": "Coho Cafe",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8e52a8d4-e8ec-4b42-b5da-ff2381d89795",
    "lat": 12.96069,
    "long": 77.7133,
    "place": "Salish Lodge",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "8e58dcc2-0797-457b-b1ca-38aca4e4d28c",
    "lat": 28.6041,
    "long": 77.04788,
    "place": "Italianissimo Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "8e8a2182-7046-4096-af24-e0eded364eec",
    "lat": 26.90493,
    "long": 80.95817,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "8ebc90b3-02c0-48ec-abb5-823ec88380af",
    "lat": 28.6385643,
    "long": 77.1836212,
    "place": "Il Fornaio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8f175261-9304-4847-83ce-7e3502694abf",
    "lat": 19.02008,
    "long": 73.01782,
    "place": "Desert Fire",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8f39b7d7-fd67-4047-a9d5-9bcccfc6d4f3",
    "lat": 12.9115315,
    "long": 77.6823752,
    "place": "Space Needle",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "8f3a56a0-68d6-4fdf-be6e-f30e15f9120f",
    "lat": 19.20575,
    "long": 72.98318,
    "place": "Isabella Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "8f3ee9f5-9061-431f-97a2-cb3b1972c12b",
    "lat": 28.5850391,
    "long": 77.356831,
    "place": "Cutters Bayhouse",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "8f479044-cc98-40ea-bda8-b2c57dea4c6f",
    "lat": 28.44283,
    "long": 77.07323,
    "place": "Mac and Jack’s Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8f4962a1-6d81-4975-b59a-cebca615f823",
    "lat": 28.47689,
    "long": 77.54738,
    "place": "Cutters Bayhouse",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "8f6fc5b2-a0e3-40a0-bc37-2f3f7648edab",
    "lat": 19.26213,
    "long": 72.95999,
    "place": "Maggiano's Little Italy",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "8fcdac6e-629e-423d-afde-db4be05e9764",
    "lat": 19.17082,
    "long": 72.87045,
    "place": "Wild Ginger",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8fdcb964-dd8c-493d-9c79-aa85d2a730e7",
    "lat": 23.05567,
    "long": 72.65281,
    "place": "Uptown China Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "8fe62515-50a6-4c51-af91-aaecfef22043",
    "lat": 19.19084,
    "long": 72.82029,
    "place": "The Calcutta Grill",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "8ff94a19-3287-4fcf-8334-bc75219ad847",
    "lat": 12.96818,
    "long": 77.50371,
    "place": "Herb Farm",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9057458a-39f4-46df-93e4-89f9a302ee1d",
    "lat": 17.41212,
    "long": 78.55732,
    "place": "Rover's",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "90656403-631d-465a-aedb-22ddca969bb6",
    "lat": 26.80445,
    "long": 80.91024,
    "place": "Uptown China Restaurant",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "907e9075-b627-48a3-b927-f5a49138aa88",
    "lat": 28.39637,
    "long": 77.05933,
    "place": "Isabella Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9080e703-1e41-4be4-b211-784b78f23ec7",
    "lat": 28.6063017,
    "long": 77.0636245,
    "place": "Café Juanita",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "90838d2d-d326-477a-a80e-4415bdf3a062",
    "lat": 12.9845616,
    "long": 77.5584315,
    "place": "Redhook Ale Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "90957888-2723-471e-adb5-7175f06065bb",
    "lat": 26.78072,
    "long": 80.96085,
    "place": "Typhoon",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "909d3fc7-47de-4c21-906e-1830ebdea545",
    "lat": 28.68358,
    "long": 77.1326,
    "place": "Salish Lodge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "90c8ab4c-7cf6-4c83-b0f3-eef86dff461d",
    "lat": 28.4440781,
    "long": 77.0511576,
    "place": "The Calcutta Grill",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "90cce465-d859-4295-bf7c-9c4ece5755d0",
    "lat": 28.66995,
    "long": 77.4478,
    "place": "Palomino",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "90d17a2d-a1b0-49fa-a629-94de303be2af",
    "lat": 17.39917,
    "long": 78.41779,
    "place": "Royal India",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "90d2f6c2-52b3-4cbf-8e78-1ba8df62b953",
    "lat": 26.78892,
    "long": 80.90964,
    "place": "Redhook Ale Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "90d6931d-b072-478e-a576-326024e92754",
    "lat": 28.62412,
    "long": 77.41956,
    "place": "Chutney's",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "90fda8ba-46eb-4ab4-8e60-8feec1e8517a",
    "lat": 19.10555,
    "long": 72.90003,
    "place": "Palomino",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "90febdd6-c180-4161-9e43-de5518939bf2",
    "lat": 17.5062,
    "long": 78.33048,
    "place": "Isabella Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "91229357-c67f-48a7-93ba-e38ab4d116aa",
    "lat": 28.68567,
    "long": 77.34823,
    "place": "Space Needle",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "91232e1f-268c-40ec-95de-7ae00ede9d3f",
    "lat": 28.50308,
    "long": 77.02709,
    "place": "Buca di Beppo",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "91366ffb-fe60-4158-932e-160fdf097cad",
    "lat": 28.4415,
    "long": 77.0635,
    "place": "Italianissimo Ristorante",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "9139ceeb-c3dd-4ebd-81d2-980546c01d62",
    "lat": 18.95525,
    "long": 72.8361,
    "place": "Maggiano's Little Italy",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9141186c-e244-4193-9c24-2b8c5b146ba2",
    "lat": 19.19084,
    "long": 72.82029,
    "place": "The Calcutta Grill",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "91713792-8282-4ae3-8114-c3dcc80f6c58",
    "lat": 26.8794116,
    "long": 81.0065383,
    "place": "Rover's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9181414d-a90c-4641-acc5-b97e0bbd1227",
    "lat": 18.91611,
    "long": 72.82682,
    "place": "Yarrow Bay Grill",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "91933cec-0fb2-4b12-b48c-7418b4a4240e",
    "lat": 28.45131235,
    "long": 76.9887128,
    "place": "Yarrow Bay Grill",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "91b9ce53-2927-4f5e-8259-c6da80736025",
    "lat": 28.6080773,
    "long": 77.3330067,
    "place": "Kaspar's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "91d805d5-e7a9-4872-b88d-5e888a2a0632",
    "lat": 28.68453,
    "long": 77.36199,
    "place": "Coho Cafe",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "91dcdbe5-f6e3-4861-92ce-684d47ffadf3",
    "lat": 28.70001,
    "long": 77.26805,
    "place": "The Tasting Room",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "91e1bc88-0bad-43dd-97f0-70adbc23c7e0",
    "lat": 19.14234,
    "long": 72.84225,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "9248d104-2fd0-41cc-aafc-57d0a5c53901",
    "lat": 28.53887,
    "long": 77.21411,
    "place": "Coho Cafe",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "924966a3-85b6-4aa9-bca7-710545038293",
    "lat": 28.47133,
    "long": 77.05241,
    "place": "Assagio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9254b7bd-ef44-406d-a9e6-0591aded325a",
    "lat": 22.50123,
    "long": 88.34768,
    "place": "Royal India",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "9269b3b1-1e79-404e-8858-37fecee09ac4",
    "lat": 28.45583,
    "long": 77.04009,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "928835ec-fb7c-443e-8230-2afe6d6a9d77",
    "lat": 22.99771,
    "long": 72.54815,
    "place": "Sans Souci",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "92cc9166-073f-433c-b4a4-e357a6a797e7",
    "lat": 17.50094,
    "long": 78.41087,
    "place": "The Calcutta Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "92ebf5d9-504c-4c18-96af-fd625d385291",
    "lat": 28.5071165,
    "long": 77.2358394,
    "place": "Manzana Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "92f4f0d4-f336-4be9-b21d-bdf564450d59",
    "lat": 28.51826,
    "long": 77.20217,
    "place": "Redhook Ale Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "92fb70dc-e50a-4f25-9a6c-148e5df6874e",
    "lat": 28.50173,
    "long": 77.37711,
    "place": "I Love Sushi",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "92fe0504-8bf5-4e98-a601-00824243c8d7",
    "lat": 18.49147,
    "long": 73.79222,
    "place": "Herb Farm",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "93197fb8-2d0a-4733-95f8-7e6a9ab8ff81",
    "lat": 28.50173,
    "long": 77.37711,
    "place": "Pike Brewing Company",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "932091f1-008f-480d-840f-83ddf02275e8",
    "lat": 26.91642,
    "long": 75.70251,
    "place": "Columbia Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "9326165a-0e89-4038-ba02-e0880d8d5058",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "The Tasting Room",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "93462d2f-288f-486f-803b-43a6bce304a5",
    "lat": 28.63199,
    "long": 77.2774,
    "place": "Soos Creek Wine Cellars",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "93c67bd5-639b-4474-935c-513034d9db53",
    "lat": 23.05641,
    "long": 72.65179,
    "place": "Salute of Bellevue",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "93dbbda1-493a-43aa-925f-9f5dc8fd7808",
    "lat": 28.63368,
    "long": 77.34193,
    "place": "Salish Lodge",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "940b5e7f-37ca-4b80-9fbd-75c1b832574f",
    "lat": 12.9214817,
    "long": 77.5197406,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "943bf581-030e-4357-ae1a-bb0b3d874db0",
    "lat": 22.5330313,
    "long": 88.3691788,
    "place": "Salish Lodge",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "945c5f9c-9dab-457d-a5fa-a62a8a683732",
    "lat": 28.49114,
    "long": 77.09291,
    "place": "Wild Ginger",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "9462f9ef-e2f2-4ac2-a954-7802a40bc3be",
    "lat": 28.48349949,
    "long": 77.09047267,
    "place": "Assagio",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "948c1e36-a85a-4df5-ab11-cb7cf1701331",
    "lat": 28.4428163,
    "long": 77.0732252,
    "place": "Bis On Main",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "94a49559-1159-463a-87cb-d7a888bf68af",
    "lat": 22.5366025,
    "long": 88.3496555,
    "place": "Space Needle",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "94d752c0-7b87-48e5-95cb-d9cc24f30acc",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "Mac and Jack’s Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "94dccfd7-39d1-4c03-8999-8301d6a07c0c",
    "lat": 22.62512,
    "long": 88.41747,
    "place": "Il Fornaio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "94eb1429-48bb-403a-a657-01f36a06fe70",
    "lat": 28.40021947,
    "long": 77.35586863,
    "place": "Redhook Ale Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9532345f-0ecd-4c8c-96ad-26674d729b25",
    "lat": 28.46247,
    "long": 77.04123,
    "place": "Salish Lodge",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "95508f85-f634-479e-834d-34d57d50bbc5",
    "lat": 28.67888,
    "long": 77.31291,
    "place": "Cutters Bayhouse",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9559b9b8-5db6-4eed-85ca-1c23d46fbb4b",
    "lat": 28.65327,
    "long": 77.30206,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9576c629-7f3a-4199-9cd7-f4d2980970f4",
    "lat": 28.4882887,
    "long": 77.0001589,
    "place": "Buca di Beppo",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "95942da6-b95f-4c9b-9255-6edcc87bd615",
    "lat": 12.99352,
    "long": 80.20019,
    "place": "Soos Creek Wine Cellars",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "959fc9de-7446-4611-a793-e20184afea11",
    "lat": 28.59467,
    "long": 77.34066,
    "place": "Coho Cafe",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "95b0f0f2-2ef4-44bd-82c0-424a2b9a4cfb",
    "lat": 28.7065292,
    "long": 77.0985168,
    "place": "Dahlia Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "95db8126-4000-49d3-b35a-2260abcc0552",
    "lat": 28.73603,
    "long": 77.16919,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "95ef4c10-4e1d-4370-944d-33db059420f2",
    "lat": 28.56816,
    "long": 77.34596,
    "place": "Kaspar's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "95fb3b19-f21d-46ea-965c-402c737132d3",
    "lat": 28.46953,
    "long": 77.29712,
    "place": "Coho Cafe",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "96065ff3-7aa4-402f-aa10-8d84023272dd",
    "lat": 28.7015348,
    "long": 77.0824682,
    "place": "The Tasting Room",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "960d9182-1187-4068-9e3b-e11832f3525a",
    "lat": 17.5062,
    "long": 78.33048,
    "place": "Space Needle",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "96372576-925d-4293-bab1-e7b0406123e5",
    "lat": 28.4447122,
    "long": 77.066974,
    "place": "The Calcutta Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "9649ab1f-2833-42b7-9d02-935cffb51ff6",
    "lat": 28.50173,
    "long": 77.37711,
    "place": "Trader Vic's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "96a8a42c-d5e5-46b7-acf9-a2e6e8ba8ffd",
    "lat": 23.01848,
    "long": 72.56154,
    "place": "Manzana Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "96b32512-7ce2-41ce-b625-af8046c93a9d",
    "lat": 28.6533,
    "long": 77.30209,
    "place": "Pike Brewing Company",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "96e0b5ff-638c-4d1e-91f4-8e1beb68f263",
    "lat": 28.5319197,
    "long": 77.2078526,
    "place": "Herb Farm",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "971622e7-a6b0-478b-9dc0-fc1f3342e122",
    "lat": 22.61009,
    "long": 88.40949,
    "place": "The Tasting Room",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "971c54ac-e057-40de-994a-6341e418e9a9",
    "lat": 28.4661264,
    "long": 77.0816006,
    "place": "Dahlia Lounge",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "974a944f-ee9b-4938-a66a-f5755d47b836",
    "lat": 28.70528,
    "long": 77.19765,
    "place": "Salute of Bellevue",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "976e22bc-6b7f-4800-85ad-0353bc859dec",
    "lat": 12.90071,
    "long": 77.6538,
    "place": "Pike Brewing Company",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "977eefd2-05a0-4779-8e4a-8b7cc55962e2",
    "lat": 28.5745459,
    "long": 77.2283789,
    "place": "Palomino",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "97a21574-f05c-48e5-8dac-307b51ca581d",
    "lat": 28.42134,
    "long": 77.31762,
    "place": "Palomino",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "97be27e9-d3ac-4454-8967-2bd89c9d7af6",
    "lat": 12.91106,
    "long": 77.56627,
    "place": "Pike Brewing Company",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "97dcfdc3-d606-4bd2-8c52-f684eeb0bb42",
    "lat": 28.57909,
    "long": 77.38177,
    "place": "Soos Creek Wine Cellars",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "97f9dc05-5c20-4818-8452-3c99100232c2",
    "lat": 28.62704,
    "long": 77.11084,
    "place": "Rover's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "98266ed7-b892-49e0-9ff3-70a3c4212b0d",
    "lat": 28.36173,
    "long": 77.2969,
    "place": "Barking Frog",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "9857b4f4-9a05-4fe2-a0fc-22afbb5283b9",
    "lat": 28.49679,
    "long": 77.18027,
    "place": "I Love Sushi",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9884e332-752f-4b43-87c9-cab6c4658f44",
    "lat": 28.60808,
    "long": 77.33301,
    "place": "Wasabi Bistro",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "98bcb5d6-b888-49e2-8334-82842ba5fbc8",
    "lat": 23.06391,
    "long": 72.52304,
    "place": "Isabella Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "98cd8305-628a-42fd-b01b-1fdd113102c9",
    "lat": 28.72664,
    "long": 77.16973,
    "place": "Rover's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "98e4ac55-cb12-4c5e-abeb-0278bedcf1cf",
    "lat": 28.6553,
    "long": 77.16448,
    "place": "Manzana Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "98ee2404-5283-4409-bafd-4a327ded361d",
    "lat": 28.6533,
    "long": 77.30209,
    "place": "Sea Garden",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "98ffede3-e3d8-4ae3-8ad6-6d9d399d0945",
    "lat": 12.84219,
    "long": 77.64598,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9930a2ac-639a-4898-8b9e-1623ed930c80",
    "lat": 28.56816,
    "long": 77.34596,
    "place": "Uptown China Restaurant",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "99368c54-c6a9-45fd-9e59-8399cdd48196",
    "lat": 28.6323,
    "long": 77.2964,
    "place": "Typhoon",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "994b96ca-4f06-43b9-83d2-b1f0135bc332",
    "lat": 28.45583,
    "long": 77.04009,
    "place": "Market Cellar Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "996df7f8-bef6-4f1e-9b6b-94b9efbed396",
    "lat": 19.14198,
    "long": 72.8421,
    "place": "Herb Farm",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "997e2723-cc76-463a-a1d5-2d46f28b870d",
    "lat": 28.7065292,
    "long": 77.0985168,
    "place": "Il Fornaio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "99d05b46-033c-4b45-a2ec-f1ac3928dc7d",
    "lat": 18.98044,
    "long": 72.84131,
    "place": "Herb Farm",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "99d41a2f-9774-4b44-8e2e-6ff308169cff",
    "lat": 28.60625,
    "long": 77.39879,
    "place": "Royal India",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "99ec7e51-6556-4a3d-b0b3-568eed08288b",
    "lat": 28.62325,
    "long": 77.2948,
    "place": "I Love Sushi",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "99ee0d82-4701-4df1-9f0c-cea6f94c1a28",
    "lat": 23.055,
    "long": 72.63162,
    "place": "Buca di Beppo",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9a04dc16-69aa-402f-ae39-fec46858f2e8",
    "lat": 28.6218186,
    "long": 77.1222418,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9a10632e-c39d-4fb8-8365-56c9c1918dbb",
    "lat": 28.64606,
    "long": 77.163,
    "place": "Typhoon",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "9a376f54-83d6-4932-a448-a49eb321be19",
    "lat": 12.99357,
    "long": 80.19993,
    "place": "Bis On Main",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9a4d3161-b97c-490c-8cd0-2f22aaf8de86",
    "lat": 17.36926,
    "long": 78.5346,
    "place": "Trader Vic's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "9a540f12-ad77-44ea-bd0c-e0e681244f2a",
    "lat": 18.59291,
    "long": 73.79789,
    "place": "The Calcutta Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "9a592de4-f089-436d-bb73-429e3afc68ae",
    "lat": 28.6023246,
    "long": 77.0745418,
    "place": "Royal India",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9a67b7e3-0c87-466f-b042-149240071a1c",
    "lat": 17.47672,
    "long": 78.54194,
    "place": "The Tasting Room",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "9a6be36e-d40a-4e95-a987-cc2ddbdf92d0",
    "lat": 22.49519,
    "long": 88.3379917,
    "place": "Il Fornaio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9a7fc59c-d3a0-4bb5-a745-b261ca0c17d8",
    "lat": 28.56801,
    "long": 77.38309,
    "place": "Pike Brewing Company",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9a831286-2bcb-45c3-b217-ca7176045fbf",
    "lat": 12.92123,
    "long": 80.15504,
    "place": "Salish Lodge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9a98fefa-73fc-40b1-8ff7-87d0100bf032",
    "lat": 28.5916616,
    "long": 77.3750196,
    "place": "Uptown China Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9b02ff8a-6404-4a36-a0f1-650152610eea",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "Barking Frog",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9b2a21ce-e04e-48c0-b69d-9359e78fca4f",
    "lat": 28.62747,
    "long": 77.30862,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "9b2d4b20-482e-46bc-add4-8464d6623d70",
    "lat": 28.491613,
    "long": 77.177957,
    "place": "Rover's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9b32dff2-3895-41da-b080-5fb09f5c73cb",
    "lat": 28.6441516,
    "long": 77.3449637,
    "place": "Wild Ginger",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9b4befd3-b86d-44c9-b904-2cd9d857165e",
    "lat": 28.4466423,
    "long": 77.0899017,
    "place": "Coho Cafe",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9b5bf8a2-96cc-483f-aed5-35a44c7fc424",
    "lat": 19.23653,
    "long": 72.9726,
    "place": "Typhoon",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9b5e5e5e-ae5e-474c-9933-78d2a0e53cee",
    "lat": 19.17082,
    "long": 72.87045,
    "place": "Coho Cafe",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "9b6dd9f0-59e2-4bd7-b53e-dadc27a18719",
    "lat": 17.41209,
    "long": 78.55735,
    "place": "Soos Creek Wine Cellars",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "9ba1d0cf-57ac-43e2-9608-ead5a143d3a9",
    "lat": 18.53289,
    "long": 73.94693,
    "place": "Market Cellar Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "9ba22b35-2ff0-4034-a1bf-c582be895e71",
    "lat": 19.20601075,
    "long": 72.96031713,
    "place": "Space Needle",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9bbe287d-1ba4-429e-9e16-11fcbd773633",
    "lat": 28.43691621,
    "long": 77.0553406,
    "place": "Columbia Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "9bc37320-43b8-4bff-9757-3614d14100c4",
    "lat": 28.62652,
    "long": 77.05782,
    "place": "Market Cellar Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9bc75069-fc03-4d52-972d-e4ab4b45be3f",
    "lat": 28.45159,
    "long": 77.01877,
    "place": "Palomino",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "9bc89e86-e6c2-4c69-a9c9-f3abb7c9ef4e",
    "lat": 28.39944,
    "long": 77.35614,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "9bdcaaea-6fd4-4f90-be6d-86a5ce4a68ee",
    "lat": 28.4356892,
    "long": 77.0566262,
    "place": "Herb Farm",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9be3cb48-f11e-4372-8cd4-4b221d1d78a9",
    "lat": 22.99771,
    "long": 72.54815,
    "place": "Bis On Main",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "9beb00bc-e103-4264-b3ba-49968a51a07b",
    "lat": 18.91609,
    "long": 72.82688,
    "place": "Il Fornaio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9c2192aa-00d3-478d-9544-ac269d27f3b7",
    "lat": 28.39655,
    "long": 76.9696,
    "place": "Cutters Bayhouse",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "9cb1192b-bd9d-46f7-8552-2b02653285d2",
    "lat": 28.5717236,
    "long": 77.1652293,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9cc2f446-4a2e-4cff-9712-4a09507fb957",
    "lat": 18.9641301,
    "long": 72.833509,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9cda08c1-b634-4cf3-98ce-12d5575931df",
    "lat": 18.95677,
    "long": 72.82302,
    "place": "Salute of Bellevue",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "9cf04414-32fd-46c6-ba80-dd20ae4275b4",
    "lat": 28.61705,
    "long": 77.03334,
    "place": "Assagio",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "9d3f4494-2d3b-4e85-8db8-8c3c7cf2aa32",
    "lat": 28.5942167,
    "long": 77.1165967,
    "place": "Italianissimo Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9d4543f2-2cd5-45bb-bb9f-c508379a4084",
    "lat": 28.6278,
    "long": 77.30252,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "9d47cf94-a9eb-4627-bd7d-9e136a6199de",
    "lat": 28.5071165,
    "long": 77.2358394,
    "place": "Uptown China Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "9d5dd82a-25fd-4301-928f-51be695e2d9a",
    "lat": 19.05035,
    "long": 73.0746,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9d8e1f9d-ee55-460a-a3d4-81f9e5ca28f5",
    "lat": 28.39688,
    "long": 76.96964,
    "place": "Maggiano's Little Italy",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9d92826e-9d50-4683-8593-ff71928f0b52",
    "lat": 23.055,
    "long": 72.63162,
    "place": "Desert Fire",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "9dd734ea-5345-4b75-9578-a129cb52a9d2",
    "lat": 28.63822,
    "long": 77.30227,
    "place": "Café Juanita",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9de3c996-2a81-45f1-9d14-a050e5f816c2",
    "lat": 23.01857,
    "long": 72.56164,
    "place": "Wild Ginger",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9df32fb6-60ba-4ad4-9529-6d5f7422e39a",
    "lat": 28.6491,
    "long": 77.37347,
    "place": "The Calcutta Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9df82142-4da0-4bd6-8c81-d933e166fd91",
    "lat": 19.0338989,
    "long": 73.0718803,
    "place": "Kaspar's",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "9e3e4c55-1d12-41b2-9777-5e9bdbe226f5",
    "lat": 28.6365279,
    "long": 77.3367275,
    "place": "Maggiano's Little Italy",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9e527b66-eb08-4067-bfe7-2cd8b026ddd6",
    "lat": 28.57949,
    "long": 77.15159,
    "place": "Barking Frog",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "9e665399-25bd-4ab9-b36b-ec3d8911baf0",
    "lat": 28.7065292,
    "long": 77.0985168,
    "place": "Palomino",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "9e7c7326-08ed-42f0-9569-e5bdf764e5f3",
    "lat": 28.46795,
    "long": 77.08669,
    "place": "Café Juanita",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "9e91b444-e57e-475f-bae7-6a730974a492",
    "lat": 28.46002,
    "long": 77.0791,
    "place": "The Tasting Room",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9e925c01-f3e7-440c-adc0-d205ec9dd1d8",
    "lat": 22.59095,
    "long": 88.3415,
    "place": "Salish Lodge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9eb54e00-56dc-47be-ada3-93bd64d021a5",
    "lat": 19.01358,
    "long": 72.84245,
    "place": "Royal India",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9f2b9480-b923-4cfd-9e46-5e4eb796b2f0",
    "lat": 17.5062,
    "long": 78.33048,
    "place": "Buca di Beppo",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9f2bd71c-ac8b-4d8f-9a40-e9a63154186f",
    "lat": 23.02689,
    "long": 72.58584,
    "place": "Sea Garden",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "9f4d0124-7fc8-4dfa-b01e-871814b14c95",
    "lat": 28.5149824,
    "long": 77.2016954,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9f58b3e0-9455-4d7b-a431-547c09e28394",
    "lat": 28.6862494,
    "long": 77.2838548,
    "place": "Wild Ginger",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "9f5a8e27-16d8-410e-82cb-3d22653fd0a4",
    "lat": 28.42324,
    "long": 77.06348,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9f7d95d8-f776-4605-8f39-4cbd424c6aa3",
    "lat": 22.4869512,
    "long": 88.3110157,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9f8cf91b-c0c3-4da1-9250-35119652597d",
    "lat": 22.49519,
    "long": 88.3379917,
    "place": "Palomino",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "9fd2053e-e3a4-418c-9a4e-6f09862a4ed9",
    "lat": 28.40872,
    "long": 77.03516,
    "place": "Isabella Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "9fe00e6a-d1f0-4316-b401-03fca71a6caf",
    "lat": 12.95657,
    "long": 77.55438,
    "place": "Wasabi Bistro",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "9fe805ac-d76e-4ec5-a723-25e14abe5e46",
    "lat": 22.5751,
    "long": 88.35735,
    "place": "Redhook Ale Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "a0069533-4e58-4f07-a845-1e5fd09bc4ff",
    "lat": 28.55015,
    "long": 77.29048,
    "place": "Salute of Bellevue",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a00b2bb3-b963-4377-92ab-77b3be2f8f33",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "Trader Vic's",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "a030b02a-7231-4f7d-afa0-3255a44b2108",
    "lat": 17.4490936,
    "long": 78.3035785,
    "place": "Isabella Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "a04ce363-e0fc-44ae-8ea5-2a228c8ccdde",
    "lat": 12.98039,
    "long": 77.67786,
    "place": "Tap House Grill",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a0522baf-bb07-4886-856c-f728fbfcdc17",
    "lat": 28.36173,
    "long": 77.2969,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a09547ca-70e0-4f0a-af7f-ca7e363b518b",
    "lat": 28.6263913,
    "long": 77.0579048,
    "place": "Typhoon",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "a0b51a92-f337-40fb-806f-e78de3d631c7",
    "lat": 19.12204,
    "long": 72.82579,
    "place": "Tap House Grill",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a0c4dc0c-a3d5-4644-b87b-a780d2004505",
    "lat": 18.45977,
    "long": 73.7852,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "a0e41352-2e25-46ce-ade2-7e69b94f9c83",
    "lat": 28.63775,
    "long": 77.3132,
    "place": "Maggiano's Little Italy",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a127dd04-1ed7-4984-963f-512f56f274ec",
    "lat": 23.00549,
    "long": 72.62121,
    "place": "Italianissimo Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a1912862-d10c-411d-856f-92788eef364d",
    "lat": 28.47683,
    "long": 77.04772,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "a1a2074d-35d7-4cf1-85b3-a16b1be0f864",
    "lat": 23.01856,
    "long": 72.56148,
    "place": "Assagio",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "a1dbb1f7-a8a3-4b5c-813d-3b637d539604",
    "lat": 28.4757552,
    "long": 77.0382683,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "a1df1068-2376-4a9e-8f4b-dfaefc4d452b",
    "lat": 28.6163725,
    "long": 77.2829117,
    "place": "Mac and Jack’s Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a1f3d76c-2316-42a3-a09a-eae6172f39e8",
    "lat": 28.40449,
    "long": 77.36368,
    "place": "Uptown China Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a1fe36e2-959f-4739-a911-8865140a13bf",
    "lat": 18.49159,
    "long": 73.79224,
    "place": "Wild Ginger",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a200a489-caf8-40e1-9e49-3fd37c31ca6d",
    "lat": 28.5631357,
    "long": 77.350686,
    "place": "Soos Creek Wine Cellars",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "a20147c5-ae90-45d8-9946-c3fc1d73676c",
    "lat": 22.57199,
    "long": 88.35692,
    "place": "Barking Frog",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a2015b91-9741-45a5-878b-d2a59e16f907",
    "lat": 28.51795,
    "long": 77.20258,
    "place": "The Tasting Room",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a21320a2-6270-4d27-bb71-fbb96f57c096",
    "lat": 28.4928,
    "long": 77.17797,
    "place": "Trader Vic's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a2500400-d021-4e5e-8127-ddd09feb52ee",
    "lat": 19.04878,
    "long": 73.07148,
    "place": "Uptown China Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a27db490-ef05-4509-ba51-12781ed88d18",
    "lat": 17.5062,
    "long": 78.33048,
    "place": "Cutters Bayhouse",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "a27eee7e-ce65-40f9-8b78-7a9aa080a984",
    "lat": 22.57202,
    "long": 88.35697,
    "place": "The Calcutta Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a2d8f81b-9545-499a-83ce-fe9980137692",
    "lat": 28.5618225,
    "long": 77.2429593,
    "place": "Cutters Bayhouse",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a2e49386-65a7-4a35-b71c-17ba16f058a1",
    "lat": 26.84489,
    "long": 81.00478,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a309726f-2801-49d2-bab1-1febaf5a0a80",
    "lat": 26.9162,
    "long": 75.70236,
    "place": "Typhoon",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "a35ffd22-d07f-4f72-be43-f4ddfe1887a6",
    "lat": 26.8794116,
    "long": 81.0065383,
    "place": "Columbia Winery",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "a3938090-dc79-4c2f-a0a4-3c7e2729d8f0",
    "lat": 19.019,
    "long": 73.01619,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a3af6256-c288-4179-8bb4-1a7b8e76f987",
    "lat": 28.58032,
    "long": 77.33023,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a3bed6fd-a4b3-403e-a8af-7ceebf9a4035",
    "lat": 28.42134,
    "long": 77.31762,
    "place": "Palomino",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a3c55640-b666-4f2e-9363-5da1ab92d103",
    "lat": 28.57909,
    "long": 77.38177,
    "place": "Herb Farm",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a3db55bd-dc3a-4696-bc05-9a3b44c77903",
    "lat": 28.48927,
    "long": 77.09308,
    "place": "Wasabi Bistro",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a408701a-6fcb-4511-af93-17fa000852ec",
    "lat": 12.87408,
    "long": 77.6196,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "a42cb84f-32f0-486e-b902-c18bc5f3eced",
    "lat": 28.39372,
    "long": 76.96863,
    "place": "Maggiano's Little Italy",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a42d9f08-f166-4797-9007-a0693a864f2c",
    "lat": 28.63319,
    "long": 77.30504,
    "place": "Italianissimo Ristorante",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a48ce288-d5f1-438b-829e-47e99d4f7390",
    "lat": 28.46903,
    "long": 77.00662,
    "place": "Redhook Ale Brewery",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a496dbd0-51d8-4757-ae94-36542ece9cba",
    "lat": 17.47695,
    "long": 78.54206,
    "place": "Barking Frog",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a4be7e33-9454-434c-918c-54fa908ce9d7",
    "lat": 12.95656,
    "long": 77.55435,
    "place": "Coho Cafe",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "a4f528c9-9c95-4d7f-b0e0-46c7086813f5",
    "lat": 12.92624,
    "long": 77.55771,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "a53499b2-863d-4f88-b910-ca7bc9f74e5a",
    "lat": 28.55211,
    "long": 77.29887,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "a54e11a1-46cc-40f2-850c-b3aaca5eb5b0",
    "lat": 28.71108,
    "long": 77.14565,
    "place": "Wild Ginger",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "a54ed844-9ae0-47d4-953a-41ce727e5e4a",
    "lat": 28.58863,
    "long": 77.0735,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "a570e3ef-3795-4f73-b8e3-4842299ed9b4",
    "lat": 28.6867006,
    "long": 77.309384,
    "place": "Sea Garden",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "a59e198c-a0a5-4a55-8f53-c25db88771c4",
    "lat": 28.4817717,
    "long": 77.0948783,
    "place": "Palomino",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "a5b3f0f7-1bd2-48bf-b722-cdb9b3855770",
    "lat": 28.4290029,
    "long": 77.3244431,
    "place": "Redhook Ale Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "a5dcbe18-9e13-459e-b447-6fa62cb450b8",
    "lat": 28.4836484,
    "long": 77.09134719,
    "place": "I Love Sushi",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a5e16932-7726-43ee-9325-aced342fbd65",
    "lat": 28.65331,
    "long": 77.30203,
    "place": "Chutney's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "a613c7d0-ebe3-4544-9c6a-c8f17762743a",
    "lat": 19.07383,
    "long": 72.8234,
    "place": "Typhoon",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "a62c21fe-bbd6-4bc7-a251-21346ac9172e",
    "lat": 28.6322982,
    "long": 77.296457,
    "place": "Rover's",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "a6536429-d5bc-441c-b64e-893b597074f8",
    "lat": 28.43028,
    "long": 77.10127,
    "place": "Uptown China Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "a66b1874-25e4-4102-9a1d-f71f312dbafe",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "Trader Vic's",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a66c92b1-4b81-4796-9728-9787735796b6",
    "lat": 28.61323,
    "long": 77.28733,
    "place": "I Love Sushi",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "a679569e-c323-4066-b78a-ecc6fda656e1",
    "lat": 22.59194,
    "long": 88.34144,
    "place": "Space Needle",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a695511b-83a8-479b-910d-97c95fc71d4b",
    "lat": 28.64469,
    "long": 77.16516,
    "place": "I Love Sushi",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "a6cb97e0-018e-4437-849d-213db0716ae9",
    "lat": 22.54176,
    "long": 88.32387,
    "place": "Chutney's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "a6cdde80-e37b-4009-95e6-9e6a08695ba4",
    "lat": 22.99771,
    "long": 72.54811,
    "place": "Cutters Bayhouse",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "a6f431dc-b76e-4096-afef-3dd291901270",
    "lat": 17.47993,
    "long": 78.5379,
    "place": "Coho Cafe",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a719270d-f991-46b0-bf9b-76ceb3ec5d1e",
    "lat": 28.4440781,
    "long": 77.0511576,
    "place": "The Calcutta Grill",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a76280a6-b3cc-479d-935a-e072ae314740",
    "lat": 19.19084,
    "long": 72.82029,
    "place": "The Tasting Room",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "a7a7e8ac-3183-44f9-a281-8a38be50202a",
    "lat": 28.49227,
    "long": 77.09601,
    "place": "Chateau Ste Michelle Winery",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "a7c37d14-d9d6-4051-9842-bc5c7c854e4d",
    "lat": 28.63545286,
    "long": 77.3646536,
    "place": "Cutters Bayhouse",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a7e75341-8085-4acd-b18f-ba40778c331a",
    "lat": 28.6263913,
    "long": 77.0579048,
    "place": "Redhook Ale Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a7f877ca-82b1-45b5-8d53-e8b8c3064e8e",
    "lat": 28.6166,
    "long": 77.2831,
    "place": "Sea Garden",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "a80da763-0919-43a5-8126-5edcf08e0fa6",
    "lat": 28.63309,
    "long": 77.31217,
    "place": "Herb Farm",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "a8241466-8700-422e-9265-784365920497",
    "lat": 12.9566,
    "long": 77.55447,
    "place": "Il Fornaio",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "a82ef08b-4cd2-49e1-8d4f-1cbce169f624",
    "lat": 28.43644,
    "long": 77.11551,
    "place": "Kaspar's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a8427853-f151-41ce-8083-7c63af437a05",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "Pike Brewing Company",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "a869dd64-ddc3-4b22-b208-c8c4627739e5",
    "lat": 28.62408,
    "long": 77.41956,
    "place": "Pike Brewing Company",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a86ae6f3-4397-49b6-9a0f-f1ffa678de75",
    "lat": 28.50173,
    "long": 77.37711,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a8def407-3530-4c3a-8419-3a001ba4a4b1",
    "lat": 12.95794,
    "long": 77.69448,
    "place": "Coho Cafe",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a8fbc3e1-8285-43db-9659-541c15178e29",
    "lat": 12.96535,
    "long": 77.6495,
    "place": "The Calcutta Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a91eea15-d56a-4ef1-98dc-244877acd23f",
    "lat": 28.4499389,
    "long": 77.0167965,
    "place": "Il Fornaio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a947c3cc-9d1e-4b37-b152-bdd699da74c6",
    "lat": 28.54449,
    "long": 77.33667,
    "place": "Rover's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a95c6219-e994-4745-b537-0a8d77d252db",
    "lat": 22.5527778,
    "long": 88.3554917,
    "place": "Bis On Main",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "a9856586-bdc1-4d04-866f-44ff9af67ab8",
    "lat": 28.6126615,
    "long": 77.4240542,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "a9d0e48e-f51f-4fd0-8019-b5049704c6f5",
    "lat": 28.4045,
    "long": 77.36368,
    "place": "Kaspar's",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "aa4a94c7-5284-40a9-8f15-d9f212007b9a",
    "lat": 23.055,
    "long": 72.63162,
    "place": "The Tasting Room",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "aa5dc39c-2c69-4143-b19e-7e83aa7db693",
    "lat": 19.0141,
    "long": 72.84306,
    "place": "Kaspar's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "aa5ec600-9cac-4199-999d-f077224660c1",
    "lat": 28.4465972,
    "long": 77.0278044,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "aa66d8ea-f13a-4bf2-9bd1-31690b272536",
    "lat": 28.6473726,
    "long": 77.1987286,
    "place": "Wild Ginger",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "aa9e1790-9d36-4fea-839f-5b1aef8a72ef",
    "lat": 28.68178,
    "long": 77.31795,
    "place": "Herb Farm",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "aacd0682-871f-4b5b-80c1-6dff62b63e99",
    "lat": 28.63229,
    "long": 77.28456,
    "place": "The Tasting Room",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "aadf55be-bf35-4511-a1b7-da6444c0077c",
    "lat": 28.51616,
    "long": 77.20494,
    "place": "Soos Creek Wine Cellars",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ab405d1f-5b0f-4797-8983-afce6e9af5c7",
    "lat": 12.9801724,
    "long": 77.6777569,
    "place": "Assagio",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "ab4c0d18-e8cf-46c8-8768-478af4ccf6f1",
    "lat": 28.58197,
    "long": 77.3731,
    "place": "Il Fornaio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ab5acab8-7f13-478a-9623-3d718fe4a727",
    "lat": 12.9214817,
    "long": 77.5197406,
    "place": "Café Juanita",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ab5b0e32-f230-4f12-93f2-6efbbca973ad",
    "lat": 23.0307288,
    "long": 72.5886029,
    "place": "Redhook Ale Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ab816180-dd8a-4f00-8ddd-25c84c4a2ea6",
    "lat": 26.91631,
    "long": 75.70235,
    "place": "Salish Lodge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "abb1e595-54c9-4cd2-bca5-67b7faa1f119",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "The Calcutta Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "abb4f982-5cd9-4810-b8b6-2ba570d6d4e1",
    "lat": 19.0122206,
    "long": 72.8420642,
    "place": "Typhoon",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "abc150c3-03c4-427e-9d20-8521aa1222fd",
    "lat": 18.95526,
    "long": 72.83608,
    "place": "Italianissimo Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "abd2dd85-42d3-4dae-97d6-8303462ef23e",
    "lat": 28.6871071,
    "long": 77.3666599,
    "place": "Pike Brewing Company",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "abf9faf3-39fa-4bf6-8a9e-a97a081aec2e",
    "lat": 22.5791915,
    "long": 88.4804969,
    "place": "Café Juanita",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ac190dc6-0798-4095-baa0-be3980fe6e87",
    "lat": 28.47523,
    "long": 77.0598,
    "place": "Sans Souci",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ac234b66-9cc9-4a3c-9866-296e8e7aa671",
    "lat": 28.6023246,
    "long": 77.0745418,
    "place": "Maggiano's Little Italy",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "ac2850b4-fe6a-4963-8530-cbbb4f649114",
    "lat": 28.47211,
    "long": 77.06078,
    "place": "Isabella Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ac31c03b-cc70-4fed-9e48-3e7d10cebe5d",
    "lat": 28.41947,
    "long": 77.32594,
    "place": "Typhoon",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ac3ba1be-72f9-4595-a846-98f72799ccca",
    "lat": 18.45977,
    "long": 73.7852,
    "place": "Palomino",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ac3dd272-46db-4c4d-a6fe-583113a23671",
    "lat": 28.47873,
    "long": 77.51491,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "ac9f663e-8e63-4297-be3e-03d417f57123",
    "lat": 18.95516,
    "long": 72.83599,
    "place": "Buca di Beppo",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "acbf50ce-d81a-460f-82f8-9d0e588c6a44",
    "lat": 28.64502,
    "long": 77.33865,
    "place": "Barking Frog",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "acc7a444-adbe-4f83-bc4d-69dac50885a1",
    "lat": 22.54285,
    "long": 88.32435,
    "place": "Rover's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "ad3ec111-a41c-4ed8-8cea-3ef44cce7ba0",
    "lat": 18.5678333,
    "long": 73.9186317,
    "place": "Uptown China Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "ad4e6aaf-7d55-4107-b904-c9165572a134",
    "lat": 28.6267384,
    "long": 77.2999215,
    "place": "Isabella Ristorante",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ad5c2303-f5e5-4f3d-8e0f-6668fbd5e7e4",
    "lat": 28.6372195,
    "long": 77.2034947,
    "place": "Market Cellar Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ad6c6471-f690-4905-b6e8-60a687f427f4",
    "lat": 19.07405,
    "long": 72.824,
    "place": "Cutters Bayhouse",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "ad9c588d-b5be-4e32-8027-82427542ecc3",
    "lat": 28.51957,
    "long": 77.16338,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "adbdf2eb-8a17-415c-9455-afcaca7f3398",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "addced4e-462d-4963-9bd6-f1892f8661d8",
    "lat": 28.46564,
    "long": 76.99638,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "ae33f7fe-70f6-4aea-a6cf-432ab763ef5a",
    "lat": 28.59784,
    "long": 77.29683,
    "place": "Sea Garden",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ae55c12e-6c6a-4a48-960f-58d71051e422",
    "lat": 28.49162,
    "long": 77.17807,
    "place": "I Love Sushi",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ae7d9100-22fe-469a-8341-aa6c2f9fbf3e",
    "lat": 28.6909763,
    "long": 77.2950709,
    "place": "Market Cellar Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "ae8ebb1c-3ffd-4023-9544-705efb2debed",
    "lat": 28.63277,
    "long": 77.31228,
    "place": "Bis On Main",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "aea9f93c-735b-412f-a982-8ee484002e9d",
    "lat": 28.6364907,
    "long": 77.336906,
    "place": "Kaspar's",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "aeb9b767-83ca-4394-8717-ad1279ad595a",
    "lat": 28.42393,
    "long": 77.10293,
    "place": "Chutney's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "aebad712-b6bd-4d84-b7c1-4c9767617c50",
    "lat": 12.9955,
    "long": 77.67937,
    "place": "Chutney's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "aebcdc4e-c8f5-4f8e-a05b-dd21d5ced16f",
    "lat": 28.6382396,
    "long": 77.3787483,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "aec101bf-fe6f-41a1-970e-21a87460a173",
    "lat": 28.71235,
    "long": 77.14652,
    "place": "Kaspar's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "aec7824a-a10e-4334-9157-9b608f2a2c1c",
    "lat": 28.46611,
    "long": 77.08162,
    "place": "Bis On Main",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "aecc6f1a-94c6-407d-82f2-d70d83c314d4",
    "lat": 12.95383,
    "long": 77.52693,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "aed36973-3569-4402-99aa-7a555270fec1",
    "lat": 28.68453,
    "long": 77.36198,
    "place": "Herb Farm",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "aed4d9a6-4450-441b-98a3-05311b5fff22",
    "lat": 28.7245,
    "long": 77.08583,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "aed6be06-0699-42b4-9d67-b9a54e133806",
    "lat": 12.98039,
    "long": 77.67786,
    "place": "Columbia Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "af0e35d4-10d4-472d-b0db-59731bfe6004",
    "lat": 28.65327,
    "long": 77.30206,
    "place": "I Love Sushi",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "af47537b-63f1-4f9d-afad-77e8c0e4db8d",
    "lat": 28.47674,
    "long": 77.04774,
    "place": "Maggiano's Little Italy",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "af5e4441-eec1-4af9-a3c1-c9ee26177247",
    "lat": 19.13713,
    "long": 72.86734,
    "place": "Trader Vic's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "af6e5b64-f2e3-409a-ae2f-926b7be522a2",
    "lat": 18.95612,
    "long": 72.82423,
    "place": "Uptown China Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "af967281-6fdc-49a3-86cf-f175af0cac9a",
    "lat": 22.46098,
    "long": 88.31025,
    "place": "Wild Ginger",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "af9a9e72-1185-4edd-b39f-8878b04f9af4",
    "lat": 28.51798,
    "long": 77.20319,
    "place": "The Calcutta Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b014f989-dd8c-4bc7-942c-de2d265ae0ce",
    "lat": 28.68858,
    "long": 77.29798,
    "place": "Market Cellar Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "b0160d73-2eba-43bb-b319-06459f72f04f",
    "lat": 18.9548517,
    "long": 72.8314037,
    "place": "Redhook Ale Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b028fb8a-1711-4dd9-b534-e4f454a75380",
    "lat": 22.60368,
    "long": 88.41134,
    "place": "Maggiano's Little Italy",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "b0800f20-724f-4f7c-823e-7ec1ff0c25b0",
    "lat": 13.1190665,
    "long": 77.5799718,
    "place": "Sea Garden",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b0ac69b0-75ea-4788-925f-2595a6ab2a3e",
    "lat": 28.6786573,
    "long": 77.1903919,
    "place": "Sans Souci",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "b0b6235f-83bb-4606-a36a-6543cd97abba",
    "lat": 28.43615,
    "long": 77.11511,
    "place": "Rover's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b0bb4f7a-010b-467a-a72e-78571e848962",
    "lat": 12.92243,
    "long": 77.66792,
    "place": "The Tasting Room",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b0c1da01-ffea-49ae-88b6-cb20ab6bad23",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "Yarrow Bay Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b0d2a620-e36c-40e9-ada2-3945b798b1ee",
    "lat": 13.0244821,
    "long": 77.6972803,
    "place": "Uptown China Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "b0dd45f8-20ed-4c6d-9926-339e22f4b458",
    "lat": 28.64309,
    "long": 77.33549,
    "place": "Buca di Beppo",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "b11c55c2-c9bb-44f5-9f58-b3ee3f160c73",
    "lat": 28.58159,
    "long": 77.32708,
    "place": "Tap House Grill",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b1207ad8-47ad-4171-820a-4c044f7e1d69",
    "lat": 26.961765,
    "long": 75.7236933,
    "place": "I Love Sushi",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "b18aa3b1-d461-4165-8572-eff87a9167c1",
    "lat": 28.51514,
    "long": 77.20544,
    "place": "Columbia Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b19007fa-6ad4-4768-b5d2-a2a5b1c624a5",
    "lat": 12.9154569,
    "long": 77.6179339,
    "place": "Maggiano's Little Italy",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b1a8196c-5544-4b9e-a097-6676eeb39543",
    "lat": 28.467659,
    "long": 77.550239,
    "place": "Buca di Beppo",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b1acfbdf-a956-4628-b77b-73913b530687",
    "lat": 22.47568,
    "long": 88.36891,
    "place": "Desert Fire",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b1ca172f-a8d5-4eb9-8107-b68ba9986a85",
    "lat": 12.9115315,
    "long": 77.6823752,
    "place": "Buca di Beppo",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b1f8d59d-0984-4039-a8e3-75220581377b",
    "lat": 22.4869512,
    "long": 88.3110157,
    "place": "Redhook Ale Brewery",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "b1f9a928-9339-4338-87f9-f2ab382c271f",
    "lat": 28.6080773,
    "long": 77.3330067,
    "place": "Wild Ginger",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "b241f248-3967-4b18-9b39-a96e6d2e57a9",
    "lat": 17.50638,
    "long": 78.33019,
    "place": "Coho Cafe",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b25a0320-4719-4ef0-a62f-7abed6bc5e30",
    "lat": 28.40454,
    "long": 77.3637,
    "place": "Salute of Bellevue",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "b26c3af2-5955-497f-8547-5eaff8f1f010",
    "lat": 28.46071622,
    "long": 77.07922788,
    "place": "Buca di Beppo",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b27c0437-2736-463b-9564-3f4246a41153",
    "lat": 28.45131235,
    "long": 76.9887128,
    "place": "Palomino",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "b28dfd58-4d95-43cf-a4c9-9aa81dd7d7a2",
    "lat": 28.36171,
    "long": 77.28377,
    "place": "Royal India",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b2ac3c93-0001-4c29-bdca-7495da54cc96",
    "lat": 26.8794116,
    "long": 81.0065383,
    "place": "Columbia Winery",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b2e0e5f3-b5eb-4ac9-af29-01820508bbf8",
    "lat": 28.6862494,
    "long": 77.2838548,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b3029fbf-680c-412d-b814-5905f62d7807",
    "lat": 28.4075365,
    "long": 77.3260768,
    "place": "Chutney's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "b338390f-b7d8-4612-bdd1-6fd3ddf046b5",
    "lat": 28.3991452,
    "long": 77.2952852,
    "place": "Italianissimo Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b354bf7d-6f00-4456-ad26-4132f6bfa445",
    "lat": 22.60368,
    "long": 88.41134,
    "place": "Maggiano's Little Italy",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b36fa647-3a1d-422f-96f4-35d38b71f4bd",
    "lat": 26.88558,
    "long": 81.00721,
    "place": "Barking Frog",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b3a59f45-8f8e-470b-82e6-0e78ab8c4999",
    "lat": 28.6862494,
    "long": 77.2838548,
    "place": "Wasabi Bistro",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "b3ace3a4-d0cc-436f-93a5-ce580556cf1c",
    "lat": 12.96842,
    "long": 77.73992,
    "place": "Uptown China Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b3c445e2-95bc-47a2-86db-327da23cb9f9",
    "lat": 28.65325,
    "long": 77.30081,
    "place": "Rover's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b3c65d32-8ac1-4b09-a9a1-2eee9d84fce3",
    "lat": 18.91609,
    "long": 72.82688,
    "place": "Yarrow Bay Grill",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "b3e6f243-28a5-4de6-809c-95114cda93a9",
    "lat": 28.49729,
    "long": 77.17955,
    "place": "Columbia Winery",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b42170a8-5225-432a-a262-cc931d5ba35d",
    "lat": 19.02001,
    "long": 73.01754,
    "place": "Manzana Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b42abf3e-7d0e-4376-97c4-89d6c32a1d5f",
    "lat": 28.4707,
    "long": 77.29647,
    "place": "Salute of Bellevue",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b42b161f-f9cb-40ea-bbc6-a4a2ae882804",
    "lat": 28.64277,
    "long": 77.35314,
    "place": "Mac and Jack’s Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "b42e6cf6-f483-446b-b49c-0b4fed276a5f",
    "lat": 28.6385643,
    "long": 77.1836212,
    "place": "Palomino",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "b43b299a-9d50-4748-8094-296d497aa880",
    "lat": 28.70801,
    "long": 77.17134,
    "place": "I Love Sushi",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b4509828-d252-49f1-b949-9ab27ad746a9",
    "lat": 28.6862494,
    "long": 77.2838548,
    "place": "Manzana Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b467c920-638f-423b-a672-2b5423b7ebba",
    "lat": 19.01998,
    "long": 73.01761,
    "place": "Salish Lodge",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b4a7aba9-4e6f-448a-8e47-14483e167f1b",
    "lat": 28.46577,
    "long": 77.01888,
    "place": "Herb Farm",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b4c0453c-47e5-41d9-8c32-bc3e57ae8c14",
    "lat": 28.5816,
    "long": 77.32708,
    "place": "Palomino",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "b4ea1557-4698-499c-b8e7-abbc469e47f9",
    "lat": 28.62279,
    "long": 77.43562,
    "place": "Royal India",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "b50b9a1b-7f83-4823-ba4b-147e1f49a37a",
    "lat": 28.6385643,
    "long": 77.1836212,
    "place": "Salute of Bellevue",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b54c7739-ac34-4426-b565-51ad5ce6c492",
    "lat": 28.6489,
    "long": 77.28194,
    "place": "Redhook Ale Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b55243b5-028a-49bd-811f-f5e9a2373353",
    "lat": 28.6163,
    "long": 77.04616,
    "place": "Manzana Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b574e016-ece1-4f7a-829f-8d777cf38991",
    "lat": 28.50173,
    "long": 77.37711,
    "place": "Uptown China Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "b575e4bf-e0b4-49c8-bd3a-577c3a4f0197",
    "lat": 23.01824,
    "long": 72.56138,
    "place": "Wasabi Bistro",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "b58f739d-e9a2-4688-9bcb-df9ecc6f4bf9",
    "lat": 28.62784,
    "long": 77.30245,
    "place": "The Calcutta Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b59ba605-d2bb-4738-ba1d-be7b89c2ae05",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "Redhook Ale Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "b5a2423c-8d06-44bf-bb95-2fef0fc01680",
    "lat": 22.6208598,
    "long": 88.3487516,
    "place": "Italianissimo Ristorante",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b5bbc304-6d04-4d05-a3f2-f8151bc9a59c",
    "lat": 19.14225,
    "long": 72.84221,
    "place": "Salish Lodge",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "b5e61f5d-9648-404d-8072-043f975f8cc1",
    "lat": 18.59922,
    "long": 73.76636,
    "place": "Yarrow Bay Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "b5f79685-ea8f-4969-a37a-aeb8ff94d78c",
    "lat": 28.4817717,
    "long": 77.0948783,
    "place": "Palomino",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b5f921a8-fa27-4a62-b6dd-72cb772932b1",
    "lat": 28.59715399,
    "long": 77.29537021,
    "place": "Assagio",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "b607bd47-e9ca-43a5-8540-20ee369fde43",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "Dahlia Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b6198ce4-444c-43b8-a051-c20784d4fb04",
    "lat": 28.66995,
    "long": 77.4478,
    "place": "Market Cellar Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "b642f31b-cc5a-4b76-bfe7-2aa0ee7584f5",
    "lat": 28.72881,
    "long": 77.08959,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b64a00ae-c9ed-4e5c-bfdd-6fb6010bb2bd",
    "lat": 12.95794,
    "long": 77.69448,
    "place": "Dahlia Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "b66dc12c-e55a-4f10-94e6-1ea97f4d88b4",
    "lat": 28.6389693,
    "long": 77.0742992,
    "place": "Buca di Beppo",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b67abf76-af1f-40ce-a840-b74ab48668b3",
    "lat": 28.54198,
    "long": 77.33495,
    "place": "Italianissimo Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "b6861b9a-6fd2-4c0a-aa8e-0fda92351590",
    "lat": 12.95794,
    "long": 77.69448,
    "place": "Il Fornaio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b69e8d12-bbcf-4442-9759-ff61051c724b",
    "lat": 28.67156,
    "long": 77.08481,
    "place": "Cutters Bayhouse",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "b6af7078-8093-4a4f-804d-2b02d4e27440",
    "lat": 28.7444186,
    "long": 77.0610211,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "b6cb40ff-e114-4627-8ce9-aeed84ff9139",
    "lat": 28.4266895,
    "long": 77.0950288,
    "place": "The Tasting Room",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b6e6a221-095c-4ea7-a0fe-9b0014cdc269",
    "lat": 28.6263913,
    "long": 77.0579048,
    "place": "Royal India",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "b6e722cc-0c1f-4d87-af5f-9eb0bff0051a",
    "lat": 26.89642,
    "long": 75.75565,
    "place": "Redhook Ale Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "b70a1111-0baf-4c47-8790-39939d359d7b",
    "lat": 28.63644,
    "long": 77.08827,
    "place": "Café Juanita",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "b72637c1-5624-40a8-b994-c07db383e1f7",
    "lat": 28.6389693,
    "long": 77.0742992,
    "place": "Café Juanita",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "b75f11aa-c036-4472-a75d-be2623e704df",
    "lat": 28.47571,
    "long": 77.51769,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b7622623-1b12-4e2f-b855-eb289f4f1f1c",
    "lat": 19.07383,
    "long": 72.82349,
    "place": "Redhook Ale Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b771eb5b-1d5b-4d8c-a0a1-2cc7931d4958",
    "lat": 28.47874,
    "long": 77.51489,
    "place": "Café Juanita",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b79e83c1-accb-4014-81a9-22ef01b61d72",
    "lat": 28.58644,
    "long": 77.36628,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "b7a56dc6-e11d-406a-acd8-99fe332ae455",
    "lat": 22.60204,
    "long": 88.37596,
    "place": "Palomino",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "b7daf02e-4220-4b6c-9355-54ff985abf03",
    "lat": 19.18633,
    "long": 72.83425,
    "place": "Salute of Bellevue",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "b811f902-9d95-4ca7-b729-95e35d29e77e",
    "lat": 17.5062,
    "long": 78.33048,
    "place": "Sans Souci",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b82ac84e-be2c-4ec8-b853-102aa4422925",
    "lat": 12.99555,
    "long": 77.67957,
    "place": "Rover's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b842765f-054f-478b-9df7-14bf5ebbc311",
    "lat": 12.84219,
    "long": 77.646,
    "place": "Columbia Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b8818bcf-e758-4616-94bd-b7778d6f09c8",
    "lat": 28.63649,
    "long": 77.33691,
    "place": "I Love Sushi",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b898a996-2232-4d52-a852-9c68efed16c6",
    "lat": 28.47168,
    "long": 77.05773,
    "place": "Salish Lodge",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "b89a9385-ee00-44c1-a654-0859413dcd87",
    "lat": 28.36173,
    "long": 77.2969,
    "place": "The Calcutta Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "b8aecb36-13d3-40fe-a196-0ea651e0a756",
    "lat": 28.65781,
    "long": 77.36209,
    "place": "Desert Fire",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "b8b56121-ffcc-4ca9-9bf3-613cc33c2a8d",
    "lat": 19.20508,
    "long": 72.95952,
    "place": "Il Fornaio",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b8efb5f1-a399-4c81-894e-73d0b10205dd",
    "lat": 28.72336,
    "long": 77.08426,
    "place": "Typhoon",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "b8faf568-4e01-4fd4-bfaf-6cadda58d16c",
    "lat": 28.69279,
    "long": 77.19977,
    "place": "Soos Creek Wine Cellars",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "b9011d06-4e7d-4807-a111-b321484bc562",
    "lat": 28.45482,
    "long": 77.04125,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b9019e45-65d1-4dfa-94b3-7bb009168c55",
    "lat": 28.41947,
    "long": 77.32594,
    "place": "Buca di Beppo",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "b9052fa4-e1c4-4d89-a268-0b7e481b4c97",
    "lat": 28.71157,
    "long": 77.14986,
    "place": "Barking Frog",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "b94f2d14-3296-445f-83c2-4197432da9a1",
    "lat": 18.95529,
    "long": 72.83603,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "b95917d7-ed13-4626-8ebd-653737c8ca96",
    "lat": 28.50308,
    "long": 77.02709,
    "place": "Typhoon",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b962d7f7-be45-4414-aef5-af27ca5cb237",
    "lat": 28.42394,
    "long": 77.10297,
    "place": "Italianissimo Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "b97e3088-c61f-4180-a3c4-a20a9e96fc49",
    "lat": 28.5732543,
    "long": 77.3803376,
    "place": "The Calcutta Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "b9847cd0-7f96-4410-8db5-3b51b2bbc3d1",
    "lat": 28.50173,
    "long": 77.37711,
    "place": "Typhoon",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "b99886ea-b2b1-4fb0-95e3-517b2d36ac75",
    "lat": 12.9154467,
    "long": 77.6179358,
    "place": "Sans Souci",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b9a3e328-da82-42c1-a14e-4358fd3d16ff",
    "lat": 26.84078,
    "long": 75.74209,
    "place": "Maggiano's Little Italy",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "b9bb9c02-bc86-43e8-b53f-f60c36629c0d",
    "lat": 19.1204017,
    "long": 72.8694867,
    "place": "Barking Frog",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "b9e721a8-4e3b-455f-8ae7-132f05296e00",
    "lat": 13.1190665,
    "long": 77.5799718,
    "place": "Assagio",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "ba0d7610-69b1-479e-9633-6afe6024c791",
    "lat": 26.80379,
    "long": 80.90872,
    "place": "Café Juanita",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "ba38d925-917c-41a0-9974-88115592e0c2",
    "lat": 18.4597696,
    "long": 73.7851956,
    "place": "Kaspar's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ba4fb68a-520c-4258-9b9a-816aa64d09e5",
    "lat": 23.06018,
    "long": 72.65271,
    "place": "I Love Sushi",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ba6b977d-342d-484d-aa7c-23fbf2b7bd1a",
    "lat": 28.40205,
    "long": 77.08866,
    "place": "Wasabi Bistro",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ba7b3819-a2a0-4380-815c-6c7bd604df74",
    "lat": 18.5329,
    "long": 73.94694,
    "place": "Cutters Bayhouse",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "bab74b21-4ff8-45ce-b41a-7ed2f0b13b9d",
    "lat": 28.58194,
    "long": 77.37316,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "bb10ce14-e90b-47ec-925e-555082dec8a9",
    "lat": 28.651913,
    "long": 77.3652817,
    "place": "Wild Ginger",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "bb59c512-d3d6-42b2-b08c-99b0dcb35c40",
    "lat": 18.5678333,
    "long": 73.9186317,
    "place": "Desert Fire",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "bb5e3e78-1f43-4a59-88d7-cb64770b9511",
    "lat": 28.5546005,
    "long": 77.2895799,
    "place": "Herb Farm",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "bb769ef2-4c35-4990-9e84-dc00f42e7b06",
    "lat": 22.5366,
    "long": 88.34966,
    "place": "Salute of Bellevue",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "bba724ca-c073-4ac7-9272-72b593563f18",
    "lat": 28.41256,
    "long": 77.35484,
    "place": "Trader Vic's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "bbb55ee8-4a81-4824-93a2-f235e7e0410c",
    "lat": 19.08858,
    "long": 72.84188,
    "place": "Bis On Main",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "bbea6561-159a-4922-a48c-205b839fa472",
    "lat": 22.61009,
    "long": 88.40947,
    "place": "Bis On Main",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "bc20261d-4e22-4f21-a304-66103e344038",
    "lat": 28.70001,
    "long": 77.26805,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "bc7fb87a-0be5-4a53-8920-4aeb5192d585",
    "lat": 28.6928582,
    "long": 77.1987846,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "bc823a9d-bbc8-4b05-bd52-2d83c17ed19e",
    "lat": 22.60204,
    "long": 88.37596,
    "place": "Salute of Bellevue",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "bc84b5ba-320b-4873-9fe6-78887a5a92cb",
    "lat": 28.6786573,
    "long": 77.1903919,
    "place": "Royal India",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "bcab1c0b-0d77-45ac-8883-0ed2fa7be3d4",
    "lat": 28.6533,
    "long": 77.30209,
    "place": "Sea Garden",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "bcadf5c2-3f1c-4397-bdf0-f2c02aa6c0db",
    "lat": 28.63541,
    "long": 77.1832,
    "place": "Cutters Bayhouse",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "bcc16521-ccd4-4e7a-afe3-ea9c1b94c02d",
    "lat": 28.6678,
    "long": 77.14239,
    "place": "Redhook Ale Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "bcc74d6f-3420-4cb8-81e3-ec505377a926",
    "lat": 28.56826,
    "long": 77.34609,
    "place": "Uptown China Restaurant",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "bcd62b1e-f627-4e53-956d-9ae5e829e8df",
    "lat": 28.46867,
    "long": 77.10353,
    "place": "Manzana Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "bceb1bb4-890a-4b0e-b5dc-6a2dda46565d",
    "lat": 28.58526,
    "long": 77.39857,
    "place": "Space Needle",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "bd1ea701-64c3-4715-bdf5-6a186e6e7bd1",
    "lat": 28.63309,
    "long": 77.31219,
    "place": "Coho Cafe",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "bd843839-4644-465b-9681-093fe553c46b",
    "lat": 23.055,
    "long": 72.63162,
    "place": "Mac and Jack’s Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "bd8d1d83-815b-4643-8b0e-671141983a27",
    "lat": 28.58139,
    "long": 77.32705,
    "place": "Tap House Grill",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "bdb0274b-9fc1-4ac9-8259-148fa82fcabc",
    "lat": 28.6267384,
    "long": 77.2999215,
    "place": "Chutney's",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "bdeafdc7-0786-4b75-91c8-43e7ad565dad",
    "lat": 28.62632,
    "long": 77.43819,
    "place": "Columbia Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "bdf07e4e-dda3-4b98-9e5a-172ba4e06038",
    "lat": 28.7165358,
    "long": 77.1260448,
    "place": "Space Needle",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "be0616bf-db65-4a0a-93cf-211bb55a5319",
    "lat": 19.07403,
    "long": 72.82402,
    "place": "Columbia Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "be428fd2-4a67-47b1-b85d-c162078f646b",
    "lat": 12.83353,
    "long": 77.70163,
    "place": "Desert Fire",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "be44cded-d5ce-400e-9e14-04bfb789e9a3",
    "lat": 18.95511,
    "long": 72.83579,
    "place": "Isabella Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "beb2ba06-3f0a-4eb8-9ad7-93b417c5c2c6",
    "lat": 28.62789,
    "long": 77.30249,
    "place": "Yarrow Bay Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "bed7120d-ce6f-4a47-808d-a51027df642c",
    "lat": 28.5913358,
    "long": 77.3097536,
    "place": "Wasabi Bistro",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "bf0901f1-0245-4aee-b181-f0ff407ef4cd",
    "lat": 28.36543,
    "long": 77.33101,
    "place": "Salish Lodge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "bf2a581f-a727-4706-8055-9ecba2b9881a",
    "lat": 28.63228,
    "long": 77.29643,
    "place": "Space Needle",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "bf345c97-462c-4eda-9048-2d43ee495770",
    "lat": 28.51686,
    "long": 77.20441,
    "place": "The Tasting Room",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "bf3e174c-3ea4-475a-a7bd-ab14e4e86426",
    "lat": 28.69243,
    "long": 77.32847,
    "place": "Buca di Beppo",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "bf46b7ac-f4c7-4415-8108-f448a6193f32",
    "lat": 28.65361,
    "long": 77.36326,
    "place": "Dahlia Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "bf75aec1-9713-4b05-972d-64d1b44c2e2f",
    "lat": 28.43662,
    "long": 77.11551,
    "place": "Wild Ginger",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "bfda5c85-2698-4320-835e-46c3232d1421",
    "lat": 28.4581116,
    "long": 77.0562474,
    "place": "Salish Lodge",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "bffdc81d-6a30-49f0-b6ad-4ec462cea326",
    "lat": 28.6028083,
    "long": 77.3789617,
    "place": "Italianissimo Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "c00a631c-66c4-4924-8084-b99f653725ea",
    "lat": 13.00438,
    "long": 77.7546,
    "place": "Kaspar's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c02281bb-e853-4f9c-9908-0d6c74ea9347",
    "lat": 28.71459,
    "long": 77.10818,
    "place": "I Love Sushi",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "c02a009e-0430-4d3b-9976-b9d66f67c440",
    "lat": 28.7165358,
    "long": 77.1260448,
    "place": "Buca di Beppo",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c043fe8d-f396-4c04-a5e8-32afbd0fd131",
    "lat": 28.63277,
    "long": 77.31228,
    "place": "The Calcutta Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "c045127f-2758-4fd1-b950-91fbfe57104f",
    "lat": 28.6632481,
    "long": 77.3325315,
    "place": "I Love Sushi",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c052aa1a-7999-4051-ad6f-ca83ccdefbd7",
    "lat": 12.96069,
    "long": 77.7133,
    "place": "Assagio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c05389e0-8525-45f6-8513-444a0b0fdbaa",
    "lat": 22.4711239,
    "long": 88.3970873,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c0640f72-7470-4437-b777-3fdc6f0d436f",
    "lat": 12.92103,
    "long": 80.15305,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "c071ee21-c4e2-42de-85c1-095a39a0f34f",
    "lat": 28.73609,
    "long": 77.16919,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c0757194-2dec-4c3a-826e-15d66433e61d",
    "lat": 28.36383,
    "long": 76.94377,
    "place": "Il Fornaio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c07da7aa-0578-453d-9172-72df8a3a96d7",
    "lat": 22.45725,
    "long": 88.3089,
    "place": "Kaspar's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c0b63c3b-acf9-4791-90c9-67b4bce06832",
    "lat": 19.19084,
    "long": 72.82029,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c0bdefc4-cadf-4900-8373-d7224fbdc58b",
    "lat": 28.5732543,
    "long": 77.3803376,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c0ebcb1d-6ee8-42d1-817f-6c7b599990d2",
    "lat": 28.5850391,
    "long": 77.356831,
    "place": "Sans Souci",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c11dab1c-6895-48c9-b6d0-03687427d801",
    "lat": 28.64909,
    "long": 77.3734,
    "place": "Typhoon",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c13b1338-8d58-447a-92b3-6791aed879ba",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "c13bc076-aed1-43ba-afe3-d6f27d6ba255",
    "lat": 28.49591,
    "long": 77.08446,
    "place": "Uptown China Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c158b0d0-6fee-468d-ad14-dc06fa6e4fe3",
    "lat": 28.35791,
    "long": 77.32831,
    "place": "Coho Cafe",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "c1721252-4266-49ff-9cb7-5343917503cd",
    "lat": 28.5774424,
    "long": 77.0856252,
    "place": "Assagio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c172dfd9-8716-45ab-a7a9-7b4ae2bbc813",
    "lat": 19.19084,
    "long": 72.82029,
    "place": "Barking Frog",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "c176bd5d-dab9-470e-898b-d0998a577414",
    "lat": 28.5605202,
    "long": 77.0624836,
    "place": "Isabella Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "c195f6d6-76d5-467e-8ca4-8e7314a5d3d3",
    "lat": 19.0085051,
    "long": 72.8230351,
    "place": "Isabella Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "c1e5c33d-f029-4309-9c5a-096c8e92fba1",
    "lat": 28.6343227,
    "long": 77.0838727,
    "place": "Dahlia Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c2145029-2bd9-4387-9d64-559b2c89895e",
    "lat": 28.651913,
    "long": 77.3652817,
    "place": "Coho Cafe",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "c2277206-e07b-42c8-8d42-250b23febb37",
    "lat": 28.5789,
    "long": 77.38166,
    "place": "Barking Frog",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c231009b-8bc2-4523-8e13-bba2b8fbc9f9",
    "lat": 26.9326741,
    "long": 75.7951361,
    "place": "Market Cellar Winery",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "c2f97847-e81c-4d11-ab18-2fb255c18b9f",
    "lat": 28.59784,
    "long": 77.29683,
    "place": "Trader Vic's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "c3139796-aa36-42db-bdd9-5ad8c7693909",
    "lat": 28.615158,
    "long": 77.3460408,
    "place": "Desert Fire",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c3327515-4bfa-4776-b50e-2fa0163183bc",
    "lat": 28.65033,
    "long": 77.27521,
    "place": "Wild Ginger",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "c33ca442-fe78-4f1d-a60d-ff5cac5d9d8c",
    "lat": 28.70528,
    "long": 77.19765,
    "place": "Wild Ginger",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c3573bc5-fd1e-4e0d-8e1f-06c3a1604a8e",
    "lat": 28.5850391,
    "long": 77.356831,
    "place": "Bis On Main",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "c392e124-3003-45c0-b6c5-1619287c3c4d",
    "lat": 19.07566,
    "long": 72.82609,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "c3a99ac2-dbb3-4ba6-b051-6bdd2609ae02",
    "lat": 19.07976,
    "long": 72.85687,
    "place": "Il Fornaio",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "c3c09766-146d-4a89-8e8e-b029e986f45f",
    "lat": 28.6001445,
    "long": 77.045787,
    "place": "Isabella Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "c3c501f8-3b86-4e1e-b55b-d64245aa88ac",
    "lat": 19.14232,
    "long": 72.84226,
    "place": "Sans Souci",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c3cc151b-0f86-458e-a2dd-6dd805bbd274",
    "lat": 23.0416351,
    "long": 72.4565828,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "c422c042-4982-4056-b333-7d3a5d2ac6a1",
    "lat": 12.95794,
    "long": 77.69448,
    "place": "Palomino",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "c4498d01-594f-405e-95d3-beffac6b5679",
    "lat": 22.61009,
    "long": 88.40949,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c45eede6-f605-4c79-bccb-b0fb19f62a52",
    "lat": 28.5745459,
    "long": 77.2283789,
    "place": "Salute of Bellevue",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c46fb667-1c50-4efa-91f1-7112cd9ebe37",
    "lat": 28.47689,
    "long": 77.54738,
    "place": "Sans Souci",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c4947337-e5fa-485d-bd3b-ea30b5aa3f92",
    "lat": 12.96071,
    "long": 77.7133,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "c4adcc8b-769e-4e71-91bf-4d2f777137d7",
    "lat": 22.61009,
    "long": 88.40952,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c4c0dfda-f8f5-429a-9e38-5018672be799",
    "lat": 28.54189,
    "long": 77.33485,
    "place": "Chutney's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c5051cbc-66b7-47ae-935c-b71ba0103516",
    "lat": 28.57336,
    "long": 77.39295,
    "place": "Market Cellar Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c535ca69-9b3f-4100-998d-918715bb9852",
    "lat": 26.874,
    "long": 80.90604,
    "place": "Sans Souci",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "c538ffec-876c-49d2-9da2-ccb645d3bef0",
    "lat": 12.88305,
    "long": 77.56964,
    "place": "Mac and Jack’s Brewery",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "c54a5d25-28c3-48ec-8ea0-b0fe8b1aae54",
    "lat": 18.9548517,
    "long": 72.8314037,
    "place": "Desert Fire",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "c559da85-8204-40b9-9629-ec01cd238ae4",
    "lat": 19.19084,
    "long": 72.82029,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c5b40868-67dd-4230-a7f2-31749dd2f7e5",
    "lat": 28.6409383,
    "long": 77.2085383,
    "place": "Café Juanita",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c5c78e67-a0d7-483a-a661-b9026e3eb27b",
    "lat": 28.57,
    "long": 77.3615,
    "place": "Manzana Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c5eb527e-382b-424f-8acd-4a88a08d570a",
    "lat": 28.491148,
    "long": 77.092897,
    "place": "Wild Ginger",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c5eb81a8-5a3a-4780-832f-25e0043ef936",
    "lat": 12.9214817,
    "long": 77.5197406,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "c60b25e9-5b45-4921-b7b9-69eb2f9ab6b2",
    "lat": 28.39372,
    "long": 76.96863,
    "place": "Soos Creek Wine Cellars",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "c62106f1-6684-4f20-95e0-a03131bb09e3",
    "lat": 28.5707074,
    "long": 77.3844247,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c621d0f6-0369-4a52-ba94-d88ea56c12cc",
    "lat": 26.84218,
    "long": 80.93635,
    "place": "Trader Vic's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "c62a884a-a79d-4b3d-a61e-b33c7b87aab6",
    "lat": 18.45977,
    "long": 73.7852,
    "place": "Barking Frog",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "c6434057-9a28-443f-87d1-5f31c92943c3",
    "lat": 28.6023246,
    "long": 77.0745418,
    "place": "Chutney's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c64f2a48-dc60-4cc1-aa82-a69436f623c0",
    "lat": 28.46773,
    "long": 77.55037,
    "place": "Uptown China Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c6531198-083c-4b20-afa4-e0b309a6b094",
    "lat": 28.44992,
    "long": 77.01682,
    "place": "I Love Sushi",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "c665e413-2b53-432d-8cbd-b7a4724710df",
    "lat": 28.5672833,
    "long": 77.2208767,
    "place": "I Love Sushi",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "c67a3d60-d22a-483a-8884-d66c627851f1",
    "lat": 28.59114,
    "long": 77.30991,
    "place": "Sea Garden",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "c6d4fe0c-4a7d-4bcf-96fe-1fc3fe634c0c",
    "lat": 28.51826,
    "long": 77.20217,
    "place": "Dahlia Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c6d9d12c-1974-47e4-aa07-783ee669aa2e",
    "lat": 22.5136132,
    "long": 88.2885725,
    "place": "Assagio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c6e87992-f342-4d7f-b7b2-abcb33245716",
    "lat": 18.59261,
    "long": 73.79756,
    "place": "Barking Frog",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "c6fc1874-a131-4067-b5ce-96a554982162",
    "lat": 23.055,
    "long": 72.63162,
    "place": "Barking Frog",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c72a4b40-f069-4f28-8e78-474343beda6f",
    "lat": 26.89684,
    "long": 80.95585,
    "place": "Dahlia Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c75f1ed9-f019-43e5-898c-2e74dc9ed03c",
    "lat": 28.5774424,
    "long": 77.0856252,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "c767fbbd-9ce0-44de-8723-60a315aa5ae7",
    "lat": 28.41346,
    "long": 77.35546,
    "place": "Chutney's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c76dab29-dff3-4881-8e7a-105583c04387",
    "lat": 28.5916616,
    "long": 77.3750196,
    "place": "Salute of Bellevue",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "c79201ac-3a84-4e39-b77e-6ab91fb359d4",
    "lat": 28.5938654,
    "long": 77.1167883,
    "place": "Coho Cafe",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "c7a40a61-e540-44a2-8e2c-8d4737d53230",
    "lat": 26.87432,
    "long": 75.79065,
    "place": "Market Cellar Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c7ce1932-96a2-461b-83ce-876e20fd823c",
    "lat": 26.8449,
    "long": 81.00477,
    "place": "Chutney's",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "c7fcd753-8fac-441c-846d-27fd4f7168cb",
    "lat": 22.59103,
    "long": 88.3414,
    "place": "Columbia Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "c81b18fe-54fc-47cc-b39c-7c91698789f4",
    "lat": 19.1204017,
    "long": 72.8694867,
    "place": "Pike Brewing Company",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "c84c1dcb-41e7-4ad9-89a9-3d047132fe59",
    "lat": 28.5877966,
    "long": 77.39980465,
    "place": "Maggiano's Little Italy",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c84ec8b8-6459-4a5a-8d8f-6066c569ecd2",
    "lat": 28.6167486,
    "long": 77.0422968,
    "place": "I Love Sushi",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "c8699eef-1445-4949-91e7-e8150cad5180",
    "lat": 28.57047,
    "long": 77.3375,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c870342c-7f8d-476f-af7a-a48771054398",
    "lat": 19.07847,
    "long": 72.88056,
    "place": "The Tasting Room",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c8864056-a268-42e9-87cb-8564cc6645af",
    "lat": 19.03201,
    "long": 72.90469,
    "place": "I Love Sushi",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "c89a6baf-d805-448d-95c7-c776b9d203b2",
    "lat": 17.5062,
    "long": 78.33048,
    "place": "Bis On Main",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "c89c5722-7ec7-4bcf-a688-e223b02f4666",
    "lat": 28.48294315,
    "long": 77.01932575,
    "place": "Uptown China Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c8adf7f7-3a43-409d-9688-ab38511c872e",
    "lat": 28.4075365,
    "long": 77.3260768,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c8db7f95-3490-4fc8-9def-be7efddbcd5b",
    "lat": 23.055,
    "long": 72.63162,
    "place": "Pike Brewing Company",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "c8e2ff76-6111-457a-9a33-94df02520ff7",
    "lat": 18.5678333,
    "long": 73.9186317,
    "place": "Sea Garden",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "c8e7a782-ab9e-4c0f-91c8-55b893c88746",
    "lat": 28.46639,
    "long": 77.03016,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c8e99214-25d2-4cca-b911-8146d8f35981",
    "lat": 13.10026,
    "long": 80.28012,
    "place": "Market Cellar Winery",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "c9038f99-1f03-4695-bd14-54ccdbf04fa8",
    "lat": 28.55859,
    "long": 77.33439,
    "place": "Rover's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c91e9aba-7e80-47a6-a2ff-cf9826bf3c21",
    "lat": 19.14992,
    "long": 72.83159,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "c946050a-7089-47f8-a2af-8b58c7f68c44",
    "lat": 28.4266895,
    "long": 77.0950288,
    "place": "The Tasting Room",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "c9485872-c01b-449f-b900-7076cb3e7dba",
    "lat": 28.57949,
    "long": 77.15161,
    "place": "Cutters Bayhouse",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c955ee2c-6a76-44eb-9140-6e8c50a87add",
    "lat": 12.95367,
    "long": 77.62276,
    "place": "Market Cellar Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "c9563712-3314-4a43-9002-7a4008a0864c",
    "lat": 12.883328,
    "long": 77.5697467,
    "place": "Manzana Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "c968a255-e591-4ddf-acfc-ed6fe2c68b78",
    "lat": 28.5203607,
    "long": 77.2559372,
    "place": "Columbia Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "c96ed5d8-334f-4bc6-aa0a-e70ca1179906",
    "lat": 28.5681,
    "long": 77.3434,
    "place": "Chutney's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "c9b8d354-1040-4a10-acdb-2eb6a6615c3c",
    "lat": 28.6841032,
    "long": 77.15382,
    "place": "Rover's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ca19bcd5-bea6-4556-ae8f-81e04e2806ba",
    "lat": 22.48948,
    "long": 88.31376,
    "place": "I Love Sushi",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "ca1c8802-7e46-4088-ac28-d29a9eb0eef8",
    "lat": 28.710614,
    "long": 77.1401026,
    "place": "Typhoon",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ca2fe791-edd4-45d8-92f8-2abdc6bfe8ff",
    "lat": 12.95794,
    "long": 77.69448,
    "place": "Salute of Bellevue",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ca354d7a-0520-4215-99a5-4b512cfbac45",
    "lat": 28.62558,
    "long": 77.43753,
    "place": "Rover's",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "ca4ed70e-2333-4382-8b83-493c5a0600c3",
    "lat": 28.55809372,
    "long": 77.37336311,
    "place": "Space Needle",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ca54e7bd-0315-449f-bcdc-5988c5b5d865",
    "lat": 23.01849,
    "long": 72.56154,
    "place": "Uptown China Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "ca66d9a7-f4eb-4f90-b40a-a7dd1afd44e3",
    "lat": 28.45583,
    "long": 77.04008,
    "place": "Columbia Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ca922169-ad95-47b6-8bab-543c2e26e9b7",
    "lat": 28.6385643,
    "long": 77.1836212,
    "place": "Herb Farm",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "cb296ea9-2276-42ac-9886-eb66578aa3a0",
    "lat": 28.4255994,
    "long": 77.0549768,
    "place": "Rover's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "cb64034b-bcf6-47c0-9a76-2cd31cf4e07b",
    "lat": 28.71803,
    "long": 77.17802,
    "place": "Manzana Grill",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "cb77501c-ca9d-4d93-8fc7-651f39098ab6",
    "lat": 28.66979,
    "long": 77.32842,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "cbcc1ea3-aec6-4ceb-8fa2-9c217877c03d",
    "lat": 28.6917,
    "long": 77.20136,
    "place": "Cutters Bayhouse",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "cbe600fa-d756-4b72-b99e-eae274b4971c",
    "lat": 22.60214,
    "long": 88.37627,
    "place": "Space Needle",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "cbf012ea-e6f1-461d-a260-b9d8011e9518",
    "lat": 23.10772918,
    "long": 72.59130133,
    "place": "Coho Cafe",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "cbfc612b-6d1b-4d2f-a0cc-1decf9a02f46",
    "lat": 26.84001,
    "long": 75.81046,
    "place": "I Love Sushi",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "cc0f9891-ad66-42ed-9f74-050b57090c9c",
    "lat": 28.71472,
    "long": 77.21317,
    "place": "Manzana Grill",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "cc1e6d9f-74e6-4aec-b3e3-8d7535fb9307",
    "lat": 12.9214817,
    "long": 77.5197406,
    "place": "Wild Ginger",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "cc23a5d4-461b-423b-a268-277aab894b8f",
    "lat": 12.92117,
    "long": 80.15224,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "cc2a6b55-4fa7-4349-98e9-4837a6d478d3",
    "lat": 28.4896933,
    "long": 77.3066681,
    "place": "Maggiano's Little Italy",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "cc45f393-8f95-4f09-9160-aba7947e5cb4",
    "lat": 18.95527,
    "long": 72.83603,
    "place": "Café Juanita",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "cc4a7da3-01c9-4ef1-9652-5add7f8c644c",
    "lat": 26.8794116,
    "long": 81.0065383,
    "place": "Café Juanita",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "ccb676fe-4830-46d1-bf56-1b873eac4c55",
    "lat": 28.59467,
    "long": 77.34066,
    "place": "Coho Cafe",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ccfe3fe1-1cbf-4ed3-bfe1-13e4e4b8a8eb",
    "lat": 28.6343227,
    "long": 77.0838727,
    "place": "Sans Souci",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "ccffade6-21d0-4330-be91-15f6385a8bd8",
    "lat": 28.6322982,
    "long": 77.296457,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "cd31ef03-1529-4605-9c69-f6806c8c37f0",
    "lat": 28.4517,
    "long": 77.01853,
    "place": "Herb Farm",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "cd4f1b92-d643-442e-886f-bc38d5199445",
    "lat": 28.48997,
    "long": 77.09433,
    "place": "Salute of Bellevue",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "cdda807b-a0a4-4e84-b49b-807d874e9e3d",
    "lat": 28.6632481,
    "long": 77.3325315,
    "place": "Isabella Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "ce0075b5-337d-4940-99f0-24f57a9ca738",
    "lat": 28.62567,
    "long": 77.43761,
    "place": "Kaspar's",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "ce0f182a-2910-4210-b142-a78d9ef85d82",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "Palomino",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ce126d96-b8f0-40ae-8e82-c2cb3c54dedb",
    "lat": 28.6346953,
    "long": 77.0846505,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "ce174c2e-6acd-4724-99cf-06511f0c9fd8",
    "lat": 12.96818,
    "long": 77.50371,
    "place": "Palomino",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ce1a4daa-7ee1-446c-a2b4-d58ecf5963bd",
    "lat": 17.47694,
    "long": 78.54207,
    "place": "The Tasting Room",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ce38e975-cf6f-4781-81a8-6eeb56e4a274",
    "lat": 28.6385643,
    "long": 77.1836212,
    "place": "Assagio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ce58886d-982a-4e6a-a6e4-2d4579ef4c94",
    "lat": 28.4290029,
    "long": 77.3244431,
    "place": "Dahlia Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ce64da80-ef17-483d-9625-637d18e6babb",
    "lat": 22.5366,
    "long": 88.34966,
    "place": "Chutney's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ce69a00b-064e-442f-a52c-8db5d13f8d6b",
    "lat": 19.10489,
    "long": 72.87862,
    "place": "I Love Sushi",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ce72a9b0-7447-4ccc-bc63-0f752cf04d55",
    "lat": 28.51795,
    "long": 77.20289,
    "place": "Mac and Jack’s Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "ce9002e3-3dfe-475d-b7e7-76a9d800512c",
    "lat": 28.63211,
    "long": 77.27759,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ceb76f7c-e01a-40ef-bee2-3635df16ae19",
    "lat": 18.53905,
    "long": 73.88657,
    "place": "The Calcutta Grill",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "ced9bcf7-5fb4-4b8b-9e1b-c2f095a86bae",
    "lat": 12.9818786,
    "long": 77.6155783,
    "place": "Royal India",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ceed04f5-58f8-46a6-94ef-b9bbc2816001",
    "lat": 26.86648,
    "long": 80.94359,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "cf2481db-63bb-4f01-949c-2e37d88caf9e",
    "lat": 28.51444,
    "long": 77.20514,
    "place": "Kaspar's",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "cf2dd87c-7922-40ba-a64e-51cec2a0f3d6",
    "lat": 28.44473,
    "long": 77.06702,
    "place": "Dahlia Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "cf4e85c1-ee75-4bce-885d-f9213e7cf387",
    "lat": 28.6330519,
    "long": 77.3122682,
    "place": "The Tasting Room",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "cf54e2a9-f306-4239-950a-c42f5ceb5379",
    "lat": 19.19084,
    "long": 72.82029,
    "place": "The Calcutta Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "cf6c817a-4927-46ee-8f85-ef72e6bdd539",
    "lat": 19.09803,
    "long": 72.84768,
    "place": "Dahlia Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "cf97a4c0-3f0a-4922-8481-a17a3f490320",
    "lat": 28.66959,
    "long": 77.3305,
    "place": "The Calcutta Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "cf995d13-b6a3-4d40-9576-371f5f5d4cdd",
    "lat": 28.5043708,
    "long": 77.1832198,
    "place": "Salish Lodge",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "cfc6e478-2c1f-46e5-9816-4af214361318",
    "lat": 28.56052,
    "long": 77.06249,
    "place": "Palomino",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "cff40e4f-f8cd-4950-b610-15e5c8926957",
    "lat": 28.6389693,
    "long": 77.0742992,
    "place": "Café Juanita",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d010a32b-2ef0-4306-a373-81c216b83f2c",
    "lat": 28.4440781,
    "long": 77.0511576,
    "place": "The Tasting Room",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "d029314d-6174-4c53-82fb-365d6946166d",
    "lat": 28.6778,
    "long": 77.18129,
    "place": "The Tasting Room",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "d0466f14-db12-46f8-b64b-6e4783def4b2",
    "lat": 28.69266,
    "long": 77.32836,
    "place": "Sans Souci",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d04c1c8e-f8be-4e71-908d-b1eada613b86",
    "lat": 22.49442,
    "long": 88.33794,
    "place": "Yarrow Bay Grill",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "d05ade5b-1feb-43d9-a975-6e4775b4a621",
    "lat": 28.4356892,
    "long": 77.0566262,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "d06021b9-035b-4255-bd59-86bb1ce4de1e",
    "lat": 28.62587,
    "long": 77.43369,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d0b57aa9-c2d4-4ec0-b986-08b33c37cb1f",
    "lat": 28.5108,
    "long": 77.20251,
    "place": "Typhoon",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d0b5c96d-5199-42dc-8745-ede20b2e952f",
    "lat": 28.49108,
    "long": 77.09293,
    "place": "Wild Ginger",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d0c1080e-0bda-4a8d-8c0e-5c200a095599",
    "lat": 26.834547,
    "long": 80.9252696,
    "place": "Typhoon",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d0cf7183-9bcc-4a4c-a0db-c022247e70e5",
    "lat": 28.7165358,
    "long": 77.1260448,
    "place": "Cutters Bayhouse",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "d0e9900f-7ae9-4949-a7ab-b2838dad7f15",
    "lat": 28.72665,
    "long": 77.16975,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d0edc662-4b76-4927-8c38-57ce7210571d",
    "lat": 28.6391824,
    "long": 77.0743123,
    "place": "Maggiano's Little Italy",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d0f69c10-f65d-4081-b9da-070a1379fe59",
    "lat": 28.65325,
    "long": 77.30081,
    "place": "Uptown China Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "d108dd40-fd26-46ef-8157-d65a6dc833c9",
    "lat": 28.49685,
    "long": 77.19081,
    "place": "Columbia Winery",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "d12eb14a-44d5-4c31-868e-cbd5b08cc050",
    "lat": 18.9548517,
    "long": 72.8314037,
    "place": "Desert Fire",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d13ed065-c246-4365-ac59-30783a063b47",
    "lat": 28.48997,
    "long": 77.09434,
    "place": "Assagio",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "d1636ff1-13db-4e78-a401-7586903f568d",
    "lat": 28.58655,
    "long": 77.36636,
    "place": "Bis On Main",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "d16e666c-57c6-47ac-a468-89061a0e173a",
    "lat": 28.53468,
    "long": 77.22938,
    "place": "Chutney's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d1c621a4-48c1-42bc-bec7-75499e098f46",
    "lat": 28.68575,
    "long": 77.34821,
    "place": "Chutney's",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "d1e09a2b-6fc0-47cc-b180-ba04f763a665",
    "lat": 28.56729,
    "long": 77.25262,
    "place": "Trader Vic's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "d208e542-f47a-4fdd-bfe0-d1673bc3f970",
    "lat": 28.63308,
    "long": 77.31218,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d20db23c-0704-478b-ada5-b83e71d505a8",
    "lat": 12.9214817,
    "long": 77.5197406,
    "place": "Coho Cafe",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "d253d747-15e1-4f70-b674-1743dbb0fe11",
    "lat": 23.10796141,
    "long": 72.59141113,
    "place": "Wasabi Bistro",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d25a7712-0a61-477e-b590-63f088205ce3",
    "lat": 28.59571,
    "long": 77.0491,
    "place": "Manzana Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d2680496-b95c-4532-842e-6ee443dca5df",
    "lat": 19.1964566,
    "long": 72.8757323,
    "place": "Palomino",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d2a552f0-3e67-4aa1-ac02-9fd29c5a2cb9",
    "lat": 28.48962,
    "long": 77.30664,
    "place": "Uptown China Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d2f0e9f6-ecda-4243-b0bd-210f3bd46f63",
    "lat": 28.57909,
    "long": 77.38177,
    "place": "Tap House Grill",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "d3118092-9ae7-4a5f-8cd7-9891ea1e53c4",
    "lat": 26.90247,
    "long": 80.95793,
    "place": "Palomino",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d321095e-0e7b-4ebb-8396-00df52784a0b",
    "lat": 19.13914,
    "long": 72.85843,
    "place": "Salute of Bellevue",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d3455de2-3b39-4833-8ad4-8fbfab7eab36",
    "lat": 26.90124,
    "long": 80.94622,
    "place": "Palomino",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d34dba57-f5bc-41fb-90de-2d135603f77e",
    "lat": 28.6394048,
    "long": 77.3791118,
    "place": "Wild Ginger",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d367b913-ad72-47f1-82ce-cf81e33a3253",
    "lat": 19.13674,
    "long": 72.86523,
    "place": "Palomino",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "d3857b48-aeaf-4f67-8bff-0a707a23b069",
    "lat": 19.17082,
    "long": 72.87046,
    "place": "Wasabi Bistro",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d39ce148-96fa-4338-949c-702ae9f16012",
    "lat": 17.47993,
    "long": 78.5379,
    "place": "Dahlia Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "d3a25bf4-ed65-4a64-a2ed-b4353da8fdfc",
    "lat": 26.87432,
    "long": 75.7907,
    "place": "Soos Creek Wine Cellars",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d3a9bc22-d919-435e-8c99-987c65302233",
    "lat": 19.14234,
    "long": 72.84225,
    "place": "Italianissimo Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d3b696b9-c970-48b3-965c-7c31b5b49a20",
    "lat": 28.46071622,
    "long": 77.07922788,
    "place": "Cutters Bayhouse",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "d3c8d86f-3887-44ef-94af-971243b76840",
    "lat": 17.5062,
    "long": 78.33048,
    "place": "Maggiano's Little Italy",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d3ce8873-bafc-4cf3-9dcd-3decae743fce",
    "lat": 28.42324,
    "long": 77.06348,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "d3e55f5c-b09f-4a8e-b3e6-a6e4d39e2efe",
    "lat": 28.57071,
    "long": 77.38442,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d4109595-98f0-4a01-a5bc-28e37873bdec",
    "lat": 12.95794,
    "long": 77.69448,
    "place": "Herb Farm",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "d4296133-4381-4270-b7e4-22e2af83b1ad",
    "lat": 28.54855,
    "long": 77.18156,
    "place": "Manzana Grill",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "d47e99e9-6a24-4579-858f-881cd5fd9708",
    "lat": 28.69225,
    "long": 77.32872,
    "place": "Isabella Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d4cdc07b-7d6c-43ce-bb15-f048290dd81d",
    "lat": 28.58196,
    "long": 77.3731,
    "place": "Coho Cafe",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d4d5b2a0-2bb0-440e-9200-7896ec471705",
    "lat": 13.0353963,
    "long": 77.5632253,
    "place": "Italianissimo Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "d4e7081f-2005-4e6f-9c85-99b4a09a0125",
    "lat": 28.49944,
    "long": 77.23435,
    "place": "Wasabi Bistro",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "d509eef4-3d53-4aca-a14d-5a64c098228e",
    "lat": 28.64309,
    "long": 77.33539,
    "place": "Isabella Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "d5243ba1-9170-4aec-8f5e-d95871b25aef",
    "lat": 26.89763,
    "long": 80.97842,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "d53db320-76cf-4357-96e6-9b0b6639cbff",
    "lat": 28.6167486,
    "long": 77.0422968,
    "place": "Pike Brewing Company",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d5458ed6-f766-4601-a4d8-f58445f47cd2",
    "lat": 22.48948,
    "long": 88.31376,
    "place": "Pike Brewing Company",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d5604ea7-b135-47ea-876b-e34f3c01fcf5",
    "lat": 28.68309,
    "long": 77.36382,
    "place": "Royal India",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d560ab2b-e232-4fdd-9037-a22492a66bae",
    "lat": 28.6023246,
    "long": 77.0745418,
    "place": "Salute of Bellevue",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "d584819e-88bd-4cc0-8590-79027430baca",
    "lat": 28.5452022,
    "long": 77.2753021,
    "place": "Barking Frog",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d596666e-ca30-40af-b983-a533a98432f6",
    "lat": 28.71131,
    "long": 77.08965,
    "place": "Royal India",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "d5d0ad0f-1305-46f7-97ee-6e42528690b5",
    "lat": 12.9154467,
    "long": 77.6179358,
    "place": "Bis On Main",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "d5da6c3c-97de-4202-a77f-4bbcacf93ee3",
    "lat": 12.84219,
    "long": 77.64599,
    "place": "Kaspar's",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "d5f94b74-be12-4984-bf08-4fef70209397",
    "lat": 22.4942,
    "long": 88.38903,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d6023574-67f6-4687-86ae-165b46a61ac6",
    "lat": 28.499314,
    "long": 77.1960993,
    "place": "Kaspar's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d6966405-a40f-41a8-83ad-2148d9014e4f",
    "lat": 28.7288072,
    "long": 77.0895855,
    "place": "Rover's",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d6e2ae33-91f3-40f4-a22c-3f3510b4119e",
    "lat": 18.95678,
    "long": 72.82301,
    "place": "Sea Garden",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d71afc16-9ef1-46c4-922a-79820e9e8bc6",
    "lat": 26.86624,
    "long": 80.9435,
    "place": "Columbia Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d74a0773-5655-436c-9add-f332578e90e9",
    "lat": 26.83434,
    "long": 80.92521,
    "place": "Desert Fire",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d76c835c-29d3-427c-812d-aa097088260d",
    "lat": 18.5678333,
    "long": 73.9186317,
    "place": "Mac and Jack’s Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d78edb67-80a8-4f96-8b7d-5963e5ba6433",
    "lat": 28.5546005,
    "long": 77.2895799,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "d791a36a-0d02-43fa-911a-fd125b184b9f",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "d79d008a-ad62-4507-94f1-1d2043894b97",
    "lat": 18.56783,
    "long": 73.91863,
    "place": "Coho Cafe",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "d7bf5bbf-f886-4911-aa63-656f57221522",
    "lat": 28.60437,
    "long": 77.04788,
    "place": "Café Juanita",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "d7c17fb0-c392-408b-873a-d656bab11424",
    "lat": 19.2626,
    "long": 72.96667,
    "place": "Chutney's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d7cabaa5-506f-4135-bc38-1c672e3745d0",
    "lat": 28.63545286,
    "long": 77.3646536,
    "place": "Maggiano's Little Italy",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "d8027c06-fc55-4ff7-bea0-74159e5890cc",
    "lat": 26.90252,
    "long": 80.95794,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "d82081fe-012e-4d7d-a338-fa850f91147e",
    "lat": 28.49944,
    "long": 77.23435,
    "place": "Manzana Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d8669044-d6da-4e3d-bd7f-a552cff865b5",
    "lat": 17.47695,
    "long": 78.54207,
    "place": "Pike Brewing Company",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "d867a4b2-fc15-44da-a632-22d7e96b9528",
    "lat": 28.49234,
    "long": 77.096,
    "place": "Isabella Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "d8688c80-5c8d-489c-8fbb-8353083e94a8",
    "lat": 28.60576,
    "long": 77.29476,
    "place": "Typhoon",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "d871e89d-42c6-4163-86b3-e502dc092030",
    "lat": 19.19084,
    "long": 72.82029,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d89d94c1-95ab-40ab-b712-6cba340cbd6c",
    "lat": 12.96818,
    "long": 77.50371,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "d8bcf602-61a4-4688-a683-7d5fc57ab121",
    "lat": 28.49573,
    "long": 77.2947,
    "place": "Royal India",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d9087cdf-e436-4b0a-9f4e-24e182279fdb",
    "lat": 28.6161768,
    "long": 77.04618227,
    "place": "Assagio",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d963d266-92f7-417c-a965-3c184cb1044e",
    "lat": 28.48294315,
    "long": 77.01932575,
    "place": "Salute of Bellevue",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "d99eba7f-5f0f-4061-b700-58abb3138111",
    "lat": 28.50853,
    "long": 77.01992,
    "place": "Rover's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "d9b0cd2a-7e47-478f-9841-0be6b0d02883",
    "lat": 23.04164,
    "long": 72.45658,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d9cde683-40c7-4ca4-ad1f-e5d7c2238295",
    "lat": 28.51798,
    "long": 77.20325,
    "place": "Yarrow Bay Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "d9f1937f-0c6b-46e4-b4d5-be5a47b5333b",
    "lat": 12.8993548,
    "long": 77.69580388,
    "place": "Herb Farm",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "d9f2af36-337f-4736-89a5-9fae672f50bd",
    "lat": 28.4703479,
    "long": 77.0226848,
    "place": "Bis On Main",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "da144f1a-b458-40aa-9aa1-f2931214f98a",
    "lat": 28.59773,
    "long": 77.34076,
    "place": "Isabella Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "da5df91c-40be-4266-83a4-b7b388619a1a",
    "lat": 12.88333,
    "long": 77.56975,
    "place": "Mac and Jack’s Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "da5e6111-5ecd-4e20-aeab-f4c49e74851b",
    "lat": 12.96842,
    "long": 77.73992,
    "place": "Sea Garden",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "da601043-57f3-47d8-8219-f62c27587e8d",
    "lat": 28.46552,
    "long": 77.00556,
    "place": "Dahlia Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "da8dbb4f-8130-4211-9c3b-975a8255ea5e",
    "lat": 28.5541909,
    "long": 77.3371235,
    "place": "Yarrow Bay Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "da993011-f980-4bf1-b055-b92dfe1e9b6f",
    "lat": 28.7108712,
    "long": 77.2138803,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "da9b393a-ec67-461f-bcdd-0a98679ca137",
    "lat": 23.01857,
    "long": 72.56164,
    "place": "Wild Ginger",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "daaef7ad-53d6-4b95-a261-6d8f8b2c336e",
    "lat": 28.68309,
    "long": 77.36382,
    "place": "Dahlia Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "dabd71ff-5d26-4fdd-822a-439567d09d23",
    "lat": 18.56783,
    "long": 73.91863,
    "place": "Wasabi Bistro",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "dad8c2fa-a694-479f-872f-ea9d38003424",
    "lat": 26.8826179,
    "long": 80.9983378,
    "place": "Manzana Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "dae13156-3c39-4d95-8e93-0a24470b4cd1",
    "lat": 26.8794116,
    "long": 81.0065383,
    "place": "Kaspar's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "dafe8fcf-0f21-43ce-89fe-ef90b5b7ff1e",
    "lat": 28.5789,
    "long": 77.38166,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "daff1da5-92f9-4d8c-9772-f10fc9d985e4",
    "lat": 28.51671,
    "long": 77.20453,
    "place": "Desert Fire",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "db3c1412-2d58-4edc-89f2-924057bceeb0",
    "lat": 28.68309,
    "long": 77.36382,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "db4029fe-bc02-48b3-9c56-d3f60360dea2",
    "lat": 28.4266895,
    "long": 77.0950288,
    "place": "The Tasting Room",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "db4e9f29-68e9-4105-bff4-0259ad5d1360",
    "lat": 28.58528,
    "long": 77.39862,
    "place": "Salute of Bellevue",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "db5a2397-c738-4076-9ae7-c987fcd33ea8",
    "lat": 28.60625,
    "long": 77.39879,
    "place": "Wild Ginger",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "db8a30fa-4745-4258-a439-6c7baeab9e9d",
    "lat": 17.4010629,
    "long": 78.3816467,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "db93f374-5c7c-4dcf-bb4a-c924fbf10114",
    "lat": 19.07391,
    "long": 72.82409,
    "place": "Chutney's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "dbe49c82-e519-499e-bea0-68d8c4e2f456",
    "lat": 28.36038,
    "long": 77.32606,
    "place": "The Tasting Room",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "dc1beb5e-34a4-42a9-beef-c5c7f4f66745",
    "lat": 19.0775351,
    "long": 72.8602718,
    "place": "Market Cellar Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "dc3e03a9-9d41-4a1f-afd1-b348e58e9987",
    "lat": 19.2185882,
    "long": 72.8705949,
    "place": "Cutters Bayhouse",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "dc45b814-bcc3-4228-ad61-3bd8a34f8c42",
    "lat": 26.88754,
    "long": 80.95903,
    "place": "Trader Vic's",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "dc6372f1-df3f-4bf9-90e0-65bbb5efff0c",
    "lat": 22.54207,
    "long": 88.32399,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "dc67a45f-f5fb-498e-9b43-d53199eb485b",
    "lat": 13.0353963,
    "long": 77.5632253,
    "place": "Rover's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "dc6c3fa3-e843-4fd8-acc1-cedab11c90a6",
    "lat": 28.65329,
    "long": 77.30209,
    "place": "Manzana Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "dc70632d-e044-4e89-b1cb-89a8075448de",
    "lat": 28.6322982,
    "long": 77.296457,
    "place": "Kaspar's",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "dc9d8e9f-12ff-4f97-96e4-ea30d332b7e7",
    "lat": 17.41209,
    "long": 78.55735,
    "place": "Soos Creek Wine Cellars",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "dcc72a7f-d9ff-4379-a42b-cefac7424f94",
    "lat": 28.42966,
    "long": 77.32413,
    "place": "Manzana Grill",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "dcd5e1e9-d4ed-4c90-9c56-6f221f4f4b8e",
    "lat": 18.59255,
    "long": 73.79751,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "dcd7948e-935f-4c58-a942-fe3ea0f59d9d",
    "lat": 28.50871,
    "long": 77.01876,
    "place": "Wasabi Bistro",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "dceaaadd-2106-4344-bc6d-1a4be53cdc0e",
    "lat": 28.39691,
    "long": 76.96948,
    "place": "Rover's",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "dd0536e4-606b-4dde-b3be-9b4dde92b3e5",
    "lat": 22.46282,
    "long": 88.33318,
    "place": "Wasabi Bistro",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "dd0ed5e7-9230-4809-b3aa-e4f985c5827b",
    "lat": 28.42661,
    "long": 77.08513,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "dd30427a-318c-4bbe-90e0-790fff10fc6b",
    "lat": 28.58114,
    "long": 77.33118,
    "place": "Dahlia Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "ddab704c-0029-4a5f-b089-cf2672ff095c",
    "lat": 19.14199,
    "long": 72.8421,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "de0fa1a4-1ea8-4a3a-a536-d4c25fca54ba",
    "lat": 12.9214817,
    "long": 77.5197406,
    "place": "Wasabi Bistro",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "de256629-f576-4c38-b67b-23c409e9cac0",
    "lat": 22.60302,
    "long": 88.37266,
    "place": "Cutters Bayhouse",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "de2957e4-6ee1-413d-81d2-964e198391ff",
    "lat": 28.45487,
    "long": 77.04118,
    "place": "Kaspar's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "de674506-028e-4827-b3f8-7e4d59fd2296",
    "lat": 28.65326,
    "long": 77.30203,
    "place": "Uptown China Restaurant",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "de758e41-aaf7-4d62-a812-7f3254ca2b3d",
    "lat": 28.5574,
    "long": 77.28304,
    "place": "Il Fornaio",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "de791a76-c09d-4540-b794-8c7f2e7a0b7e",
    "lat": 28.4266895,
    "long": 77.0950288,
    "place": "Mac and Jack’s Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "de9c85e7-fbdf-46f1-b62e-ef4d9c745488",
    "lat": 18.98044,
    "long": 72.84131,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "df0bbf79-ce8a-4b79-8052-d83b0534a1a8",
    "lat": 18.98044,
    "long": 72.84131,
    "place": "Salish Lodge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "df306baf-e82c-4bef-8d1c-984299afc68b",
    "lat": 28.557835,
    "long": 77.3766383,
    "place": "Columbia Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "df583254-1200-415c-b6a5-e10baee6c900",
    "lat": 28.40454,
    "long": 77.3637,
    "place": "Sea Garden",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "df70c15a-5863-4546-b9d3-88f665f834d4",
    "lat": 28.55419,
    "long": 77.33723,
    "place": "Sans Souci",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "df793026-11c2-4c6b-b751-e82402363c9b",
    "lat": 28.56213,
    "long": 77.16316,
    "place": "I Love Sushi",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "dfe11c6c-cf5d-4006-9cb6-f3ef5d39f57a",
    "lat": 28.66995,
    "long": 77.4478,
    "place": "Herb Farm",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "dfe4c6b0-e35e-4123-b791-b7a8697f7ff9",
    "lat": 19.14234,
    "long": 72.84225,
    "place": "Coho Cafe",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "e05fa135-ca61-41e4-9ece-db1835051f4a",
    "lat": 28.48868,
    "long": 77.09565,
    "place": "Herb Farm",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "e06d596d-2174-456c-a307-4895f82f30f8",
    "lat": 28.47689,
    "long": 77.5474,
    "place": "Bis On Main",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "e074d034-0cb7-44a0-92af-e612ac899e88",
    "lat": 26.84334,
    "long": 80.88356,
    "place": "Bis On Main",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "e09d2522-24ba-4c9d-be77-6261f24490b2",
    "lat": 28.63622,
    "long": 77.1804,
    "place": "Isabella Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "e0a94b1d-5a92-46c5-b630-1b0abbda136d",
    "lat": 28.64684,
    "long": 77.41508,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "e0d5056d-d0d4-4ee0-8ec9-bc76a053a0a0",
    "lat": 28.6330519,
    "long": 77.3122682,
    "place": "Mac and Jack’s Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "e0fb07ea-1df7-48d6-9e5c-94d74764c9f5",
    "lat": 28.61702,
    "long": 77.03338,
    "place": "Desert Fire",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "e11b986c-b71f-40a0-a132-6d0427e26f7d",
    "lat": 22.4711239,
    "long": 88.3970873,
    "place": "The Calcutta Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "e127b593-1c36-45b0-b994-dbf51d460d4f",
    "lat": 28.5681,
    "long": 77.3434,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "e20de9c4-3ad9-4d60-bdc7-fd4ab09c5e53",
    "lat": 22.61009,
    "long": 88.40955,
    "place": "The Calcutta Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "e21d0b41-ff3a-4bf6-a0ee-0d84aa83fa3f",
    "lat": 19.19084,
    "long": 72.82029,
    "place": "Yarrow Bay Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "e222556e-4bb5-4cdb-a316-d9bbdb1aaeeb",
    "lat": 28.59697,
    "long": 77.07179,
    "place": "Wild Ginger",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "e226f1be-d284-445f-8673-8dbca0def6b1",
    "lat": 28.71466,
    "long": 77.10814,
    "place": "Manzana Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "e233963c-a2fb-45ec-9d70-43401a75f877",
    "lat": 22.61009,
    "long": 88.40949,
    "place": "Barking Frog",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "e290f4c3-2e69-4439-86b9-f71c2b6b12dd",
    "lat": 28.57047,
    "long": 77.3375,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "e29598ef-8b1c-4a4e-ac4a-a0e321591497",
    "lat": 28.43642,
    "long": 77.11549,
    "place": "Café Juanita",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "e2e9fbf2-eb1a-42a2-b0e0-575fc47b5dd4",
    "lat": 28.53886,
    "long": 77.21409,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "e36298c9-b22f-4e9d-abf0-e659e4feb3d8",
    "lat": 19.12204,
    "long": 72.82579,
    "place": "The Calcutta Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "e38b8f4c-c714-4f46-ad9f-5346d388ad9f",
    "lat": 28.36363,
    "long": 76.94377,
    "place": "Dahlia Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "e3d2b664-db82-408f-9310-fd134b8fc68c",
    "lat": 28.67779,
    "long": 77.1813,
    "place": "Salish Lodge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "e3e561e8-4ca9-45fe-9880-95fcf602622d",
    "lat": 23.055,
    "long": 72.63162,
    "place": "The Calcutta Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "e3f11b7f-f282-4cdb-b047-4756185baf0a",
    "lat": 28.57188,
    "long": 77.22495,
    "place": "Mac and Jack’s Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "e3fe8508-78be-4d33-8975-8221a2555b6f",
    "lat": 28.4634,
    "long": 77.56355,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "e4162031-a629-4800-b63d-cfccccad3dd3",
    "lat": 17.50638,
    "long": 78.33019,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "e44766da-2127-43f5-97f5-15efff50ad5b",
    "lat": 28.5135719,
    "long": 77.1699641,
    "place": "Chutney's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "e459ece9-1361-4d51-bfd6-ea46f077a0e7",
    "lat": 28.3937,
    "long": 76.96901,
    "place": "Maggiano's Little Italy",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "e4a8799d-3fa6-4bb8-8a08-e5b8ede5fedd",
    "lat": 26.80445,
    "long": 80.91024,
    "place": "Wild Ginger",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "e4be6f7b-ab62-4c36-928a-a7069c805127",
    "lat": 28.4882887,
    "long": 77.0001589,
    "place": "Typhoon",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "e4cf16bf-6ca7-4ff4-9579-64469be16a59",
    "lat": 28.56051,
    "long": 77.06249,
    "place": "Wasabi Bistro",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "e4dc5711-68b4-48c5-8b66-c90ec9f118f9",
    "lat": 17.5062,
    "long": 78.33048,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "e4e46ac0-de25-47dd-b167-32705faaddd2",
    "lat": 22.5527778,
    "long": 88.3554917,
    "place": "Maggiano's Little Italy",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "e4e63660-da33-43d5-9b13-8d27baec74ae",
    "lat": 22.5738,
    "long": 88.46142,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "e4e79cb7-20ac-4422-8fe1-75df43647094",
    "lat": 28.5886261,
    "long": 77.0734991,
    "place": "Italianissimo Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "e4ebb851-1d9f-44e7-aa15-5bf53dab735f",
    "lat": 22.52531,
    "long": 88.39469,
    "place": "Isabella Ristorante",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "e5210748-fa18-4ee0-bc90-b7cefd7e3170",
    "lat": 26.85598,
    "long": 80.88686,
    "place": "I Love Sushi",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "e554d998-8e44-462b-8c35-b70ecca9d569",
    "lat": 18.95609,
    "long": 72.82952,
    "place": "Il Fornaio",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "e55bf8ed-5d43-4ab5-8a23-697cc973dc61",
    "lat": 28.5521125,
    "long": 77.29887,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "e56b1a7a-868b-4f0f-a400-e7d154c07ee3",
    "lat": 28.5631357,
    "long": 77.350686,
    "place": "Bis On Main",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "e5ccb156-12b5-46c3-a4f2-c8796fa9ee93",
    "lat": 12.95375,
    "long": 77.52692,
    "place": "Redhook Ale Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "e5d08498-28e5-4fbb-a427-fa7c2f6354ac",
    "lat": 26.90767,
    "long": 80.97674,
    "place": "Herb Farm",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "e637cd04-dc96-42fc-ac84-5d7a1121e6fc",
    "lat": 28.40449,
    "long": 77.36316,
    "place": "Il Fornaio",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "e67d6706-d5cb-4f43-81c2-788a370791d9",
    "lat": 28.56022,
    "long": 77.21974,
    "place": "Salute of Bellevue",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "e68d1e31-a8e3-4721-ae3a-477dde1f370b",
    "lat": 26.84213,
    "long": 80.93633,
    "place": "Sea Garden",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "e6a3e103-fd8b-4fd8-a8d6-58dfe47e9346",
    "lat": 17.47993,
    "long": 78.5379,
    "place": "Il Fornaio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "e6a6e72c-5840-406d-8fd0-1fb8adcd26ff",
    "lat": 28.57894,
    "long": 77.38171,
    "place": "Columbia Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "e6bd9ab2-2db6-4e1b-8e3a-b9aec0d4f3e2",
    "lat": 23.055,
    "long": 72.63162,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "e6f84343-3749-45e4-a587-2fb2de5f9da6",
    "lat": 28.6811952,
    "long": 77.1638391,
    "place": "Italianissimo Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "e76d6273-d4d6-4644-9b3e-92dc3bd165c1",
    "lat": 12.8993548,
    "long": 77.69580388,
    "place": "Assagio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "e7b541cc-93bd-4012-bc44-daad7a54843d",
    "lat": 28.57403,
    "long": 77.33075,
    "place": "Uptown China Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "e84b61f6-386f-430f-bd9b-54a2cc02a11c",
    "lat": 28.5541909,
    "long": 77.3371235,
    "place": "Redhook Ale Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "e87a694c-68fb-470e-9dbc-f957edf2b3b8",
    "lat": 28.4045,
    "long": 77.36305,
    "place": "Kaspar's",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "e8be1c09-77de-4c42-8713-ea4bde5e410c",
    "lat": 28.5741926,
    "long": 77.3305331,
    "place": "Dahlia Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "e922b0cb-c1c3-4746-b219-a5299db55d1c",
    "lat": 18.59291,
    "long": 73.79788,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "e934278a-4b2a-4267-833f-a8ed46fec3a6",
    "lat": 28.36684,
    "long": 77.32895,
    "place": "Salute of Bellevue",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "e95052c6-edfa-4a64-b297-7743bcf778a1",
    "lat": 28.51795,
    "long": 77.20298,
    "place": "Barking Frog",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "e95f8963-8091-4af6-8983-32db69900ae2",
    "lat": 28.64469,
    "long": 77.16516,
    "place": "Pike Brewing Company",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "e966680e-4eef-4fd7-96c4-be21c1b95f4a",
    "lat": 28.646696,
    "long": 77.415042,
    "place": "Royal India",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "e97a2f6e-c354-4982-a81b-e1e0d2d0cfc0",
    "lat": 13.0370884,
    "long": 77.6219758,
    "place": "Kaspar's",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "e98fae98-b170-4af9-9206-3d0f572d8411",
    "lat": 28.51235,
    "long": 77.20761,
    "place": "Sans Souci",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "e9bcca79-7933-49e9-940a-e38a21883d06",
    "lat": 28.64585,
    "long": 77.1631,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "e9ce24f7-91c8-4a18-b695-4e41e5f18561",
    "lat": 28.4465972,
    "long": 77.0278044,
    "place": "Yarrow Bay Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "ea45b0f1-01c1-43e7-9543-c684a960b3dc",
    "lat": 19.1204017,
    "long": 72.8694867,
    "place": "The Calcutta Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ea5c68c5-5db1-4a3b-bf78-ffeed1891e0b",
    "lat": 28.6366387,
    "long": 77.2791422,
    "place": "The Tasting Room",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "ea71f3ba-b0fa-48bf-ac6e-cb0b2399eee3",
    "lat": 28.62587,
    "long": 77.43369,
    "place": "Isabella Ristorante",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "ea75e352-8a02-4f04-83a3-03ef71fc5d50",
    "lat": 28.70528,
    "long": 77.19765,
    "place": "Wasabi Bistro",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ead705c9-8964-4db0-b9b5-78c1428e9a5f",
    "lat": 28.499314,
    "long": 77.1960993,
    "place": "Wild Ginger",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "eaf770d6-d3d5-4189-9f3c-11d19dec213a",
    "lat": 28.4266895,
    "long": 77.0950288,
    "place": "Barking Frog",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "eb0f2d5a-3a09-43f9-82c7-c4fe2a0549b0",
    "lat": 19.20601075,
    "long": 72.96031713,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "eb46504b-7d5b-44f7-b4e7-9be2147a80ef",
    "lat": 28.51978,
    "long": 77.22668,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "eb6e2d90-0c9f-451a-b9ab-f21704297a6f",
    "lat": 28.39581,
    "long": 77.05945,
    "place": "Salish Lodge",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "eb7810f1-2197-450c-90f6-120756691c50",
    "lat": 28.42134,
    "long": 77.31762,
    "place": "Il Fornaio",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "eb7d714a-afb1-4a10-9c6d-36cce1e7c918",
    "lat": 28.47133,
    "long": 77.05241,
    "place": "Herb Farm",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "eb8b3222-c6ea-404f-bf40-503c28af2c4d",
    "lat": 18.57873,
    "long": 73.88763,
    "place": "The Tasting Room",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "ebb26257-917f-4315-9789-b39e88c798e4",
    "lat": 28.7288072,
    "long": 77.0895855,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "ebf491a2-d223-40c7-aa08-0328f142edaf",
    "lat": 22.56732,
    "long": 88.47365,
    "place": "Manzana Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "ebf62a95-743f-4ffb-b37c-eddc03238ac9",
    "lat": 28.4255994,
    "long": 77.0549768,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ec24aea2-2b31-4806-9454-7e0ef2bbe87b",
    "lat": 22.95976,
    "long": 72.61125,
    "place": "Isabella Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ec2d6714-ac0b-448d-83de-7357b2411687",
    "lat": 28.6330519,
    "long": 77.3122682,
    "place": "Barking Frog",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ec319ea2-4cd3-4357-b0ee-5fcb18944fd6",
    "lat": 28.6862494,
    "long": 77.2838548,
    "place": "Uptown China Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "ec6f3f82-a7ba-4f91-8ac0-6a430c915445",
    "lat": 18.95526,
    "long": 72.83608,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "ec7524a5-cd7e-460a-ab14-144891220e9b",
    "lat": 23.00523,
    "long": 72.62054,
    "place": "Maggiano's Little Italy",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ec899d19-7173-42ca-a869-795a2952bd76",
    "lat": 19.01223,
    "long": 72.84204,
    "place": "Herb Farm",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ecb07993-713c-4d70-8216-d3806117357a",
    "lat": 22.5527778,
    "long": 88.3554917,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "ed05f3ea-30bc-4321-b4c5-32c7483f1514",
    "lat": 22.46282,
    "long": 88.33318,
    "place": "Manzana Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ed229b52-4b13-447e-97c4-50a4ba8b8aa5",
    "lat": 19.12092,
    "long": 72.84947,
    "place": "Barking Frog",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "ed303d6f-76cb-4df2-85f9-617c80c4b99d",
    "lat": 28.63659,
    "long": 77.33837,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "ed391b4e-239f-4d6d-9cd0-de9fb8add3a8",
    "lat": 28.68452,
    "long": 77.36203,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ed4c31e9-54d0-4f06-bfc8-2afcc683ab9e",
    "lat": 28.5240194,
    "long": 77.1930409,
    "place": "Manzana Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "ed86961a-312b-429e-a506-4fbf9cbe5020",
    "lat": 28.38953,
    "long": 77.05899,
    "place": "Market Cellar Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "edbb39d4-fd1b-4085-a2b6-26ab4899b06b",
    "lat": 12.9801724,
    "long": 77.6777569,
    "place": "I Love Sushi",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "edc3bd83-476d-47fa-a7fb-d6bb3f94b537",
    "lat": 28.59886,
    "long": 77.31865,
    "place": "Sea Garden",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "edd5c3e6-69f8-428a-a576-0554928da540",
    "lat": 13.00862,
    "long": 77.71769,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "eddae10f-75f9-4750-810d-9a98157749fa",
    "lat": 28.4294,
    "long": 77.32433,
    "place": "Manzana Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "eddae1dc-1fef-4e98-b2c2-c5e7327f7503",
    "lat": 19.18021,
    "long": 72.85016,
    "place": "Il Fornaio",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "ede49ce3-191d-40d4-b351-ce97cd9c6d02",
    "lat": 28.70365,
    "long": 77.10369,
    "place": "Rover's",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ee09c15b-7421-4174-9e4b-a4e942383b85",
    "lat": 26.926095,
    "long": 75.7906767,
    "place": "Desert Fire",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ee14c85d-61e0-46fc-b7f8-4d49b97c8f61",
    "lat": 28.59715399,
    "long": 77.29537021,
    "place": "I Love Sushi",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ee418214-034c-44c4-adab-5bbdbe532d7c",
    "lat": 28.42701,
    "long": 77.07632,
    "place": "Café Juanita",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "ee83c8db-9192-49dd-b567-cc2bfcbef93f",
    "lat": 28.39915,
    "long": 77.29529,
    "place": "Café Juanita",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ee84b843-c3c3-42f6-a84c-2f0449ad2282",
    "lat": 22.60201,
    "long": 88.37586,
    "place": "Yarrow Bay Grill",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ee8a918c-12f6-4c6b-b11b-de456d8abb8e",
    "lat": 28.56639,
    "long": 77.35934,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "eeb323f9-806d-4c22-9d50-786935ea5fa6",
    "lat": 28.61957,
    "long": 77.2996,
    "place": "Royal India",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "eed9786e-2e82-49d0-920a-da4b2f053dde",
    "lat": 28.67155,
    "long": 77.08491,
    "place": "Kaspar's",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "ef19d94c-a9c6-4f95-94e0-9ec7c773fe89",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "Herb Farm",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ef247c06-48c1-4934-bf66-d61b8941d64e",
    "lat": 19.1204017,
    "long": 72.8694867,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "ef2d5f9e-ff38-4125-9e19-20b8fc4c7d4d",
    "lat": 28.63783,
    "long": 77.31322,
    "place": "Coho Cafe",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "ef402979-37ea-4aea-b81f-6a2158ff4b34",
    "lat": 19.18336,
    "long": 72.83213,
    "place": "Sans Souci",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "ef516748-c3fd-47d6-bc8e-781dc6a6e3f0",
    "lat": 28.45159,
    "long": 77.01877,
    "place": "Salute of Bellevue",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ef6afb4d-7b9b-42fc-92c2-a3c8eec9ad02",
    "lat": 22.5009267,
    "long": 88.3476781,
    "place": "Herb Farm",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "ef726122-159a-47f5-a04a-acd6be23d3e7",
    "lat": 28.4276819,
    "long": 77.0385928,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ef884506-4c4e-497a-8727-6f5c32d674c0",
    "lat": 28.6872161,
    "long": 77.132511,
    "place": "Redhook Ale Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ef979956-657a-490f-b24a-bb76dfe58231",
    "lat": 22.63207,
    "long": 88.43494,
    "place": "Assagio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "efbab08a-a13a-48f5-a89a-746f546bf8a7",
    "lat": 28.39653,
    "long": 76.96959,
    "place": "Space Needle",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "efbff97b-4b92-432c-b66f-a8a1a263080d",
    "lat": 19.10901,
    "long": 72.85514,
    "place": "Columbia Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "efe42d06-6946-40de-a182-c0e427cbc14a",
    "lat": 26.87636,
    "long": 80.90704,
    "place": "Desert Fire",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "eff8cf6d-5120-4856-b134-0f169beeb0d3",
    "lat": 28.53216,
    "long": 77.29826,
    "place": "The Calcutta Grill",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f000a9d9-7215-4734-9a93-1fcb24e46da0",
    "lat": 12.88333,
    "long": 77.56975,
    "place": "Barking Frog",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f02109ca-8606-4ac1-81ea-929e3f279d1a",
    "lat": 28.49611,
    "long": 77.18935,
    "place": "Italianissimo Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "f0422d44-696f-424b-9507-b90c5586a567",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "Typhoon",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "f06a50de-2d62-44b4-b999-91b3990be541",
    "lat": 19.1152028,
    "long": 73.0028817,
    "place": "Soos Creek Wine Cellars",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "f086c350-f98f-4504-aa08-021dd1b84a3f",
    "lat": 28.46795,
    "long": 77.08669,
    "place": "Café Juanita",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f0a4c0c6-c069-4c3c-a380-d060e51616c3",
    "lat": 28.4658,
    "long": 77.01884,
    "place": "Columbia Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "f0b63dc1-7f1c-426e-a750-579adb5a4873",
    "lat": 17.5062,
    "long": 78.33048,
    "place": "Italianissimo Ristorante",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f0eca924-f965-4432-82d5-414c92ef389a",
    "lat": 19.13713,
    "long": 72.86739,
    "place": "Buca di Beppo",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "f11e0eb2-08cc-4af4-a2c0-4b87eea3e45a",
    "lat": 18.4879,
    "long": 73.79627,
    "place": "Barking Frog",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f1229b95-73b3-4315-8515-43f35d8b7597",
    "lat": 19.20545,
    "long": 72.98352,
    "place": "I Love Sushi",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f14823c2-1889-4530-a537-63e7c0504ed7",
    "lat": 26.89692,
    "long": 80.95599,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "f14e5baa-4794-44c5-b133-11f40a7d8196",
    "lat": 28.64309,
    "long": 77.33549,
    "place": "Uptown China Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f183b379-24fa-47b2-8fa3-83602e35506b",
    "lat": 28.71027,
    "long": 77.03224,
    "place": "Italianissimo Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "f1b0f986-6dde-40b2-b64b-14251865eb68",
    "lat": 28.579477,
    "long": 77.2580933,
    "place": "Salish Lodge",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "f1ca1bef-8963-4ae3-91c6-cc0720f27310",
    "lat": 12.96714,
    "long": 77.74104,
    "place": "Wild Ginger",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f1ce48d5-237c-4a79-afa7-111e45db57e2",
    "lat": 28.5152,
    "long": 77.20414,
    "place": "Market Cellar Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f1fd1e5b-9262-4130-8a24-4a3e28255ef8",
    "lat": 18.49121,
    "long": 73.79216,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "f21542b3-0290-407f-a7f4-1b84fcf2135b",
    "lat": 28.52436,
    "long": 77.39213,
    "place": "The Calcutta Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "f2372115-f0cb-464e-9e5d-b01ab61aa1f9",
    "lat": 18.53905,
    "long": 73.88657,
    "place": "The Calcutta Grill",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f26c3692-a5ff-45a8-a207-5c0b11e1a8da",
    "lat": 28.61957,
    "long": 77.2996,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f27fbc6a-a2da-4b2e-bf3c-751ddf7c3037",
    "lat": 28.69255,
    "long": 77.32848,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "f2977b55-19c9-46c7-a4c2-8a258216628c",
    "lat": 28.5071507,
    "long": 77.2357335,
    "place": "Space Needle",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "f2ce29d6-36c7-41d9-a1ed-a11eac346694",
    "lat": 19.18021,
    "long": 72.85016,
    "place": "Uptown China Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f2cefeb5-c998-4131-8381-02a5609158dc",
    "lat": 19.14225,
    "long": 72.84221,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f2fabcb0-289a-4cc2-8446-9bb423e1ca60",
    "lat": 23.01853,
    "long": 72.56159,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "f34f9aba-15d9-4bd0-9572-c7db0a89f573",
    "lat": 12.9503643,
    "long": 77.6160986,
    "place": "Kaspar's",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "f35f19bc-a101-4970-80c0-532b65ada2a5",
    "lat": 28.65323,
    "long": 77.30199,
    "place": "Wasabi Bistro",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "f392d06d-ae2b-4278-b705-29123cf779a3",
    "lat": 23.055,
    "long": 72.63162,
    "place": "Yarrow Bay Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f39d9ada-127b-42d8-8a86-d03fe050eae0",
    "lat": 19.13608,
    "long": 72.86498,
    "place": "Il Fornaio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f3c1b413-87c0-43f7-8a8d-12b79676720e",
    "lat": 28.57075,
    "long": 77.17241,
    "place": "Manzana Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "f3d8c34a-451d-45c5-83bb-98ed5e6136f2",
    "lat": 28.36243,
    "long": 77.32671,
    "place": "Coho Cafe",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f3e543e8-be34-4594-9aab-39e6f3bb63dd",
    "lat": 28.35674,
    "long": 77.29094,
    "place": "Space Needle",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f3eeebc5-321e-4af6-af98-942b77ac0cd8",
    "lat": 19.0138182,
    "long": 72.8189984,
    "place": "Chutney's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f40c70c8-1a08-40b3-9bc8-b74e2d428210",
    "lat": 28.496,
    "long": 77.29483,
    "place": "Chutney's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f422d809-6469-4181-b2fb-d7d2aa176aff",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "Typhoon",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f435a670-9e85-48d6-9917-8a9d45c53904",
    "lat": 26.88754,
    "long": 80.95903,
    "place": "Pike Brewing Company",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "f47224a8-e904-4bae-a5fb-276734d1c382",
    "lat": 28.35676,
    "long": 77.29141,
    "place": "Market Cellar Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "f48678af-26e1-4084-abe6-f4b8600e80db",
    "lat": 28.64333,
    "long": 77.33543,
    "place": "I Love Sushi",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f489dfd9-67fd-4c2d-98ea-530ff4918881",
    "lat": 28.5203607,
    "long": 77.2559372,
    "place": "Cutters Bayhouse",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "f498e2ed-ab88-4898-bbb5-ae92126df986",
    "lat": 28.4289604,
    "long": 77.3245185,
    "place": "Yarrow Bay Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f499b1b1-f747-4a0e-ad2d-0bc882b941e2",
    "lat": 28.49229,
    "long": 77.09607,
    "place": "Chateau Ste Michelle Winery",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f4a250f8-b278-41c0-9bc0-8186d69a3cf7",
    "lat": 28.61705,
    "long": 77.03334,
    "place": "Assagio",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f4a6b7ff-ad2b-4415-ada8-8ceb45a44fe7",
    "lat": 28.41947,
    "long": 77.32594,
    "place": "Royal India",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f52e6485-df5f-4428-8a80-eec3049eff2a",
    "lat": 28.36015,
    "long": 77.32696,
    "place": "Bis On Main",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f5964d44-b449-4de0-a1be-2e58445f74b8",
    "lat": 28.51977,
    "long": 77.22669,
    "place": "Barking Frog",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f5a0e17c-d991-48bc-a429-2601722e8f88",
    "lat": 28.6385643,
    "long": 77.1836212,
    "place": "Salish Lodge",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "f5cce372-1cc1-400b-ab9b-805e2794c85a",
    "lat": 28.46903,
    "long": 77.00662,
    "place": "Yarrow Bay Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "f605d896-3644-467d-88f2-e14dc9a9b5a5",
    "lat": 23.02039,
    "long": 72.54376,
    "place": "Wasabi Bistro",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f66f13d4-4caf-4709-8b0c-8c5b69fdff9c",
    "lat": 28.38948,
    "long": 77.05923,
    "place": "Space Needle",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f672af53-52b8-4dd7-8f31-89981c735422",
    "lat": 19.20601075,
    "long": 72.96031713,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f68b6d72-1e10-4c8b-b773-e75b92b3ab3c",
    "lat": 28.4695327,
    "long": 77.2971186,
    "place": "Il Fornaio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f6a9d936-4913-4dc4-b63c-37d7f8f6531a",
    "lat": 28.63308,
    "long": 77.31218,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f7296f7e-6aeb-450c-ab4e-2019595c6f15",
    "lat": 22.47568,
    "long": 88.36891,
    "place": "Assagio",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "f7547dec-60bc-4c18-b63d-1054d3991331",
    "lat": 28.65781,
    "long": 77.36207,
    "place": "I Love Sushi",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "f77141f4-609c-4c5e-9bda-90f484009dc6",
    "lat": 28.59274,
    "long": 77.15325,
    "place": "Matt's Rotisserier & Oyster Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "f792d1c7-7346-43dd-a717-a7037fd8e6cd",
    "lat": 23.1081723,
    "long": 72.5914872,
    "place": "Assagio",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "f7b8207a-1630-4693-ae25-beba27463d8f",
    "lat": 23.02039,
    "long": 72.54376,
    "place": "Coho Cafe",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "f7d33d94-a90d-4dba-8b54-357231e7efc8",
    "lat": 28.5507,
    "long": 77.29179,
    "place": "Bis On Main",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "f7f51001-d1a0-4f35-813a-de1e14e5487f",
    "lat": 28.51978,
    "long": 77.22669,
    "place": "Space Needle",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f82b458d-7313-4ff4-abb4-b5cae2f75da4",
    "lat": 22.48948,
    "long": 88.31376,
    "place": "Trader Vic's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "f8551826-ab11-4e1e-995b-a0e2952a8d97",
    "lat": 28.6391824,
    "long": 77.0743123,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "f855c0b6-1e74-47d6-bca3-eb11dc047078",
    "lat": 28.65331,
    "long": 77.30207,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f8752534-6839-48b5-ade0-47590282ac7e",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "f875f3d4-ee2e-4e68-aafc-5cb3ccfd2032",
    "lat": 28.45811,
    "long": 77.05626,
    "place": "Manzana Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "f88344e5-41c5-4320-9278-b0fe8da55ce0",
    "lat": 23.1081723,
    "long": 72.5914872,
    "place": "I Love Sushi",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f8859336-3b2e-4b93-8363-5faea1ef953e",
    "lat": 28.3998202,
    "long": 77.3556479,
    "place": "Typhoon",
    "status": "on_trip",
    "reason": "",
    "activity": "Biking"
  },
  {
    "device_id": "f8988325-3d8d-422e-a520-a7a63c996aa9",
    "lat": 28.5632,
    "long": 77.21441,
    "place": "Manzana Grill",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f8e327dc-15c1-4b27-810c-616aa33a5ecb",
    "lat": 28.4136255,
    "long": 77.3552835,
    "place": "Soos Creek Wine Cellars",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "f91a9022-f034-43b6-8b02-0bb450fb20e0",
    "lat": 19.07847,
    "long": 72.88057,
    "place": "Barking Frog",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f92f4b65-229a-454c-819f-ea7ba2f89df1",
    "lat": 28.48944,
    "long": 77.09277,
    "place": "Il Fornaio",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "f94361d1-b048-4f0a-bbbd-13c5b76ac1ec",
    "lat": 28.56218,
    "long": 77.16292,
    "place": "Trader Vic's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f946564d-6c93-4415-9f34-5c666bf78f3f",
    "lat": 19.1964566,
    "long": 72.8757323,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "f963566d-cff7-4941-973d-54512a27041a",
    "lat": 28.51978,
    "long": 77.22668,
    "place": "Chateau Ste Michelle Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "f9652b3a-0607-49be-9ee5-27e5d4a6de35",
    "lat": 28.5707074,
    "long": 77.3844247,
    "place": "Barking Frog",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "f99698a1-3d2b-47c1-b639-89fceabdfbce",
    "lat": 23.10912,
    "long": 72.58958,
    "place": "Isabella Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "f9c9cdfe-4329-4586-8c75-a602ecaf9a9a",
    "lat": 18.95529,
    "long": 72.83603,
    "place": "Wild Ginger",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "f9e033c6-90f9-4223-af5f-beba37fdc07c",
    "lat": 22.57519,
    "long": 88.3573,
    "place": "Dahlia Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "fa0efcee-0209-444e-a189-f0a04d374168",
    "lat": 28.65529,
    "long": 77.16445,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "fa254674-8912-4701-a112-8cdedf80a2f3",
    "lat": 28.62675,
    "long": 77.11083,
    "place": "Chutney's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "fa66cb38-b069-40cd-8971-9f0580f51580",
    "lat": 28.53822,
    "long": 77.23058,
    "place": "Dahlia Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "faa67dcc-330b-491b-8e88-c0f1ade466b9",
    "lat": 12.96104,
    "long": 77.71334,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "fab71f67-d056-4a89-8212-d9b4d5884b3a",
    "lat": 28.6332138,
    "long": 77.3122497,
    "place": "Pyramid Alehouse, Brewery & Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "fab80467-03df-4232-8840-89a3869f0120",
    "lat": 19.23654,
    "long": 72.97261,
    "place": "Sans Souci",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "fac6c9c2-7a6d-4084-a4e7-d4838bb056c8",
    "lat": 28.68788,
    "long": 77.2694017,
    "place": "Salish Lodge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "facd12f7-d65e-4d92-bc3b-61d19437c7af",
    "lat": 28.4634,
    "long": 77.56355,
    "place": "Yarrow Bay Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "faea9ef1-3d0b-42a1-b9aa-fe889e145ce4",
    "lat": 28.5519447,
    "long": 77.3505566,
    "place": "Bis On Main",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "fafbf5c0-0c05-4d18-b921-bd7e40097218",
    "lat": 28.67544,
    "long": 77.22055,
    "place": "Soos Creek Wine Cellars",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "fb0cf590-3759-440b-be0e-2ddbfdb3539e",
    "lat": 28.59764,
    "long": 77.34069,
    "place": "Wasabi Bistro",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "fb16fa9b-f1c9-4401-a3aa-bd4ed193466d",
    "lat": 28.4312483,
    "long": 77.0868759,
    "place": "Barking Frog",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "fb32535c-ab90-481e-9b11-3e1094d05500",
    "lat": 28.65541,
    "long": 77.13163,
    "place": "Rock Bottom Restaurant & Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "fb441a58-09ae-45a4-8e2b-596a6e62f8be",
    "lat": 28.59571,
    "long": 77.0491,
    "place": "Uptown China Restaurant",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "fb5197c7-0d44-4b3d-8f6b-ae9ce762e92f",
    "lat": 28.554,
    "long": 77.33946,
    "place": "Kaspar's",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "fb6aa138-c828-4c42-b6ef-78fdb193d81f",
    "lat": 28.5708,
    "long": 77.17234,
    "place": "Desert Fire",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "fb7d0861-f58c-47a2-bc46-ea979048c320",
    "lat": 28.55696,
    "long": 77.0673,
    "place": "Maggiano's Little Italy",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "fb8cfc57-e443-4ed3-b1dd-e63795058702",
    "lat": 28.4356892,
    "long": 77.0566262,
    "place": "Salish Lodge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "fb90885e-2dc0-41c0-abd3-1a7b3878f9a1",
    "lat": 28.48347749,
    "long": 77.09036161,
    "place": "Sea Garden",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "fba42d77-26b4-4f1f-bcb6-d50aaf84eabb",
    "lat": 18.46323,
    "long": 73.90469,
    "place": "Café Juanita",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "fbf752be-0fbd-4ec9-9155-e623406f62e5",
    "lat": 28.6323,
    "long": 77.27341,
    "place": "Barking Frog",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "fc07affa-8f59-401d-b308-33e4a9a79af9",
    "lat": 28.71802,
    "long": 77.1779013,
    "place": "Sans Souci",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "fc47d95e-79e8-4136-b876-d4967ac440f0",
    "lat": 22.59209,
    "long": 88.34144,
    "place": "Market Cellar Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "fc669846-594d-48fc-986f-b878f983e582",
    "lat": 12.8831041,
    "long": 77.5695627,
    "place": "Il Fornaio",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "fc690ffe-4f83-4a74-b337-9e0c43c8cc37",
    "lat": 28.634539,
    "long": 77.3530191,
    "place": "Palomino",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "fc829564-7e6e-4b41-8613-d24e7ef70300",
    "lat": 19.07394,
    "long": 72.88359,
    "place": "Kaspar's",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "fc914e3c-5798-4ec1-af0d-a5fe92fb2c68",
    "lat": 12.95791,
    "long": 77.69444,
    "place": "The Tasting Room",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "fc9f9979-2b81-4233-92db-d7a39bcdb68f",
    "lat": 12.8993548,
    "long": 77.69580388,
    "place": "Salish Lodge",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "fcc0da9a-c922-47e6-b4c0-9c09482b63ac",
    "lat": 28.66995,
    "long": 77.4478,
    "place": "Typhoon",
    "status": "disconnected",
    "reason": "Location permission denied",
    "activity": ""
  },
  {
    "device_id": "fce1ba77-e8cf-4fcf-9222-1c9f766382ff",
    "lat": 28.59698,
    "long": 77.07179,
    "place": "Assagio",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "fd11f025-8672-40c1-b363-061d7335aa9d",
    "lat": 12.99556,
    "long": 77.67964,
    "place": "Columbia Winery",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "fd21851d-7bf8-4e4b-9f98-83dd4505fbcc",
    "lat": 19.14992,
    "long": 72.83159,
    "place": "Café Juanita",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "fd3dccda-2043-4d09-8d1e-2bcc202b0877",
    "lat": 28.63309,
    "long": 77.31219,
    "place": "Italianissimo Ristorante",
    "status": "disconnected",
    "reason": "Tracking paused",
    "activity": ""
  },
  {
    "device_id": "fd3eb48f-5624-4ea6-8fb0-d10f1e2216d4",
    "lat": 28.5087,
    "long": 77.0192,
    "place": "Wasabi Bistro",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "fd446e98-8baa-497c-bf48-95bd6db55b72",
    "lat": 28.6390066,
    "long": 77.3788168,
    "place": "Wild Ginger",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "fd509952-8e58-47eb-8213-2f6795e172d6",
    "lat": 28.51978,
    "long": 77.2267,
    "place": "Kaspar's",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "fd5d5504-17c6-4600-b321-bc3e94b54c9a",
    "lat": 19.11517,
    "long": 73.00293,
    "place": "Market Cellar Winery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "fd884b41-576c-43ca-b081-7222531c633c",
    "lat": 26.8794116,
    "long": 81.0065383,
    "place": "Wild Ginger",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "fdaed2f8-aae4-4e77-b7d9-903a493b6009",
    "lat": 28.65322,
    "long": 77.10017,
    "place": "Redhook Ale Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "fdc984b5-ca81-4b28-9577-cb4f0344e41d",
    "lat": 28.6379272,
    "long": 77.3615437,
    "place": "Tap House Grill",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "fdcc56f3-82a8-446f-9656-3f032b1ce14b",
    "lat": 19.19084,
    "long": 72.82029,
    "place": "Coho Cafe",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "fde7e4c6-3621-4a3b-bef6-37449c6c01c7",
    "lat": 28.45291,
    "long": 77.06154,
    "place": "Trader Vic's",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "fdfe64d8-5c6e-49d4-a705-df2ee5841f94",
    "lat": 28.63818,
    "long": 77.30221,
    "place": "Il Fornaio",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "fe08c8a8-0c66-4e2c-92ab-805681fb988a",
    "lat": 17.44528,
    "long": 78.38695,
    "place": "Kaspar's",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "fe2a78fd-a864-43ff-94b8-722e7a17e695",
    "lat": 22.5473616,
    "long": 88.366274,
    "place": "Sans Souci",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "fe417016-775a-4713-b73e-e9b72f9086b7",
    "lat": 19.01998,
    "long": 73.01761,
    "place": "Wasabi Bistro",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "fea05853-63a0-486e-8c85-c61805f086c8",
    "lat": 28.6023246,
    "long": 77.0745418,
    "place": "Salute of Bellevue",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "feaed433-4475-406f-ae7c-d054553ea8a9",
    "lat": 28.56801,
    "long": 77.38309,
    "place": "Mac and Jack’s Brewery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "febb35dc-b262-4a9d-92a8-b472c6c5644f",
    "lat": 28.5541909,
    "long": 77.3371235,
    "place": "Dahlia Lounge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "fee30c2f-15c8-4397-bc38-5286d98b7ed1",
    "lat": 12.9818786,
    "long": 77.6155783,
    "place": "Jalisco's Mexican Restaurants & Taqueria",
    "status": "disconnected",
    "reason": "Activity permission denied",
    "activity": ""
  },
  {
    "device_id": "ff020769-53f2-49ef-a848-2432cd8b8543",
    "lat": 28.51798,
    "long": 77.20239,
    "place": "Pike Brewing Company",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "ff4b7da9-b149-458e-8588-3510f329d4cf",
    "lat": 18.6306,
    "long": 73.79628,
    "place": "Dahlia Lounge",
    "status": "on_trip",
    "reason": "",
    "activity": "Running"
  },
  {
    "device_id": "ff67abc5-4c12-40fa-866c-3f4095c94c9c",
    "lat": 28.6909763,
    "long": 77.2950709,
    "place": "Cutters Bayhouse",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ff7090bc-89bb-4c54-977d-8a53c84af293",
    "lat": 28.5889147,
    "long": 77.0768528,
    "place": "Tap House Grill",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "ff72171f-73a5-4488-ae49-9202444b76e6",
    "lat": 28.66799,
    "long": 77.1425,
    "place": "Trader Vic's",
    "status": "disconnected",
    "reason": "Location disabled",
    "activity": ""
  },
  {
    "device_id": "ff796781-b97d-49fe-87f7-22fa18b64f00",
    "lat": 12.9801724,
    "long": 77.6777569,
    "place": "Isabella Ristorante",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "ff9cf4c9-8ff0-45e9-be45-117a3b845324",
    "lat": 28.74275,
    "long": 77.11773,
    "place": "Columbia Winery",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ffa34fe7-7883-4bc4-a0b2-782e71d6c7bd",
    "lat": 28.52395367,
    "long": 77.19309043,
    "place": "Cutters Bayhouse",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ffb903ab-3ddd-4e47-9490-114ef7f1a64e",
    "lat": 28.5972,
    "long": 77.29704,
    "place": "Sea Garden",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "ffbdd4b8-4b17-4b13-81fb-078b0512dfbe",
    "lat": 28.66052,
    "long": 77.35902,
    "place": "The Tasting Room",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "ffdd4105-0740-4597-8097-313dd85a436c",
    "lat": 28.43911,
    "long": 77.07236,
    "place": "Sea Garden",
    "status": "on_trip",
    "reason": "",
    "activity": "Driving"
  },
  {
    "device_id": "ffe3ddca-98cb-4811-942d-0c46b0789e31",
    "lat": 12.96869,
    "long": 77.75205,
    "place": "Chateau Ste Michelle Winery",
    "status": "disconnected",
    "reason": "GPS lost",
    "activity": ""
  },
  {
    "device_id": "ffeabd51-f6d3-41c0-9a92-3c77ef0144a3",
    "lat": 19.1784515,
    "long": 72.8530593,
    "place": "Redhook Ale Brewery",
    "status": "on_trip",
    "reason": "",
    "activity": "Walking"
  },
  {
    "device_id": "fff15c2c-ca10-4cda-9c81-e3a05c680332",
    "lat": 22.52531,
    "long": 88.39469,
    "place": "Isabella Ristorante",
    "status": "tracking_disabled",
    "reason": "",
    "activity": ""
  },
  {
    "device_id": "fffffa29-db83-4128-a730-ab5c66e94c29",
    "lat": 28.4658,
    "long": 77.00628,
    "place": "Salish Lodge",
    "status": "on_visit",
    "reason": "",
    "activity": ""
  }
]

const sumLatLong = (dataPoints = []) => dataPoints.reduce((acc, currentVal) => ({
  sum_lat: acc.sum_lat + currentVal.lat,
  sum_long: acc.sum_long + currentVal.long
}), {sum_lat: 0, sum_long: 0})

const averageLatLong = (dataPoints = []) => {
  const {sum_lat, sum_long} = sumLatLong(dataPoints)
  return {
    avg_lat: sum_lat/dataPoints.length,
    avg_long: sum_long/dataPoints.length
  }
}

const coordinateCreator = (dataPoint) => [dataPoint.lat, dataPoint.long]

const PointCreator = (dataPoint) => ({
  type: 'Point',
  coordinates: [coordinateCreator(dataPoint)]
})

const constructGeoJson = (dataPoints = []) => {
  const features = dataPoints.reduce((features, currentDatapoint) => {
    if (features[currentDatapoint.status]) {
      return ({
        ...features,
        [currentDatapoint.status]: {
          ...features[currentDatapoint.status],
          geometry: {
            ...features[currentDatapoint.status].geometry,
            coordinates: [...features[currentDatapoint.status].geometry.coordinates, coordinateCreator(currentDatapoint)]
          }
        }
      })
    } else {
      return ({
        ...features,
        [currentDatapoint.status]: {
          properties: {},
          geometry: PointCreator(currentDatapoint)
        }
      })
    }
  }, {})
  return Object.keys(features).map(feature => ({type: feature, ...features[feature]}))
}

export {
  sampleJSON,
  completeJson,
  averageLatLong,
  constructGeoJson
}