1. Devices with nearby locations are clustered by location and status.
2. When you zoom-in to that location, you can see individual devices.
3. We should be able to filter based on the status of the device.
4. Every status is colour coded.